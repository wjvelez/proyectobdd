﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    
    class DataBase
    {


        /*   --DataAdapter--
         * Representa un conjunto de comandos
         * SQL y una conexión de base de datos
         * que se utilizan para rellenar el objeto 
         * DataSet y actualizar el origen de datos.*/

        /*    --Command--
         * Representa un procedimiento almacenado o 
         * una instrucción de Transact-SQL 
         * que se ejecuta en una base de datos de SQL Server. 
         * Esta clase no puede heredarse.
         */

        /*   --CommandType--	    
         * Obtiene o establece un valor que indica cómo 
         * se INTERPRETA la propiedad CommandText. 
         * (Invalida a DbCommand.CommandType).*/


        /*     --CommandText--	
         * Obtiene o establece la INSTRUCCIONES de Transact-SQL,
         * el nombre de tabla o el procedimiento almacenado 
         * que se ejecutan en el origen de datos. 
         * (Invalida a DbCommand.CommandText).*/

        /*     --DataTable--
         * Representa una tabla de datos en memoria.*/


        public MySqlConnection conect;
        public MySqlCommand cmd;
        public MySqlDataAdapter d_Adp;
        public DataTable d_tb;


        public string UnicoValor;

        /*CONECTA A LA BASE DE DATOS*/
        public bool ConexionBase()
        {
            try
            {
                conect = new MySqlConnection("server=127.0.0.1; database = proyecto_BD; Uid=root; pwd=shalom7;");

                if (conect.State == System.Data.ConnectionState.Closed)
                    conect.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }


        /*ExTRAE INFORMACION DE ACUERDO AL --STORED PROCEDURES--*/
        public bool ReadTable(string stored)
        {
            try
            {
                ConexionBase();
                cmd = new MySqlCommand();
                cmd.Connection = conect;
                cmd.CommandText = stored;
                cmd.CommandType = CommandType.StoredProcedure;
                d_Adp = new MySqlDataAdapter(cmd);
                d_tb = new DataTable();
                //Una ves que se ejecuta la linea sgt DataTable d_tb queda lleno de los datos de la base
                d_Adp.Fill(d_tb);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /*RETORNA UN VALOR ENTERO DE LA BASE DE DATOS*/

        public bool Return_Unico_Valor(string stored,string Atributo_salida)
        {

            // nombre del unico atributo que se quiere enviar  --> "Atributo_salida"
            try
            {
                ConexionBase();
                cmd = new MySqlCommand();
                cmd.Connection = conect;
                cmd.CommandText = stored;
                cmd.CommandType = CommandType.StoredProcedure;
                d_Adp = new MySqlDataAdapter(cmd);
                d_tb = new DataTable();
                d_Adp.Fill(d_tb);

                //DataRow es un tipo de dato al que se le asigna una fila del dataTable
                DataRow row = d_tb.Rows[0];

                UnicoValor = row[Atributo_salida].ToString();

                return true;
            }
            catch
            {
                return false;
            }
        }

        /*AGREGA INFORMACION DE ACUERDO AL --STORED PROCEDURES--*/
        public bool WriteTable(string stored,parametros [] inDatos)
        {
            try
            {
                ConexionBase();
                cmd = new MySqlCommand();
                cmd.Connection = conect;
                cmd.CommandText = stored;
                cmd.CommandType = CommandType.StoredProcedure;


                for (int i = 0; i < inDatos.Length ; i++ )
                {
                    /* ingresa datos a la base referencial
                     primero : el nombre de la columna correpondiente a la base de datos utilizar
                     segundo : el valor de lo que se desea ingresar*/
                    cmd.Parameters.Add(inDatos[i].nameColumna,inDatos[i].values);
                }
                

                d_Adp = new MySqlDataAdapter(cmd);
                d_tb = new DataTable();
                //Una ves que se ejecuta la linea sgt DataTable d_tb queda lleno de los datos de la base
                d_Adp.Fill(d_tb);
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        


        




    }
}
