﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;




namespace EquirentCS
{
    class Alquiler : DataBase
    {


         //insert into alquiler (ruc,cedula,fecha_inicio,costoHora,anticipo)
         //values ('1234567890987',null,'2015-08-28',2,1);

        public string rucA;
        public string cedulaA;
        public string fecha_inicioA;
        public string costoHoraA;
        public parametros[] prm;


        public Alquiler()
        {

        }


        /*Constructor cuando el alquiler se lo hace a una cliente natural*/
        public Alquiler(string ruc,string cedula,string fecha_inicio,string costoHora)
        {
            rucA = ruc;
            cedulaA = cedula;
            fecha_inicioA = fecha_inicio;
            costoHoraA = costoHora;

        }


        public bool getCodigo_ALquiler()
        {
            return Return_Unico_Valor("GET_CODIGO_ALQUILER_LAST","codigo");
        }

        public bool agregarAlquiler()
        {
            prm = new parametros[4];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_ruc", rucA);
            prm[1] = new parametros("in_cedula",cedulaA);
            prm[2] = new parametros("in_fecha_inicio", fecha_inicioA);
            prm[3] = new parametros("in_costoHora", costoHoraA);
            return WriteTable("SET_ALQUILER", prm);
        }



    }
}
