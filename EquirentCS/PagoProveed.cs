﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class PagoProveed: DataBase
    {
        public string ruc = "";
        public string maquina = "";
        public string codFact = "";
        public string fecha = "";
        public string det = "";
        public string tipo = "";
        public string valor = "";
        public parametros[] prm;

        public PagoProveed()
        {
        }

        public PagoProveed(string ruc, string maq, string codFact, string f, string det, string tipo, string valor)
        {
            this.ruc = ruc;
            this.maquina = maq;
            this.codFact = codFact;
            this.fecha = f;
            this.det = det;
            this.tipo = tipo;
            this.valor = valor;
        }

        public bool leerPagoProveed()
        {
            return ReadTable("GET_PAGPROVEED");
        }


        public bool agregarPagoProveed()
        {
            prm = new parametros[7];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("fact", codFact);
            prm[1] = new parametros("in_ruc", ruc);
            prm[2] = new parametros("in_placa", maquina);
            prm[3] = new parametros("det", det);
            prm[4] = new parametros("val", valor);
            prm[5] = new parametros("in_tipo", tipo);
            prm[6] = new parametros("fech", fecha);

            return WriteTable("SET_PAGPROVEED", prm);
        }

        public bool modificarPagoProveed()
        {
            prm = new parametros[7];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("fact", codFact);
            prm[1] = new parametros("in_ruc", ruc);
            prm[2] = new parametros("in_placa", maquina);
            prm[3] = new parametros("det", det);
            prm[4] = new parametros("val", valor);
            prm[5] = new parametros("in_tipo", tipo);
            prm[6] = new parametros("fech", fecha);

            return WriteTable("MOD_PAGPROVEED", prm);
        }

        public bool eliminarPagoProveed()
        {
            prm = new parametros[2];
            
            prm[0] = new parametros("fact", codFact);
            prm[1] = new parametros("in_ruc", ruc);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_PAGPROVEED", prm);
        }
    }
}
