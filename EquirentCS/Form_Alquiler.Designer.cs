﻿namespace EquirentCS
{
    partial class Form_Alquiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtn_ci = new System.Windows.Forms.RadioButton();
            this.rbtn_ruc = new System.Windows.Forms.RadioButton();
            this.txt_ident = new System.Windows.Forms.TextBox();
            this.lbl_costo = new System.Windows.Forms.Label();
            this.txt_costoHora = new System.Windows.Forms.TextBox();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_Alquilar = new System.Windows.Forms.Button();
            this.lbl_fecha = new System.Windows.Forms.Label();
            this.dateTP_alquiler = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tPg_DC = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_filtro = new System.Windows.Forms.TextBox();
            this.dgv_cliente = new System.Windows.Forms.DataGridView();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.txt_telefono = new System.Windows.Forms.TextBox();
            this.lbl_telefono = new System.Windows.Forms.Label();
            this.txt_direcc = new System.Windows.Forms.TextBox();
            this.lbl_direccion = new System.Windows.Forms.Label();
            this.lbl_nombre = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.tPg_DA = new System.Windows.Forms.TabPage();
            this.txt_date = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_tipoFiltro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_tipo = new System.Windows.Forms.TextBox();
            this.dG_maquinas = new System.Windows.Forms.DataGridView();
            this.tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_placa = new System.Windows.Forms.TextBox();
            this.btn_añadir = new System.Windows.Forms.Button();
            this.dG_OperMaq = new System.Windows.Forms.DataGridView();
            this.maquinariaAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placaAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabControl1.SuspendLayout();
            this.tPg_DC.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cliente)).BeginInit();
            this.tPg_DA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dG_maquinas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dG_OperMaq)).BeginInit();
            this.SuspendLayout();
            // 
            // rbtn_ci
            // 
            this.rbtn_ci.AutoSize = true;
            this.rbtn_ci.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ci.Location = new System.Drawing.Point(73, 38);
            this.rbtn_ci.Name = "rbtn_ci";
            this.rbtn_ci.Size = new System.Drawing.Size(51, 24);
            this.rbtn_ci.TabIndex = 2;
            this.rbtn_ci.TabStop = true;
            this.rbtn_ci.Text = "C.I.";
            this.rbtn_ci.UseVisualStyleBackColor = true;
            this.rbtn_ci.CheckedChanged += new System.EventHandler(this.rB_CI_CheckedChanged);
            // 
            // rbtn_ruc
            // 
            this.rbtn_ruc.AutoSize = true;
            this.rbtn_ruc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ruc.Location = new System.Drawing.Point(137, 40);
            this.rbtn_ruc.Name = "rbtn_ruc";
            this.rbtn_ruc.Size = new System.Drawing.Size(74, 24);
            this.rbtn_ruc.TabIndex = 3;
            this.rbtn_ruc.TabStop = true;
            this.rbtn_ruc.Text = "R.U.C.";
            this.rbtn_ruc.UseVisualStyleBackColor = true;
            this.rbtn_ruc.CheckedChanged += new System.EventHandler(this.rB_ruc_CheckedChanged);
            // 
            // txt_ident
            // 
            this.txt_ident.Enabled = false;
            this.txt_ident.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ident.Location = new System.Drawing.Point(72, 117);
            this.txt_ident.Name = "txt_ident";
            this.txt_ident.Size = new System.Drawing.Size(121, 26);
            this.txt_ident.TabIndex = 4;
            this.txt_ident.TextChanged += new System.EventHandler(this.txt_ident_TextChanged);
            this.txt_ident.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ident_KeyPress);
            this.txt_ident.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_ident_KeyUp);
            this.txt_ident.Leave += new System.EventHandler(this.txt_ident_Leave);
            // 
            // lbl_costo
            // 
            this.lbl_costo.AutoSize = true;
            this.lbl_costo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_costo.Location = new System.Drawing.Point(17, 397);
            this.lbl_costo.Name = "lbl_costo";
            this.lbl_costo.Size = new System.Drawing.Size(109, 20);
            this.lbl_costo.TabIndex = 14;
            this.lbl_costo.Text = "Costo de hora";
            // 
            // txt_costoHora
            // 
            this.txt_costoHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_costoHora.Location = new System.Drawing.Point(135, 397);
            this.txt_costoHora.Name = "txt_costoHora";
            this.txt_costoHora.Size = new System.Drawing.Size(123, 26);
            this.txt_costoHora.TabIndex = 15;
            this.txt_costoHora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_costoHora_KeyPress);
            this.txt_costoHora.Leave += new System.EventHandler(this.txt_costoHora_Leave);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancelar.Location = new System.Drawing.Point(310, 500);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(89, 35);
            this.btn_cancelar.TabIndex = 17;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_Alquilar
            // 
            this.btn_Alquilar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Alquilar.Location = new System.Drawing.Point(558, 391);
            this.btn_Alquilar.Name = "btn_Alquilar";
            this.btn_Alquilar.Size = new System.Drawing.Size(89, 35);
            this.btn_Alquilar.TabIndex = 18;
            this.btn_Alquilar.Text = "Alquilar";
            this.btn_Alquilar.UseVisualStyleBackColor = true;
            this.btn_Alquilar.Click += new System.EventHandler(this.btn_Alquilar_Click);
            // 
            // lbl_fecha
            // 
            this.lbl_fecha.AutoSize = true;
            this.lbl_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fecha.Location = new System.Drawing.Point(17, 20);
            this.lbl_fecha.Name = "lbl_fecha";
            this.lbl_fecha.Size = new System.Drawing.Size(54, 20);
            this.lbl_fecha.TabIndex = 19;
            this.lbl_fecha.Text = "Fecha";
            // 
            // dateTP_alquiler
            // 
            this.dateTP_alquiler.CustomFormat = "";
            this.dateTP_alquiler.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTP_alquiler.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTP_alquiler.Location = new System.Drawing.Point(94, 15);
            this.dateTP_alquiler.MinDate = new System.DateTime(2015, 9, 2, 0, 0, 0, 0);
            this.dateTP_alquiler.Name = "dateTP_alquiler";
            this.dateTP_alquiler.Size = new System.Drawing.Size(131, 26);
            this.dateTP_alquiler.TabIndex = 20;
            this.dateTP_alquiler.Value = new System.DateTime(2015, 9, 2, 0, 0, 0, 0);
            this.dateTP_alquiler.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tPg_DC);
            this.tabControl1.Controls.Add(this.tPg_DA);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(28, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(794, 472);
            this.tabControl1.TabIndex = 23;
            this.tabControl1.BindingContextChanged += new System.EventHandler(this.tabControl1_BindingContextChanged);
            // 
            // tPg_DC
            // 
            this.tPg_DC.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tPg_DC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tPg_DC.Controls.Add(this.groupBox1);
            this.tPg_DC.Location = new System.Drawing.Point(4, 28);
            this.tPg_DC.Name = "tPg_DC";
            this.tPg_DC.Padding = new System.Windows.Forms.Padding(3);
            this.tPg_DC.Size = new System.Drawing.Size(786, 440);
            this.tPg_DC.TabIndex = 0;
            this.tPg_DC.Text = "DATOS DEL CLIENTE";
            this.tPg_DC.Click += new System.EventHandler(this.tPg_DC_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_filtro);
            this.groupBox1.Controls.Add(this.dgv_cliente);
            this.groupBox1.Controls.Add(this.btn_limpiar);
            this.groupBox1.Controls.Add(this.txt_email);
            this.groupBox1.Controls.Add(this.lbl_email);
            this.groupBox1.Controls.Add(this.txt_telefono);
            this.groupBox1.Controls.Add(this.lbl_telefono);
            this.groupBox1.Controls.Add(this.txt_direcc);
            this.groupBox1.Controls.Add(this.lbl_direccion);
            this.groupBox1.Controls.Add(this.lbl_nombre);
            this.groupBox1.Controls.Add(this.txt_nombre);
            this.groupBox1.Controls.Add(this.txt_ident);
            this.groupBox1.Controls.Add(this.rbtn_ci);
            this.groupBox1.Controls.Add(this.rbtn_ruc);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 426);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TIPO DE CLIENTE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 36;
            this.label3.Text = "Filtro";
            // 
            // txt_filtro
            // 
            this.txt_filtro.Enabled = false;
            this.txt_filtro.Location = new System.Drawing.Point(72, 76);
            this.txt_filtro.Name = "txt_filtro";
            this.txt_filtro.Size = new System.Drawing.Size(121, 23);
            this.txt_filtro.TabIndex = 35;
            this.txt_filtro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_filtro_KeyPress);
            this.txt_filtro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_filtro_KeyUp);
            this.txt_filtro.Leave += new System.EventHandler(this.txt_filtro_Leave);
            // 
            // dgv_cliente
            // 
            this.dgv_cliente.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_cliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombre,
            this.direccion,
            this.telefono,
            this.email});
            this.dgv_cliente.Location = new System.Drawing.Point(6, 185);
            this.dgv_cliente.Name = "dgv_cliente";
            this.dgv_cliente.Size = new System.Drawing.Size(760, 235);
            this.dgv_cliente.TabIndex = 34;
            this.dgv_cliente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_autollenadoClient);
            // 
            // nombre
            // 
            this.nombre.DataPropertyName = "nombre";
            this.nombre.HeaderText = "nombre";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 150;
            // 
            // direccion
            // 
            this.direccion.DataPropertyName = "direccion";
            this.direccion.HeaderText = "direccion";
            this.direccion.Name = "direccion";
            this.direccion.ReadOnly = true;
            this.direccion.Width = 150;
            // 
            // telefono
            // 
            this.telefono.DataPropertyName = "telefono";
            this.telefono.HeaderText = "telefono";
            this.telefono.Name = "telefono";
            this.telefono.ReadOnly = true;
            // 
            // email
            // 
            this.email.DataPropertyName = "email";
            this.email.HeaderText = "email";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            this.email.Width = 150;
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_limpiar.Location = new System.Drawing.Point(554, 155);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(85, 24);
            this.btn_limpiar.TabIndex = 16;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // txt_email
            // 
            this.txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_email.Location = new System.Drawing.Point(462, 111);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(284, 26);
            this.txt_email.TabIndex = 33;
            this.txt_email.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_email_KeyPress);
            this.txt_email.MouseLeave += new System.EventHandler(this.txt_email_Leave);
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_email.Location = new System.Drawing.Point(344, 117);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(53, 20);
            this.lbl_email.TabIndex = 32;
            this.lbl_email.Text = "E-mail";
            // 
            // txt_telefono
            // 
            this.txt_telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_telefono.Location = new System.Drawing.Point(462, 76);
            this.txt_telefono.Name = "txt_telefono";
            this.txt_telefono.Size = new System.Drawing.Size(284, 26);
            this.txt_telefono.TabIndex = 31;
            this.txt_telefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Telefono_KeyPress);
            // 
            // lbl_telefono
            // 
            this.lbl_telefono.AutoSize = true;
            this.lbl_telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_telefono.Location = new System.Drawing.Point(338, 79);
            this.lbl_telefono.Name = "lbl_telefono";
            this.lbl_telefono.Size = new System.Drawing.Size(71, 20);
            this.lbl_telefono.TabIndex = 30;
            this.lbl_telefono.Text = "Telefono";
            // 
            // txt_direcc
            // 
            this.txt_direcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_direcc.Location = new System.Drawing.Point(462, 44);
            this.txt_direcc.Name = "txt_direcc";
            this.txt_direcc.Size = new System.Drawing.Size(284, 26);
            this.txt_direcc.TabIndex = 29;
            this.txt_direcc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Adress_KeyPress);
            // 
            // lbl_direccion
            // 
            this.lbl_direccion.AutoSize = true;
            this.lbl_direccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_direccion.Location = new System.Drawing.Point(344, 44);
            this.lbl_direccion.Name = "lbl_direccion";
            this.lbl_direccion.Size = new System.Drawing.Size(75, 20);
            this.lbl_direccion.TabIndex = 28;
            this.lbl_direccion.Text = "Dirección";
            // 
            // lbl_nombre
            // 
            this.lbl_nombre.AutoSize = true;
            this.lbl_nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nombre.Location = new System.Drawing.Point(344, 12);
            this.lbl_nombre.Name = "lbl_nombre";
            this.lbl_nombre.Size = new System.Drawing.Size(65, 20);
            this.lbl_nombre.TabIndex = 26;
            this.lbl_nombre.Text = "Nombre";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nombre.Location = new System.Drawing.Point(462, 12);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(284, 26);
            this.txt_nombre.TabIndex = 27;
            this.txt_nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Nombre_KeyPress);
            // 
            // tPg_DA
            // 
            this.tPg_DA.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tPg_DA.Controls.Add(this.txt_date);
            this.tPg_DA.Controls.Add(this.label2);
            this.tPg_DA.Controls.Add(this.txt_tipoFiltro);
            this.tPg_DA.Controls.Add(this.label1);
            this.tPg_DA.Controls.Add(this.txt_tipo);
            this.tPg_DA.Controls.Add(this.dG_maquinas);
            this.tPg_DA.Controls.Add(this.txt_placa);
            this.tPg_DA.Controls.Add(this.btn_añadir);
            this.tPg_DA.Controls.Add(this.dG_OperMaq);
            this.tPg_DA.Controls.Add(this.lbl_fecha);
            this.tPg_DA.Controls.Add(this.btn_Alquilar);
            this.tPg_DA.Controls.Add(this.dateTP_alquiler);
            this.tPg_DA.Controls.Add(this.txt_costoHora);
            this.tPg_DA.Controls.Add(this.lbl_costo);
            this.tPg_DA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tPg_DA.Location = new System.Drawing.Point(4, 28);
            this.tPg_DA.Name = "tPg_DA";
            this.tPg_DA.Padding = new System.Windows.Forms.Padding(3);
            this.tPg_DA.Size = new System.Drawing.Size(786, 440);
            this.tPg_DA.TabIndex = 1;
            this.tPg_DA.Text = "DATOS ALQUILER";
            this.tPg_DA.Click += new System.EventHandler(this.tPg_DA_Click);
            // 
            // txt_date
            // 
            this.txt_date.BackColor = System.Drawing.SystemColors.Info;
            this.txt_date.Enabled = false;
            this.txt_date.Location = new System.Drawing.Point(94, 47);
            this.txt_date.Name = "txt_date";
            this.txt_date.Size = new System.Drawing.Size(131, 23);
            this.txt_date.TabIndex = 32;
            this.txt_date.TextChanged += new System.EventHandler(this.txt_date_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(51, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "TIPO";
            // 
            // txt_tipoFiltro
            // 
            this.txt_tipoFiltro.Location = new System.Drawing.Point(97, 102);
            this.txt_tipoFiltro.MaxLength = 20;
            this.txt_tipoFiltro.Name = "txt_tipoFiltro";
            this.txt_tipoFiltro.Size = new System.Drawing.Size(142, 23);
            this.txt_tipoFiltro.TabIndex = 30;
            this.txt_tipoFiltro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_tipoFiltro_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.MintCream;
            this.label1.Location = new System.Drawing.Point(51, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 17);
            this.label1.TabIndex = 29;
            this.label1.Text = "MAQUINAS DISPONIBLES";
            // 
            // txt_tipo
            // 
            this.txt_tipo.BackColor = System.Drawing.SystemColors.Info;
            this.txt_tipo.Enabled = false;
            this.txt_tipo.Location = new System.Drawing.Point(453, 20);
            this.txt_tipo.Name = "txt_tipo";
            this.txt_tipo.Size = new System.Drawing.Size(180, 23);
            this.txt_tipo.TabIndex = 28;
            // 
            // dG_maquinas
            // 
            this.dG_maquinas.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dG_maquinas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_maquinas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipo,
            this.placa});
            this.dG_maquinas.Location = new System.Drawing.Point(21, 136);
            this.dG_maquinas.Name = "dG_maquinas";
            this.dG_maquinas.Size = new System.Drawing.Size(280, 210);
            this.dG_maquinas.TabIndex = 27;
            this.dG_maquinas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_autollenado);
            // 
            // tipo
            // 
            this.tipo.DataPropertyName = "tipo";
            this.tipo.HeaderText = "TIPO";
            this.tipo.Name = "tipo";
            this.tipo.ReadOnly = true;
            // 
            // placa
            // 
            this.placa.DataPropertyName = "placa";
            this.placa.HeaderText = "PLACA";
            this.placa.Name = "placa";
            this.placa.ReadOnly = true;
            // 
            // txt_placa
            // 
            this.txt_placa.BackColor = System.Drawing.SystemColors.Info;
            this.txt_placa.Enabled = false;
            this.txt_placa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_placa.Location = new System.Drawing.Point(453, 86);
            this.txt_placa.Name = "txt_placa";
            this.txt_placa.Size = new System.Drawing.Size(180, 26);
            this.txt_placa.TabIndex = 26;
            // 
            // btn_añadir
            // 
            this.btn_añadir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_añadir.Location = new System.Drawing.Point(665, 49);
            this.btn_añadir.Name = "btn_añadir";
            this.btn_añadir.Size = new System.Drawing.Size(89, 35);
            this.btn_añadir.TabIndex = 24;
            this.btn_añadir.Text = "Añadir";
            this.btn_añadir.UseVisualStyleBackColor = true;
            this.btn_añadir.Click += new System.EventHandler(this.btn_añadir_Click);
            // 
            // dG_OperMaq
            // 
            this.dG_OperMaq.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dG_OperMaq.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_OperMaq.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maquinariaAdd,
            this.placaAdd,
            this.eliminar});
            this.dG_OperMaq.Location = new System.Drawing.Point(402, 149);
            this.dG_OperMaq.Name = "dG_OperMaq";
            this.dG_OperMaq.Size = new System.Drawing.Size(352, 229);
            this.dG_OperMaq.TabIndex = 21;
            this.dG_OperMaq.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_botondG);
            this.dG_OperMaq.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dG_OperMaq_CellContentClick);
            // 
            // maquinariaAdd
            // 
            this.maquinariaAdd.HeaderText = "TIPO";
            this.maquinariaAdd.Name = "maquinariaAdd";
            this.maquinariaAdd.ReadOnly = true;
            // 
            // placaAdd
            // 
            this.placaAdd.HeaderText = "PLACA";
            this.placaAdd.Name = "placaAdd";
            this.placaAdd.ReadOnly = true;
            // 
            // eliminar
            // 
            this.eliminar.HeaderText = "";
            this.eliminar.Name = "eliminar";
            // 
            // Form_Alquiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(834, 537);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_cancelar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Alquiler";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alquiler";
            this.Load += new System.EventHandler(this.Form_Alquiler_Load);
            this.tabControl1.ResumeLayout(false);
            this.tPg_DC.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cliente)).EndInit();
            this.tPg_DA.ResumeLayout(false);
            this.tPg_DA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dG_maquinas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dG_OperMaq)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtn_ci;
        private System.Windows.Forms.RadioButton rbtn_ruc;
        private System.Windows.Forms.TextBox txt_ident;
        private System.Windows.Forms.Label lbl_costo;
        private System.Windows.Forms.TextBox txt_costoHora;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_Alquilar;
        private System.Windows.Forms.Label lbl_fecha;
        private System.Windows.Forms.DateTimePicker dateTP_alquiler;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tPg_DC;
        private System.Windows.Forms.TabPage tPg_DA;
        private System.Windows.Forms.DataGridView dG_OperMaq;
        private System.Windows.Forms.Button btn_añadir;
        private System.Windows.Forms.TextBox txt_placa;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.TextBox txt_telefono;
        private System.Windows.Forms.Label lbl_telefono;
        private System.Windows.Forms.TextBox txt_direcc;
        private System.Windows.Forms.Label lbl_direccion;
        private System.Windows.Forms.Label lbl_nombre;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.DataGridView dG_maquinas;
        private System.Windows.Forms.TextBox txt_tipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.DataGridView dgv_cliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_tipoFiltro;
        private System.Windows.Forms.TextBox txt_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn maquinariaAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn placaAdd;
        private System.Windows.Forms.DataGridViewButtonColumn eliminar;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn placa;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.TextBox txt_filtro;
        private System.Windows.Forms.Label label3;

    }
}