﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class ClienteNat : DataBase
    {

        public string ced = "";
        public string nombre = "";
        public string direc = "";
        public string telf = "";
        public string email = "";
        public parametros []prm;

        public ClienteNat()
        {
        }
        public ClienteNat(string ci)
        {
            ced = ci;
        }
        public ClienteNat(string ci, string nomb, string d, string tel, string mail)
        {
            ced = ci;
            nombre = nomb;
            direc = d;
            telf = tel;
            email = mail;
        }


        /*Instroduce el STORED PROCEDURES especifico de Operador*/
        public bool leerClienteNat()
        {
            return ReadTable("GET_CLIENTNATURAL");
        }

        public bool agregarClienteNat()
        {
            prm = new parametros[5];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("cedula", ced);
            prm[1] = new parametros("nombre", nombre);
            prm[2] = new parametros("direccion", direc);
            prm[3] = new parametros("telefono", telf);
            prm[4] = new parametros("email", email);

            return WriteTable("SET_CLIENTENATURAL", prm);
        }
        public bool buscarClientNat()
        {
            prm = new parametros[1];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("cedula", ced);
            return WriteTable("FILTRO_CLIENTENAT", prm);
        }

        public bool modificarClienteNat()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_cedula", ced);
            prm[1] = new parametros("in_nombre", nombre);
            prm[2] = new parametros("in_direccion", direc);
            prm[3] = new parametros("in_telefono", telf);
            prm[4] = new parametros("in_email", email);

            return WriteTable("MOD_CLIENTENATURAL", prm);
        }

        public bool eliminarClienteNat()
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_cedula", ced);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_CLIENTENATURAL", prm);
        }

    }
}
