﻿namespace EquirentCS
{
    partial class Form_Planillajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gB_palnillajes = new System.Windows.Forms.GroupBox();
            this.dG_planillajes = new System.Windows.Forms.DataGridView();
            this.cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alquiler = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_fecha = new System.Windows.Forms.Label();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.btn_modificar = new System.Windows.Forms.Button();
            this.txt_ci = new System.Windows.Forms.TextBox();
            this.lbl_maq = new System.Windows.Forms.Label();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.lbl_horas = new System.Windows.Forms.Label();
            this.txt_maq = new System.Windows.Forms.TextBox();
            this.lbl_ci = new System.Windows.Forms.Label();
            this.txt_horas = new System.Windows.Forms.TextBox();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.btn_salir = new System.Windows.Forms.Button();
            this.txt_alq = new System.Windows.Forms.TextBox();
            this.lbl_alq = new System.Windows.Forms.Label();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.gB_palnillajes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dG_planillajes)).BeginInit();
            this.SuspendLayout();
            // 
            // gB_palnillajes
            // 
            this.gB_palnillajes.AutoSize = true;
            this.gB_palnillajes.Controls.Add(this.dG_planillajes);
            this.gB_palnillajes.Location = new System.Drawing.Point(4, 112);
            this.gB_palnillajes.Name = "gB_palnillajes";
            this.gB_palnillajes.Size = new System.Drawing.Size(569, 221);
            this.gB_palnillajes.TabIndex = 0;
            this.gB_palnillajes.TabStop = false;
            // 
            // dG_planillajes
            // 
            this.dG_planillajes.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dG_planillajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_planillajes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cedula,
            this.placa,
            this.alquiler,
            this.horas,
            this.fecha});
            this.dG_planillajes.Location = new System.Drawing.Point(11, 12);
            this.dG_planillajes.Name = "dG_planillajes";
            this.dG_planillajes.Size = new System.Drawing.Size(547, 190);
            this.dG_planillajes.TabIndex = 0;
            this.dG_planillajes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_autollenado);

            // 
            // cedula
            // 
            this.cedula.DataPropertyName = "cedula";
            this.cedula.HeaderText = "Cédula";
            this.cedula.Name = "cedula";
            // 
            // placa
            // 
            this.placa.DataPropertyName = "placa";
            this.placa.HeaderText = "Máquina";
            this.placa.Name = "placa";
            // 
            // alquiler
            // 
            this.alquiler.DataPropertyName = "codigoAlquiler";
            this.alquiler.HeaderText = "Alquiler";
            this.alquiler.Name = "alquiler";
            // 
            // horas
            // 
            this.horas.DataPropertyName = "horas";
            this.horas.HeaderText = "Horas";
            this.horas.Name = "horas";
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            // 
            // lbl_fecha
            // 
            this.lbl_fecha.AutoSize = true;
            this.lbl_fecha.Location = new System.Drawing.Point(226, 74);
            this.lbl_fecha.Name = "lbl_fecha";
            this.lbl_fecha.Size = new System.Drawing.Size(37, 13);
            this.lbl_fecha.TabIndex = 57;
            this.lbl_fecha.Text = "Fecha";
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Location = new System.Drawing.Point(466, 80);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminar.TabIndex = 55;
            this.btn_eliminar.Text = "ELIMINAR";
            this.btn_eliminar.UseVisualStyleBackColor = true;
            // 
            // btn_modificar
            // 
            this.btn_modificar.Location = new System.Drawing.Point(466, 51);
            this.btn_modificar.Name = "btn_modificar";
            this.btn_modificar.Size = new System.Drawing.Size(75, 23);
            this.btn_modificar.TabIndex = 54;
            this.btn_modificar.Text = "MODIFICAR";
            this.btn_modificar.UseVisualStyleBackColor = true;
            this.btn_modificar.Click += new System.EventHandler(this.btn_modificar_Click);
            // 
            // txt_ci
            // 
            this.txt_ci.Location = new System.Drawing.Point(100, 22);
            this.txt_ci.MaxLength = 10;
            this.txt_ci.Name = "txt_ci";
            this.txt_ci.Size = new System.Drawing.Size(100, 20);
            this.txt_ci.TabIndex = 48;
            // 
            // lbl_maq
            // 
            this.lbl_maq.AutoSize = true;
            this.lbl_maq.Location = new System.Drawing.Point(27, 51);
            this.lbl_maq.Name = "lbl_maq";
            this.lbl_maq.Size = new System.Drawing.Size(48, 13);
            this.lbl_maq.TabIndex = 53;
            this.lbl_maq.Text = "Máquina";
            // 
            // btn_guardar
            // 
            this.btn_guardar.Location = new System.Drawing.Point(466, 22);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(75, 23);
            this.btn_guardar.TabIndex = 47;
            this.btn_guardar.Text = "GUARDAR";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // lbl_horas
            // 
            this.lbl_horas.AutoSize = true;
            this.lbl_horas.Location = new System.Drawing.Point(226, 29);
            this.lbl_horas.Name = "lbl_horas";
            this.lbl_horas.Size = new System.Drawing.Size(35, 13);
            this.lbl_horas.TabIndex = 52;
            this.lbl_horas.Text = "Horas";
            // 
            // txt_maq
            // 
            this.txt_maq.Location = new System.Drawing.Point(100, 48);
            this.txt_maq.MaxLength = 10;
            this.txt_maq.Name = "txt_maq";
            this.txt_maq.Size = new System.Drawing.Size(100, 20);
            this.txt_maq.TabIndex = 49;
            // 
            // lbl_ci
            // 
            this.lbl_ci.AutoSize = true;
            this.lbl_ci.Location = new System.Drawing.Point(27, 25);
            this.lbl_ci.Name = "lbl_ci";
            this.lbl_ci.Size = new System.Drawing.Size(20, 13);
            this.lbl_ci.TabIndex = 51;
            this.lbl_ci.Text = "C.I";
            // 
            // txt_horas
            // 
            this.txt_horas.Location = new System.Drawing.Point(267, 26);
            this.txt_horas.MaxLength = 4;
            this.txt_horas.Name = "txt_horas";
            this.txt_horas.Size = new System.Drawing.Size(105, 20);
            this.txt_horas.TabIndex = 50;
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Location = new System.Drawing.Point(355, 339);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpiar.TabIndex = 59;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(482, 339);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 58;
            this.btn_salir.Text = "SALIR";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // txt_alq
            // 
            this.txt_alq.Location = new System.Drawing.Point(100, 74);
            this.txt_alq.MaxLength = 7;
            this.txt_alq.Name = "txt_alq";
            this.txt_alq.Size = new System.Drawing.Size(100, 20);
            this.txt_alq.TabIndex = 60;
            // 
            // lbl_alq
            // 
            this.lbl_alq.AutoSize = true;
            this.lbl_alq.Location = new System.Drawing.Point(27, 77);
            this.lbl_alq.Name = "lbl_alq";
            this.lbl_alq.Size = new System.Drawing.Size(41, 13);
            this.lbl_alq.TabIndex = 61;
            this.lbl_alq.Text = "Alquiler";
            // 
            // dtp
            // 
            this.dtp.CustomFormat = "yyyy-MM-dd";
            this.dtp.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Location = new System.Drawing.Point(267, 71);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(105, 20);
            this.dtp.TabIndex = 62;
            this.dtp.Value = new System.DateTime(2015, 9, 1, 1, 6, 43, 0);
            // 
            // Form_Planillajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(574, 369);
            this.Controls.Add(this.dtp);
            this.Controls.Add(this.lbl_alq);
            this.Controls.Add(this.txt_alq);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.lbl_fecha);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_modificar);
            this.Controls.Add(this.txt_ci);
            this.Controls.Add(this.lbl_maq);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.lbl_horas);
            this.Controls.Add(this.txt_maq);
            this.Controls.Add(this.lbl_ci);
            this.Controls.Add(this.txt_horas);
            this.Controls.Add(this.gB_palnillajes);
            this.Name = "Form_Planillajes";
            this.Text = "Planillajes";
            this.Load += new System.EventHandler(this.Form_Planillajes_Load);
            this.gB_palnillajes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dG_planillajes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gB_palnillajes;
        private System.Windows.Forms.DataGridView dG_planillajes;
        private System.Windows.Forms.Label lbl_fecha;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Button btn_modificar;
        private System.Windows.Forms.TextBox txt_ci;
        private System.Windows.Forms.Label lbl_maq;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Label lbl_horas;
        private System.Windows.Forms.TextBox txt_maq;
        private System.Windows.Forms.Label lbl_ci;
        private System.Windows.Forms.TextBox txt_horas;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.TextBox txt_alq;
        private System.Windows.Forms.Label lbl_alq;
        private System.Windows.Forms.DateTimePicker dtp;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn placa;
        private System.Windows.Forms.DataGridViewTextBoxColumn alquiler;
        private System.Windows.Forms.DataGridViewTextBoxColumn horas;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
    }
}