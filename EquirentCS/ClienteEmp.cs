﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class ClienteEmp : DataBase
    {

        public string ruc = "";
        public string nombre = "";
        public string direc = "";
        public string telf = "";
        public string email = "";
        public parametros []prm;

        public ClienteEmp()
        {
        }

        public ClienteEmp(string r, string nomb, string d, string tel, string mail)
        {
            ruc = r;
            nombre = nomb;
            direc = d;
            telf = tel;
            email = mail;
        }

        /*constructor para busqueda por FILTRO*/
        public ClienteEmp(string id)
        {
            ruc = id;
        }

        public bool buscarClienteEmp()
        {
            prm = new parametros[1];
           /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("ruc",ruc);
            return WriteTable("FILTRO_CLIENTE_EMP",prm);
        }


        /*Instroduce el STORED PROCEDURES especifico de Operador*/
        public bool leerClienteEmp()
        {
            return ReadTable("GET_CLIENTEMPRESA");
        }

        public bool agregarClienteEmp()
        {
            prm = new parametros[5];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("ruc", ruc);
            prm[1] = new parametros("nombre", nombre);
            prm[2] = new parametros("direccion", direc);
            prm[3] = new parametros("telefono", telf);
            prm[4] = new parametros("email", email);

            return WriteTable("SET_CLIENTEEMPRESA", prm);
        }

        public bool modificarClienteEmp()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_ruc", ruc);
            prm[1] = new parametros("in_nombre", nombre);
            prm[2] = new parametros("in_direccion", direc);
            prm[3] = new parametros("in_telefono", telf);
            prm[4] = new parametros("in_email", email);

            return WriteTable("MOD_CLIENTEEMPRESA", prm);
        }

        public bool eliminarClienteEmp()
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_ruc", ruc);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_CLIENTEEMPRESA", prm);
        }

    }
}
