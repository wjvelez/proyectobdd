﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class alquilerMaquinas : DataBase
    {

       public  string codigo_A_M;
       public  string placa_A_M;
       public bool devuelto_A_M;
       public string fecha_u;
       public parametros[] prm;

       public alquilerMaquinas()
       { }


        /*Constructor al realizar por primera vez el alquiler */
        public alquilerMaquinas(string codigo, string placa)
        {
            codigo_A_M = codigo;
            placa_A_M = placa;
        }

        public bool agregarAlquilerMaquinaDefault()
        {
            prm = new parametros[2];
            prm[0]= new parametros("in_codigoAlquiler",codigo_A_M);
            prm[1]=new parametros("in_placa",placa_A_M);
            return WriteTable("SET_ALQUILER_MAQUINA_DEFAULT", prm);
        }



        // elimina fecha de ultimo pago
        
        public bool devolverAlquilerMaquina()
        {     
            prm = new parametros[2];
            prm[0] = new parametros("in_codigoAlquiler", codigo_A_M);
            prm[1] = new parametros("in_placa", placa_A_M);
            return WriteTable("MOD_DEVUELVE_ALQUILER_MAQUINA", prm);

            //buscar placa y codAlquiler
            //cambia el estado de devuelto
            //Si ya se devolvio la fecha ultimo pago no cambia
        }



    }
}
