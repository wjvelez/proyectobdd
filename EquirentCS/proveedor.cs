﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class proveedor :DataBase
    {

        public string ruc = "";
        public string nombre = "";
        public string direccion = "";
        public string telefono = "";
        public parametros [] prm;
        
        public proveedor()
        { 
        }

        public proveedor(string ru, string nom,  string dir, string tel)
        {
         ruc = ru;
         nombre = nom;
         direccion = dir;
         telefono = tel;
        }
        public bool leerProveedor()
        {
            return ReadTable("GET_PROVEEDOR");
        }


        public bool agregarProveedor()
        {
            prm = new parametros[4];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("ruc", ruc);
            prm[1] = new parametros("nombre", nombre);
            prm[2] = new parametros("direccion", direccion);
            prm[3] = new parametros("telefono", telefono);

            return WriteTable("SET_PROVEEDOR", prm);
        }

        public bool modificarProveedor()
        {
            prm = new parametros[4];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_ruc", ruc);
            prm[1] = new parametros("in_nombre", nombre);
            prm[2] = new parametros("in_direccion", direccion);
            prm[3] = new parametros("in_telefono", telefono);

            return WriteTable("MOD_PROVEEDOR", prm);
        }

        public bool eliminarProveedor()
        {
            prm = new parametros[1];



            prm[0] = new parametros("in_ruc", ruc);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_PROVEEDOR", prm);

        }

    }
}
