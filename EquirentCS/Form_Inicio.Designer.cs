﻿namespace EquirentCS
{
    partial class Form_Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_nuevoAlquiler = new System.Windows.Forms.Button();
            this.btn_reportes = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.btn_facturar = new System.Windows.Forms.Button();
            this.lbl_usuario = new System.Windows.Forms.Label();
            this.btn_planillajes = new System.Windows.Forms.Button();
            this.btn_administrar = new System.Windows.Forms.Button();
            this.btn_pagosProveedores = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_nuevoAlquiler
            // 
            this.btn_nuevoAlquiler.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_nuevoAlquiler.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_nuevoAlquiler.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_nuevoAlquiler.Location = new System.Drawing.Point(73, 39);
            this.btn_nuevoAlquiler.Name = "btn_nuevoAlquiler";
            this.btn_nuevoAlquiler.Size = new System.Drawing.Size(181, 79);
            this.btn_nuevoAlquiler.TabIndex = 0;
            this.btn_nuevoAlquiler.Text = "NUEVO ALQUILER";
            this.btn_nuevoAlquiler.UseVisualStyleBackColor = false;
            this.btn_nuevoAlquiler.Click += new System.EventHandler(this.btn_Alquiler_Click);
            // 
            // btn_reportes
            // 
            this.btn_reportes.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_reportes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reportes.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_reportes.Location = new System.Drawing.Point(73, 138);
            this.btn_reportes.Name = "btn_reportes";
            this.btn_reportes.Size = new System.Drawing.Size(181, 79);
            this.btn_reportes.TabIndex = 1;
            this.btn_reportes.Text = "REPORTES";
            this.btn_reportes.UseVisualStyleBackColor = false;
            this.btn_reportes.Click += new System.EventHandler(this.btn_reportes_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exit.ForeColor = System.Drawing.Color.Red;
            this.btn_Exit.Location = new System.Drawing.Point(486, 315);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(71, 33);
            this.btn_Exit.TabIndex = 6;
            this.btn_Exit.Text = "SALIR";
            this.btn_Exit.UseVisualStyleBackColor = false;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_facturar
            // 
            this.btn_facturar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_facturar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_facturar.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_facturar.Location = new System.Drawing.Point(304, 39);
            this.btn_facturar.Name = "btn_facturar";
            this.btn_facturar.Size = new System.Drawing.Size(181, 79);
            this.btn_facturar.TabIndex = 3;
            this.btn_facturar.Text = "EMITIR FACTURA";
            this.btn_facturar.UseVisualStyleBackColor = false;
            this.btn_facturar.Click += new System.EventHandler(this.btn_facturar_Click);
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.AutoSize = true;
            this.lbl_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_usuario.Location = new System.Drawing.Point(12, 9);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(131, 16);
            this.lbl_usuario.TabIndex = 0;
            this.lbl_usuario.Text = "Nombre_de_usuario";
            // 
            // btn_planillajes
            // 
            this.btn_planillajes.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_planillajes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_planillajes.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_planillajes.Location = new System.Drawing.Point(304, 230);
            this.btn_planillajes.Name = "btn_planillajes";
            this.btn_planillajes.Size = new System.Drawing.Size(181, 79);
            this.btn_planillajes.TabIndex = 5;
            this.btn_planillajes.Text = "PLANILLAJES";
            this.btn_planillajes.UseVisualStyleBackColor = false;
            this.btn_planillajes.Click += new System.EventHandler(this.btn_planillajes_Click);
            // 
            // btn_administrar
            // 
            this.btn_administrar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_administrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_administrar.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_administrar.Location = new System.Drawing.Point(304, 138);
            this.btn_administrar.Name = "btn_administrar";
            this.btn_administrar.Size = new System.Drawing.Size(181, 79);
            this.btn_administrar.TabIndex = 4;
            this.btn_administrar.Text = "ADMINISTRADOR DE BASE";
            this.btn_administrar.UseVisualStyleBackColor = false;
            this.btn_administrar.Click += new System.EventHandler(this.btn_administrar_Click);
            // 
            // btn_pagosProveedores
            // 
            this.btn_pagosProveedores.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btn_pagosProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_pagosProveedores.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_pagosProveedores.Location = new System.Drawing.Point(73, 230);
            this.btn_pagosProveedores.Name = "btn_pagosProveedores";
            this.btn_pagosProveedores.Size = new System.Drawing.Size(181, 79);
            this.btn_pagosProveedores.TabIndex = 2;
            this.btn_pagosProveedores.Text = "PAGOS A PROVEEDORES";
            this.btn_pagosProveedores.UseVisualStyleBackColor = false;
            this.btn_pagosProveedores.Click += new System.EventHandler(this.btn_pagosProveedores_Click);
            // 
            // Form_Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(569, 360);
            this.Controls.Add(this.btn_administrar);
            this.Controls.Add(this.btn_pagosProveedores);
            this.Controls.Add(this.btn_planillajes);
            this.Controls.Add(this.lbl_usuario);
            this.Controls.Add(this.btn_facturar);
            this.Controls.Add(this.btn_nuevoAlquiler);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_reportes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Inicio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Inicio_FormClosing);
            this.Load += new System.EventHandler(this.Form_Inicio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_reportes;
        private System.Windows.Forms.Button btn_nuevoAlquiler;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_facturar;
        private System.Windows.Forms.Label lbl_usuario;
        private System.Windows.Forms.Button btn_planillajes;
        private System.Windows.Forms.Button btn_administrar;
        private System.Windows.Forms.Button btn_pagosProveedores;
    }
}