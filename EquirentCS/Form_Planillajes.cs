﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_Planillajes : Form
    {

        Planillaje pl;
        Validaciones valida = new Validaciones();
        DialogResult confirmacion;

        public Form_Planillajes()
        {
            InitializeComponent();
        }

        public void cargar()
        {
            if (pl.leerPlanillaje())
            {
                dG_planillajes.DataSource = pl.d_tb;
            }
            else
            {
                MessageBox.Show("Error en lectura de planillaje");
            }
        } 


        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void Form_Planillajes_Load(object sender, EventArgs e)
        {
            pl = new Planillaje();
            cargar();
        }

        private void txt_ci_KeyPress(object sender, KeyPressEventArgs e)
        {

            char c = e.KeyChar;
            string num = "" + c;

            txt_ci.MaxLength = 10;

            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;

                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                    txt_ci.Focus();

            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.txt_ci.Clear();
            //this.dtp.Select();
            this.txt_horas.Clear();
            this.txt_maq.Clear();
            this.txt_alq.Clear();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear planillaje con lo valores a guardar
                // string ci, string maq, string horas, string f, string cod
                // public Planillaje(string ci, string maq, string horas, string f, string cod)
                pl = new Planillaje(txt_ci.Text, txt_maq.Text, txt_horas.Text, dtp.Text, txt_alq.Text);

                //Lo va guardar en la base de datos
                pl.agregarPlanillaje();

                //refresca la tabla
                cargar();

            }
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pl = new Planillaje(txt_ci.Text, txt_maq.Text, txt_horas.Text, dtp.Text, txt_alq.Text);

                //Lo va guardar en la base de datos
                pl.modificarPlanillaje();

                //refresca la tabla
                cargar();
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pl = new Planillaje(txt_ci.Text, txt_maq.Text, txt_horas.Text, dtp.Text, txt_alq.Text);
                //Lo va guardar en la base de datos
                pl.eliminarPlanillaje();

                //refresca la tabla
                cargar();
            }
        }

        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {
            //txt_fecha.Text = dG_planillajes.CurrentRow.Cells[4].Value.ToString();
            dtp.Text = dG_planillajes.CurrentRow.Cells[4].Value.ToString();
            txt_horas.Text = dG_planillajes.CurrentRow.Cells[3].Value.ToString();
            txt_ci.Text = dG_planillajes.CurrentRow.Cells[0].Value.ToString();
            txt_maq.Text = dG_planillajes.CurrentRow.Cells[1].Value.ToString();
            txt_alq.Text = dG_planillajes.CurrentRow.Cells[2].Value.ToString();

        }







        
    }
}
