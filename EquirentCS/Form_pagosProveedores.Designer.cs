﻿namespace EquirentCS
{
    partial class Form_PagosProveedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gB_pagosP = new System.Windows.Forms.GroupBox();
            this.dG_pagos = new System.Windows.Forms.DataGridView();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ruc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.btn_salir = new System.Windows.Forms.Button();
            this.lbl_det = new System.Windows.Forms.Label();
            this.txt_det = new System.Windows.Forms.TextBox();
            this.lbl_fecha = new System.Windows.Forms.Label();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.btn_modificar = new System.Windows.Forms.Button();
            this.txt_ruc = new System.Windows.Forms.TextBox();
            this.lbl_maq = new System.Windows.Forms.Label();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.lbl_fact = new System.Windows.Forms.Label();
            this.txt_maq = new System.Windows.Forms.TextBox();
            this.lbl_ci = new System.Windows.Forms.Label();
            this.txt_fact = new System.Windows.Forms.TextBox();
            this.lbl_tipo = new System.Windows.Forms.Label();
            this.txt_tipo = new System.Windows.Forms.TextBox();
            this.lbl_valor = new System.Windows.Forms.Label();
            this.txt_valor = new System.Windows.Forms.TextBox();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.gB_pagosP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dG_pagos)).BeginInit();
            this.SuspendLayout();
            // 
            // gB_pagosP
            // 
            this.gB_pagosP.Controls.Add(this.dG_pagos);
            this.gB_pagosP.Location = new System.Drawing.Point(12, 101);
            this.gB_pagosP.Name = "gB_pagosP";
            this.gB_pagosP.Size = new System.Drawing.Size(757, 210);
            this.gB_pagosP.TabIndex = 0;
            this.gB_pagosP.TabStop = false;
            // 
            // dG_pagos
            // 
            this.dG_pagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_pagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fecha,
            this.codigoFactura,
            this.ruc,
            this.detalle,
            this.placa,
            this.tipo,
            this.valor});
            this.dG_pagos.Location = new System.Drawing.Point(6, 19);
            this.dG_pagos.Name = "dG_pagos";
            this.dG_pagos.Size = new System.Drawing.Size(745, 185);
            this.dG_pagos.TabIndex = 0;
            this.dG_pagos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_autollenado);
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fecha.Width = 80;
            // 
            // codigoFactura
            // 
            this.codigoFactura.DataPropertyName = "codigo_factura";
            this.codigoFactura.HeaderText = "Cod. Factura";
            this.codigoFactura.Name = "codigoFactura";
            this.codigoFactura.Width = 80;
            // 
            // ruc
            // 
            this.ruc.DataPropertyName = "ruc";
            this.ruc.HeaderText = "RUC";
            this.ruc.Name = "ruc";
            this.ruc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ruc.Width = 80;
            // 
            // detalle
            // 
            this.detalle.DataPropertyName = "detalle";
            this.detalle.HeaderText = "Detalle";
            this.detalle.Name = "detalle";
            this.detalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.detalle.Width = 200;
            // 
            // placa
            // 
            this.placa.DataPropertyName = "placa";
            this.placa.HeaderText = "Placa";
            this.placa.Name = "placa";
            this.placa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.placa.Width = 70;
            // 
            // tipo
            // 
            this.tipo.DataPropertyName = "tipo";
            this.tipo.HeaderText = "Tipo";
            this.tipo.Name = "tipo";
            this.tipo.Width = 120;
            // 
            // valor
            // 
            this.valor.DataPropertyName = "valor";
            this.valor.HeaderText = "Valor";
            this.valor.Name = "valor";
            this.valor.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valor.Width = 70;
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Location = new System.Drawing.Point(527, 317);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpiar.TabIndex = 1;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(657, 317);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 2;
            this.btn_salir.Text = "SALIR";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // lbl_det
            // 
            this.lbl_det.AutoSize = true;
            this.lbl_det.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_det.Location = new System.Drawing.Point(231, 23);
            this.lbl_det.Name = "lbl_det";
            this.lbl_det.Size = new System.Drawing.Size(46, 15);
            this.lbl_det.TabIndex = 74;
            this.lbl_det.Text = "Detalle";
            // 
            // txt_det
            // 
            this.txt_det.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_det.Location = new System.Drawing.Point(307, 19);
            this.txt_det.MaxLength = 7;
            this.txt_det.Name = "txt_det";
            this.txt_det.Size = new System.Drawing.Size(295, 21);
            this.txt_det.TabIndex = 73;
            this.txt_det.TextChanged += new System.EventHandler(this.txt_det_TextChanged);
            this.txt_det.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_det_KeyPress);
            // 
            // lbl_fecha
            // 
            this.lbl_fecha.AutoSize = true;
            this.lbl_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fecha.Location = new System.Drawing.Point(438, 62);
            this.lbl_fecha.Name = "lbl_fecha";
            this.lbl_fecha.Size = new System.Drawing.Size(41, 15);
            this.lbl_fecha.TabIndex = 72;
            this.lbl_fecha.Text = "Fecha";
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Location = new System.Drawing.Point(646, 78);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminar.TabIndex = 71;
            this.btn_eliminar.Text = "ELIMINAR";
            this.btn_eliminar.UseVisualStyleBackColor = true;
            this.btn_eliminar.Click += new System.EventHandler(this.btn_eliminar_Click);
            // 
            // btn_modificar
            // 
            this.btn_modificar.Location = new System.Drawing.Point(646, 49);
            this.btn_modificar.Name = "btn_modificar";
            this.btn_modificar.Size = new System.Drawing.Size(75, 23);
            this.btn_modificar.TabIndex = 70;
            this.btn_modificar.Text = "MODIFICAR";
            this.btn_modificar.UseVisualStyleBackColor = true;
            this.btn_modificar.Click += new System.EventHandler(this.btn_modificar_Click);
            // 
            // txt_ruc
            // 
            this.txt_ruc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ruc.Location = new System.Drawing.Point(94, 46);
            this.txt_ruc.MaxLength = 10;
            this.txt_ruc.Name = "txt_ruc";
            this.txt_ruc.Size = new System.Drawing.Size(110, 21);
            this.txt_ruc.TabIndex = 64;
            this.txt_ruc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ruc_KeyPress);
            // 
            // lbl_maq
            // 
            this.lbl_maq.AutoSize = true;
            this.lbl_maq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maq.Location = new System.Drawing.Point(231, 49);
            this.lbl_maq.Name = "lbl_maq";
            this.lbl_maq.Size = new System.Drawing.Size(56, 15);
            this.lbl_maq.TabIndex = 69;
            this.lbl_maq.Text = "Máquina";
            // 
            // btn_guardar
            // 
            this.btn_guardar.Location = new System.Drawing.Point(646, 20);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(75, 23);
            this.btn_guardar.TabIndex = 63;
            this.btn_guardar.Text = "GUARDAR";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // lbl_fact
            // 
            this.lbl_fact.AutoSize = true;
            this.lbl_fact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fact.Location = new System.Drawing.Point(15, 22);
            this.lbl_fact.Name = "lbl_fact";
            this.lbl_fact.Size = new System.Drawing.Size(48, 15);
            this.lbl_fact.TabIndex = 68;
            this.lbl_fact.Text = "Factura";
            // 
            // txt_maq
            // 
            this.txt_maq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_maq.Location = new System.Drawing.Point(307, 46);
            this.txt_maq.MaxLength = 10;
            this.txt_maq.Name = "txt_maq";
            this.txt_maq.Size = new System.Drawing.Size(110, 21);
            this.txt_maq.TabIndex = 65;
            // 
            // lbl_ci
            // 
            this.lbl_ci.AutoSize = true;
            this.lbl_ci.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ci.Location = new System.Drawing.Point(15, 49);
            this.lbl_ci.Name = "lbl_ci";
            this.lbl_ci.Size = new System.Drawing.Size(33, 15);
            this.lbl_ci.TabIndex = 67;
            this.lbl_ci.Text = "RUC";
            // 
            // txt_fact
            // 
            this.txt_fact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_fact.Location = new System.Drawing.Point(94, 20);
            this.txt_fact.MaxLength = 4;
            this.txt_fact.Name = "txt_fact";
            this.txt_fact.Size = new System.Drawing.Size(110, 21);
            this.txt_fact.TabIndex = 66;
            this.txt_fact.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_fact_KeyPress);
            // 
            // lbl_tipo
            // 
            this.lbl_tipo.AutoSize = true;
            this.lbl_tipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tipo.Location = new System.Drawing.Point(231, 75);
            this.lbl_tipo.Name = "lbl_tipo";
            this.lbl_tipo.Size = new System.Drawing.Size(31, 15);
            this.lbl_tipo.TabIndex = 77;
            this.lbl_tipo.Text = "Tipo";
            // 
            // txt_tipo
            // 
            this.txt_tipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tipo.Location = new System.Drawing.Point(307, 72);
            this.txt_tipo.MaxLength = 30;
            this.txt_tipo.Name = "txt_tipo";
            this.txt_tipo.Size = new System.Drawing.Size(110, 21);
            this.txt_tipo.TabIndex = 76;
            // 
            // lbl_valor
            // 
            this.lbl_valor.AutoSize = true;
            this.lbl_valor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_valor.Location = new System.Drawing.Point(15, 75);
            this.lbl_valor.Name = "lbl_valor";
            this.lbl_valor.Size = new System.Drawing.Size(35, 15);
            this.lbl_valor.TabIndex = 79;
            this.lbl_valor.Text = "Valor";
            // 
            // txt_valor
            // 
            this.txt_valor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_valor.Location = new System.Drawing.Point(94, 72);
            this.txt_valor.MaxLength = 10;
            this.txt_valor.Name = "txt_valor";
            this.txt_valor.Size = new System.Drawing.Size(110, 21);
            this.txt_valor.TabIndex = 78;
            this.txt_valor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_valor_KeyPress);
            this.txt_valor.Leave += new System.EventHandler(this.txt_valor_Leave);
            // 
            // dtp
            // 
            this.dtp.CustomFormat = "yyyy-MM-dd";
            this.dtp.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Location = new System.Drawing.Point(497, 58);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(105, 20);
            this.dtp.TabIndex = 80;
            this.dtp.Value = new System.DateTime(2015, 9, 1, 1, 6, 43, 0);
            // 
            // Form_PagosProveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(773, 352);
            this.Controls.Add(this.dtp);
            this.Controls.Add(this.lbl_valor);
            this.Controls.Add(this.lbl_tipo);
            this.Controls.Add(this.txt_tipo);
            this.Controls.Add(this.txt_valor);
            this.Controls.Add(this.lbl_det);
            this.Controls.Add(this.txt_det);
            this.Controls.Add(this.lbl_fecha);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_modificar);
            this.Controls.Add(this.txt_ruc);
            this.Controls.Add(this.lbl_maq);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.lbl_fact);
            this.Controls.Add(this.txt_maq);
            this.Controls.Add(this.lbl_ci);
            this.Controls.Add(this.txt_fact);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.gB_pagosP);
            this.Name = "Form_PagosProveedores";
            this.Text = "Pagos a Proveedores";
            this.Load += new System.EventHandler(this.Form_PagoProoved_Load);
            this.gB_pagosP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dG_pagos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gB_pagosP;
        private System.Windows.Forms.DataGridView dG_pagos;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.Label lbl_det;
        private System.Windows.Forms.TextBox txt_det;
        private System.Windows.Forms.Label lbl_fecha;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.Button btn_modificar;
        private System.Windows.Forms.TextBox txt_ruc;
        private System.Windows.Forms.Label lbl_maq;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Label lbl_fact;
        private System.Windows.Forms.TextBox txt_maq;
        private System.Windows.Forms.Label lbl_ci;
        private System.Windows.Forms.TextBox txt_fact;
        private System.Windows.Forms.Label lbl_tipo;
        private System.Windows.Forms.TextBox txt_tipo;
        private System.Windows.Forms.Label lbl_valor;
        private System.Windows.Forms.TextBox txt_valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn ruc;
        private System.Windows.Forms.DataGridViewTextBoxColumn detalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn placa;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.DateTimePicker dtp;
    }
}