﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace EquirentCS
{
    class Maquina : DataBase
    {
        public string placa = "";
        public string marca="";
        public string tipo="";
        public string horometro="";
        public string modelo="";
        public parametros [] prm;

        public Maquina()
        {

        }
        public Maquina(string tip)
        {
            tipo = tip;
        }
        public Maquina(string pla, string mar, string tip, string hor, string mod)
        {
            placa = pla;
            marca = mar;

            tipo = tip;

            horometro = hor;

            modelo = mod;
        }

        public bool leerMaquina()
        {

            return ReadTable("GET_MAQUINA");
        }

        /*hace una lectura por maquina pero solo mostrando la placa y el tipo. Y ademas si estan disponibles*/
        public bool leerMaquina_P_T()
        {
            return ReadTable("GET_MAQUINASDISPONIBLES");
        }

        //Busca maquina pero por el filtro de TIPO, mostrando PLACA y TIPO
        public bool buscarMaquina_Tipo()
        {
            prm = new parametros[1];
            prm[0] = new parametros("tipoV", tipo);
            return WriteTable("FILTRO_MAQUINA_TIPO",prm);
        }

        public bool agregarMaquina()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("placa", placa);
            prm[1] = new parametros("marca", marca);
            prm[2] = new parametros("tipo", tipo);
            prm[3] = new parametros("horometro", "" + horometro);
            prm[4] = new parametros("modelo", modelo);

            return WriteTable("SET_MAQUINA", prm);
        }

        public bool modificarMaquina()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_placa", placa);
            prm[1] = new parametros("in_marca", marca);
            prm[2] = new parametros("in_tipo", tipo);
            prm[3] = new parametros("in_horometro", "" + horometro);
            prm[4] = new parametros("in_modelo", modelo);

            return WriteTable("MOD_MAQUINA", prm);
        }

        public bool eliminarMaquina()
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_placa", placa);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_MAQUINA", prm);
        }


        // Usado en Form_Facturacion
        // Llama al procedure para obtener el total de horas trabajadas de una maquina
        // dentro de un periodo de tiempo
        // Fechas en el formato 'yyyy-MM-dd'
        // Retorna un único elemento
        public int getHorasTrabajadas(string placa, string f_inicio, string f_fin)
        {
            int value = 0;

            prm = new parametros[3];
            prm[0] = new parametros("placa", placa);
            prm[1] = new parametros("f_inicio", f_inicio);
            prm[2] = new parametros("f_fin", f_fin);

            if (WriteTable("calcularHorasAFacturar", prm))
            {
                DataRow row = this.d_tb.Rows[0];
                string tmp = row["horas_trabajadas"].ToString();

                try
                {
                    Int32.TryParse(tmp, out value);
                }
                catch
                {
                    MessageBox.Show("Ocurrio un error en 'getHorasTrabajadas' ");
                }
            }
            return value;
        }
    }
}
