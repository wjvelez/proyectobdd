﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    // Vista de selección de alquiler en Form_FacturarAlquiler 
    class SeleccionarAlquiler : DataBase
    {
        public string id_cliente = "";
        public int cod_alquiler = 0;
        public string fecha_alquiler = "";

        public parametros[] prm;

        public SeleccionarAlquiler(string cliente)
        {
            this.id_cliente = cliente;
        }

        public SeleccionarAlquiler()
        {
        }

        public bool buscarVistaClienteNat()
        {
            prm = new parametros[1];

            prm[0] = new parametros("cedula", id_cliente);

            return WriteTable("searchViewSeleccionarAlquilerCN", prm);
        }

        public bool buscarVistaClienteEmp()
        {
            prm = new parametros[1];

            prm[0] = new parametros("ruc", id_cliente);

            return WriteTable("searchViewSeleccionarAlquilerEMP", prm);
        }


        public bool buscarVistaCodigo(string codigo)
        {
            prm = new parametros[1];

            if (codigo != "")
            {
                prm[0] = new parametros("codigo", codigo);
            }
            else
            {
                prm[0] = new parametros("codigo", "0");
            }
            return WriteTable("searchViewSeleccionarAlquilerCodigo", prm);

        }

       
    }

    class MaquinariasAlquiladas : DataBase
    {
        public int cod_alquiler = 0;
        public parametros[] prm;

        public MaquinariasAlquiladas()
        {

        }

        public MaquinariasAlquiladas(int codigo)
        {
            this.cod_alquiler = codigo;
        }

        public bool buscarMaquinariasAlquiladas()
        {
            prm = new parametros[1];

            prm[0] = new parametros("codigoAlquiler", "" + cod_alquiler);

            return WriteTable("searchViewSeleccionarMaquina", prm);
        }
    }
}
