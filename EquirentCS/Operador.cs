﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    class Operador : DataBase //EXISTE UN RELACION DE HERENCIA
    {
        public string cedula;
        public string nombre;
        public string apellido;
        public string direccion;
        public string telefono;
        public parametros [] prm;

        public Operador()
        { 
        }

        public Operador(string ced, string nom, string ape, string dir, string tel)
        {
         cedula = ced;
         nombre = nom;
         apellido =ape;
         direccion = dir;
         telefono = tel;
        }

        /*Instroduce el STORED PROCEDURES especifico de Operador*/
        public bool leerOperador()
        {
            return ReadTable("GET_OPERADOR");
        }
            
        public bool agregarOperador()
        {
            prm = new parametros[5];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("cedula",cedula);
            prm[1] = new parametros("nombre",nombre);
            prm[2] = new parametros("apellido",apellido);
            prm[3] = new parametros("direccion",direccion);
            prm[4] = new parametros("telefono",telefono);

            return WriteTable("SET_OPERADOR", prm);
        }

        public bool modificarOperador()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_cedula", cedula);
            prm[1] = new parametros("in_nombre", nombre);
            prm[2] = new parametros("in_apellido", apellido);
            prm[3] = new parametros("in_direccion", direccion);
            prm[4] = new parametros("in_telefono", telefono);

            return WriteTable("MOD_OPERADOR", prm);
        }

        public bool eliminarOperador()
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_cedula", cedula);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_OPERADOR", prm);
        }

    }
}
