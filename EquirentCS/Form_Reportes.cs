﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_Reportes : Form
    {
        public Form_Reportes()
        {
            InitializeComponent();
        }


        private void btn_Salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_ConsultarR_Click(object sender, EventArgs e)
        {
            if (rB_alquileres.Checked == true)
            {
                Form_repAlquileres A = new Form_repAlquileres();
                A.Show();
            }
            else if (rB_facturas.Checked == true)
            {
                Form_repFacturas M = new Form_repFacturas();
                M.Show();
            }
            else if (rB_planillajes.Checked == true)
            {
                Form_repPlanillajes Pl = new Form_repPlanillajes();
                Pl.Show();
            }
            else
                MessageBox.Show("SELECCIONE ANTES EL TIPO");
        }

        private void rB_facturas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rB_planillajes_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gB_tipo_Enter(object sender, EventArgs e)
        {

        }

    }
}
