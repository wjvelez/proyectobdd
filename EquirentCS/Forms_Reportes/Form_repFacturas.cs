﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_repFacturas : Form
    {
        RepFactCN perNat;
        RepFactCE emp;
        DialogResult confirmacion;
        Validaciones valida = new Validaciones();
        
        public Form_repFacturas()
        {
            InitializeComponent();
        }




        private void cb_filtro_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Clear();
            this.rB_emp.Enabled = true;
            this.rB_nat.Enabled = true;
            this.rB_emp.Checked = false;
            this.rB_nat.Checked = false;
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.cb_filtro.SelectedIndex = -1;
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = false;
            this.rB_emp.Checked = false;
            this.rB_nat.Checked = false;
            this.rB_emp.Enabled = false;
            this.rB_nat.Enabled = false;      
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txt_filtro_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.rB_emp.Checked == true)
            {
                // los filtros para empresaaas................
                int ind = this.cb_filtro.SelectedIndex;
                if (ind == 0)
                {
                    //filtrar por nombre
                    if (emp.filtrar_Nombre(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = emp.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente empresa por nombre");
                }
                else if (ind == 1)
                {
                    //filtrar por descripcion
                    if (emp.filtrar_Descrip(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = emp.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente empresa por detalle");
                }
                else if (ind == 2)
                {
                    //filtrar por alquiler
                    if (emp.filtrar_Alquiler(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = emp.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente empresa por alquiler");
                }
                else
                {
                    // filtrar por valor
                    if (emp.filtrar_Valor(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = emp.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente empresa por valor");
                }
            }
            if (this.rB_nat.Checked == true)
            {
                //los filtros  para personas naturales.........
                int ind = this.cb_filtro.SelectedIndex;
                if (ind == 0)
                {
                    //filtrar por nombre
                    if (perNat.filtrar_Nombre(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = perNat.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente persona");
                    
                }
                else if (ind == 1)
                {
                    //filtrar por descripcion
                    if (perNat.filtrar_Descrip(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = perNat.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente nat por detalle");
                }
                else if (ind == 2)
                {
                    //filtrar por alquiler
                    if (perNat.filtrar_Alquiler(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = perNat.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente nat por alquiler");
                }
                else
                {
                    // filtrar por valor
                    if (perNat.filtrar_Valor(this.txt_filtro.Text))
                    {
                        dgv_fact.DataSource = perNat.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar cliente empresa por valor");
                }
            }         
        }

        private void Form_repFacturas_Load(object sender, EventArgs e)
        {
            perNat = new RepFactCN();
            emp = new RepFactCE();

            cargarFacturasCN();


        }

        public void cargarFacturasCN()
        {
            if (perNat.leerFacturas())
            {
                dgv_fact.DataSource = perNat.d_tb;
            }
            else
                MessageBox.Show("ERROR PERSONA");
        }

        private void rB_emp_CheckedChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Enabled = true;
            this.txt_filtro.Clear();
            
        }

        private void rB_nat_CheckedChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Enabled = true;
            this.txt_filtro.Clear();
        }

        private void txt_filtro_TextChanged(object sender, EventArgs e)
        {

        }



    }





}
