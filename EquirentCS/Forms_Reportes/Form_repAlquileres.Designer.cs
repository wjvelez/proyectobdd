﻿namespace EquirentCS
{
    partial class Form_repAlquileres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_salir = new System.Windows.Forms.Button();
            this.dG_RepALQ = new System.Windows.Forms.DataGridView();
            this.gB_alquileres = new System.Windows.Forms.GroupBox();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.lbl_filtrar = new System.Windows.Forms.Label();
            this.cb_filtro = new System.Windows.Forms.ComboBox();
            this.txt_filtro = new System.Windows.Forms.TextBox();
            this.rB_nat = new System.Windows.Forms.RadioButton();
            this.rB_emp = new System.Windows.Forms.RadioButton();
            this.codAlquiler = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaIni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dG_RepALQ)).BeginInit();
            this.gB_alquileres.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(532, 333);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 5;
            this.btn_salir.Text = "SALIR";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // dG_RepALQ
            // 
            this.dG_RepALQ.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dG_RepALQ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_RepALQ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codAlquiler,
            this.fechaIni,
            this.fechaFin});
            this.dG_RepALQ.Location = new System.Drawing.Point(12, 19);
            this.dG_RepALQ.Name = "dG_RepALQ";
            this.dG_RepALQ.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dG_RepALQ.Size = new System.Drawing.Size(583, 236);
            this.dG_RepALQ.TabIndex = 0;
            // 
            // gB_alquileres
            // 
            this.gB_alquileres.Controls.Add(this.dG_RepALQ);
            this.gB_alquileres.Location = new System.Drawing.Point(12, 66);
            this.gB_alquileres.Name = "gB_alquileres";
            this.gB_alquileres.Size = new System.Drawing.Size(611, 261);
            this.gB_alquileres.TabIndex = 0;
            this.gB_alquileres.TabStop = false;
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Location = new System.Drawing.Point(440, 333);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpiar.TabIndex = 4;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // lbl_filtrar
            // 
            this.lbl_filtrar.AutoSize = true;
            this.lbl_filtrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_filtrar.Location = new System.Drawing.Point(17, 26);
            this.lbl_filtrar.Name = "lbl_filtrar";
            this.lbl_filtrar.Size = new System.Drawing.Size(75, 18);
            this.lbl_filtrar.TabIndex = 47;
            this.lbl_filtrar.Text = "Filtrar por:";
            // 
            // cb_filtro
            // 
            this.cb_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cb_filtro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_filtro.FormattingEnabled = true;
            this.cb_filtro.Items.AddRange(new object[] {
            "Nombre",
            "Código de Alquiler"});
            this.cb_filtro.Location = new System.Drawing.Point(98, 23);
            this.cb_filtro.Name = "cb_filtro";
            this.cb_filtro.Size = new System.Drawing.Size(182, 24);
            this.cb_filtro.TabIndex = 46;
            this.cb_filtro.SelectedIndexChanged += new System.EventHandler(this.cb_filtro_SelectedIndexChanged);
            // 
            // txt_filtro
            // 
            this.txt_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txt_filtro.Enabled = false;
            this.txt_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtro.Location = new System.Drawing.Point(314, 23);
            this.txt_filtro.Name = "txt_filtro";
            this.txt_filtro.Size = new System.Drawing.Size(309, 24);
            this.txt_filtro.TabIndex = 45;
            this.txt_filtro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_filtro_KeyUp);
            // 
            // rB_nat
            // 
            this.rB_nat.AutoSize = true;
            this.rB_nat.Enabled = false;
            this.rB_nat.Location = new System.Drawing.Point(179, 53);
            this.rB_nat.Name = "rB_nat";
            this.rB_nat.Size = new System.Drawing.Size(101, 17);
            this.rB_nat.TabIndex = 49;
            this.rB_nat.TabStop = true;
            this.rB_nat.Text = "Persona Natural";
            this.rB_nat.UseVisualStyleBackColor = true;
            this.rB_nat.CheckedChanged += new System.EventHandler(this.rB_nat_CheckedChanged);
            // 
            // rB_emp
            // 
            this.rB_emp.AutoSize = true;
            this.rB_emp.Enabled = false;
            this.rB_emp.Location = new System.Drawing.Point(98, 53);
            this.rB_emp.Name = "rB_emp";
            this.rB_emp.Size = new System.Drawing.Size(66, 17);
            this.rB_emp.TabIndex = 48;
            this.rB_emp.TabStop = true;
            this.rB_emp.Text = "Empresa";
            this.rB_emp.UseVisualStyleBackColor = true;
            this.rB_emp.CheckedChanged += new System.EventHandler(this.rB_emp_CheckedChanged);
            // 
            // codAlquiler
            // 
            this.codAlquiler.DataPropertyName = "codigoAlquiler";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.codAlquiler.DefaultCellStyle = dataGridViewCellStyle7;
            this.codAlquiler.HeaderText = "Código";
            this.codAlquiler.Name = "codAlquiler";
            this.codAlquiler.ReadOnly = true;
            this.codAlquiler.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // fechaIni
            // 
            this.fechaIni.DataPropertyName = "fecha_inicio";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Format = "d";
            dataGridViewCellStyle8.NullValue = null;
            this.fechaIni.DefaultCellStyle = dataGridViewCellStyle8;
            this.fechaIni.HeaderText = "Fecha Inicio";
            this.fechaIni.Name = "fechaIni";
            this.fechaIni.ReadOnly = true;
            this.fechaIni.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // fechaFin
            // 
            this.fechaFin.DataPropertyName = "fecha_fin";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.NullValue = null;
            this.fechaFin.DefaultCellStyle = dataGridViewCellStyle9;
            this.fechaFin.HeaderText = "Fecha Fin";
            this.fechaFin.Name = "fechaFin";
            this.fechaFin.ReadOnly = true;
            this.fechaFin.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Form_repAlquileres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(636, 368);
            this.Controls.Add(this.rB_nat);
            this.Controls.Add(this.rB_emp);
            this.Controls.Add(this.lbl_filtrar);
            this.Controls.Add(this.cb_filtro);
            this.Controls.Add(this.txt_filtro);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.gB_alquileres);
            this.Controls.Add(this.btn_salir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_repAlquileres";
            this.Text = "REPORTE ALQUILERES";
            this.Load += new System.EventHandler(this.Form_Ralquiler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dG_RepALQ)).EndInit();
            this.gB_alquileres.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.DataGridView dG_RepALQ;
        private System.Windows.Forms.GroupBox gB_alquileres;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Label lbl_filtrar;
        private System.Windows.Forms.ComboBox cb_filtro;
        private System.Windows.Forms.TextBox txt_filtro;
        private System.Windows.Forms.RadioButton rB_nat;
        private System.Windows.Forms.RadioButton rB_emp;
        private System.Windows.Forms.DataGridViewTextBoxColumn codAlquiler;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaIni;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaFin;


    }
}