﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class RepFactCE: DataBase
    {
        public string codF = "";
        public string codA = "";
        public string fecha = "";
        public string cliente = "";
        public float valor;
        public string descrip = "";
        public string ruc = "";
        public parametros[] prm;

        public RepFactCE() 
        { 
        }

        public RepFactCE(string codF, string codA, string fecha, string cliente, float valor, string descrip, string ruc)
        {
            this.codF = codF;
            this.codA = codA;
            this.ruc = ruc;
            this.cliente = cliente;
            this.descrip = descrip;
            this.fecha = fecha;
            this.valor = valor;

        }

        // filtra los registros de facturas de empresas por nombre

        public bool filtrar_Nombre(string nombre)
        {
            prm = new parametros[1];
            prm[0] = new parametros("nombre", nombre);
            return WriteTable("FILTRO_REPFACT_CE", prm);
        }

        public bool filtrar_Descrip(string descrip)
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_detalle", descrip);
            return WriteTable("FILTRO_REPFACT_DET_CE", prm);
        }

        public bool filtrar_Alquiler(string alquiler)
        {
            prm = new parametros[1];
            if (alquiler == "")
            {
                alquiler = "0";
            }
            prm[0] = new parametros("in_alq", alquiler);
            return WriteTable("FILTRO_REPFACT_ALQ_CE", prm);
        }

        public bool filtrar_Valor(string valor)
        {
            prm = new parametros[1];
            if (valor == "")
            {
                valor = "0";
            }
            prm[0] = new parametros("in_valor", valor);
            return WriteTable("FILTRO_REPFACT_VAL_CE", prm);
        }

    
    }
}
