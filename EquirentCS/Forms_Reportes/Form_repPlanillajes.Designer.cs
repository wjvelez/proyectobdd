﻿namespace EquirentCS
{
    partial class Form_repPlanillajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gB_planillajes = new System.Windows.Forms.GroupBox();
            this.dgv_plan = new System.Windows.Forms.DataGridView();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horasReales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maquina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_salir = new System.Windows.Forms.Button();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.lbl_filtrar = new System.Windows.Forms.Label();
            this.cb_filtro = new System.Windows.Forms.ComboBox();
            this.txt_filtro = new System.Windows.Forms.TextBox();
            this.dG_trabajdor = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.gB_planillajes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_plan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dG_trabajdor)).BeginInit();
            this.SuspendLayout();
            // 
            // gB_planillajes
            // 
            this.gB_planillajes.Controls.Add(this.dgv_plan);
            this.gB_planillajes.Location = new System.Drawing.Point(9, 55);
            this.gB_planillajes.Name = "gB_planillajes";
            this.gB_planillajes.Size = new System.Drawing.Size(455, 264);
            this.gB_planillajes.TabIndex = 0;
            this.gB_planillajes.TabStop = false;
            // 
            // dgv_plan
            // 
            this.dgv_plan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_plan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fecha,
            this.horasReales,
            this.operador,
            this.maquina});
            this.dgv_plan.Location = new System.Drawing.Point(6, 10);
            this.dgv_plan.Name = "dgv_plan";
            this.dgv_plan.Size = new System.Drawing.Size(443, 248);
            this.dgv_plan.TabIndex = 0;
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fecha.Width = 80;
            // 
            // horasReales
            // 
            this.horasReales.DataPropertyName = "horas";
            this.horasReales.HeaderText = "Horas Reales";
            this.horasReales.Name = "horasReales";
            this.horasReales.ReadOnly = true;
            // 
            // operador
            // 
            this.operador.DataPropertyName = "cedula";
            this.operador.HeaderText = "Operador";
            this.operador.Name = "operador";
            this.operador.ReadOnly = true;
            this.operador.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.operador.Width = 120;
            // 
            // maquina
            // 
            this.maquina.DataPropertyName = "placa";
            this.maquina.HeaderText = "Máquina";
            this.maquina.Name = "maquina";
            this.maquina.ReadOnly = true;
            this.maquina.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(383, 334);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 3;
            this.btn_salir.Text = "SALIR";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Location = new System.Drawing.Point(284, 334);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpiar.TabIndex = 21;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // lbl_filtrar
            // 
            this.lbl_filtrar.AutoSize = true;
            this.lbl_filtrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_filtrar.Location = new System.Drawing.Point(12, 18);
            this.lbl_filtrar.Name = "lbl_filtrar";
            this.lbl_filtrar.Size = new System.Drawing.Size(75, 18);
            this.lbl_filtrar.TabIndex = 53;
            this.lbl_filtrar.Text = "Filtrar por:";
            // 
            // cb_filtro
            // 
            this.cb_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cb_filtro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_filtro.FormattingEnabled = true;
            this.cb_filtro.Items.AddRange(new object[] {
            "Operador",
            "Máquina",
            "Alquiler"});
            this.cb_filtro.Location = new System.Drawing.Point(99, 15);
            this.cb_filtro.Name = "cb_filtro";
            this.cb_filtro.Size = new System.Drawing.Size(123, 24);
            this.cb_filtro.TabIndex = 52;
            this.cb_filtro.SelectedIndexChanged += new System.EventHandler(this.cb_filtro_SelectedIndexChanged);
            // 
            // txt_filtro
            // 
            this.txt_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txt_filtro.Enabled = false;
            this.txt_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtro.Location = new System.Drawing.Point(253, 15);
            this.txt_filtro.MaxLength = 50;
            this.txt_filtro.Name = "txt_filtro";
            this.txt_filtro.Size = new System.Drawing.Size(205, 24);
            this.txt_filtro.TabIndex = 51;
            this.txt_filtro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_filtro_KeyUp);
            // 
            // dG_trabajdor
            // 
            this.dG_trabajdor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dG_trabajdor.Location = new System.Drawing.Point(470, 107);
            this.dG_trabajdor.Name = "dG_trabajdor";
            this.dG_trabajdor.Size = new System.Drawing.Size(351, 150);
            this.dG_trabajdor.TabIndex = 54;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(546, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "TRABAJADOR CON MAS HORAS TRABAJADAS";
            // 
            // Form_repPlanillajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(843, 360);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dG_trabajdor);
            this.Controls.Add(this.lbl_filtrar);
            this.Controls.Add(this.cb_filtro);
            this.Controls.Add(this.txt_filtro);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.gB_planillajes);
            this.Name = "Form_repPlanillajes";
            this.Text = "Reportes Planillajes";
            this.Load += new System.EventHandler(this.Form_repPlanillajes_Load);
            this.gB_planillajes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_plan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dG_trabajdor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gB_planillajes;
        private System.Windows.Forms.DataGridView dgv_plan;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Label lbl_filtrar;
        private System.Windows.Forms.ComboBox cb_filtro;
        private System.Windows.Forms.TextBox txt_filtro;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn horasReales;
        private System.Windows.Forms.DataGridViewTextBoxColumn operador;
        private System.Windows.Forms.DataGridViewTextBoxColumn maquina;
        private System.Windows.Forms.DataGridView dG_trabajdor;
        private System.Windows.Forms.Label label1;
    }
}