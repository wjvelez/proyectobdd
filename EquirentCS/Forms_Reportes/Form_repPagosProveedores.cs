﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_repPagosProveedores : Form
    {
        public Form_repPagosProveedores()
        {
            InitializeComponent();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.cb_filtro.SelectedIndex = -1;
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = false;
        }

        private void cb_filtro_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = true;
        }


        private void Form_repPagosProveedores_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_filtro_KeyUp(object sender, KeyEventArgs e)
        {
            int ind = this.cb_filtro.SelectedIndex;

            if (ind == 0)
            {
                //filtrar por fecha
            }
            else if (ind == 1)
            {
                //filtrar por factura
            }
            else if (ind == 2)
            {
                //filtrar por proveedor

            }
            else if (ind == 3)
            {
                //filtrar por detalle

            }
            else if (ind == 4)
            {
                //filtrar portipo servico

            }
            else if (ind == 5)
            {
                //filtrar pormaquian

            }
            else
            {
                //filtrar por valor
            }
        }


    }
}
