﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_repPlanillajes : Form
    {


        RepPlan plan;
        DialogResult confirmacion;
        Validaciones valida = new Validaciones();
        public Form_repPlanillajes()
        {
            InitializeComponent();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.cb_filtro.SelectedIndex = -1;
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = false;
        }

        private void cb_filtro_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = true;
        }

        private void txt_filtro_KeyUp(object sender, KeyEventArgs e)
        {
            int ind = this.cb_filtro.SelectedIndex;

            if (ind == 0)
            {
                //filtrar por operador
                if (plan.filtrar_Ced(this.txt_filtro.Text))
                {

                    dgv_plan.DataSource = plan.d_tb;
                }
                else
                    MessageBox.Show("Error al filtrar por operador planillaje");
            }
            else if(ind == 1)
            {
                //filtrar por maquina
                if (plan.filtrar_Maquina(this.txt_filtro.Text))
                {

                    dgv_plan.DataSource = plan.d_tb;
                }
                else
                    MessageBox.Show("Error al filtrar por maquina planillaje");
            }
            else
            {
                //filtrar por alquiler
                if (plan.filtrar_Alq(this.txt_filtro.Text))
                {

                    dgv_plan.DataSource = plan.d_tb;
                }
                else
                    MessageBox.Show("Error al filtrar por alquiler planillaje");
            }
        }

        private void Form_repPlanillajes_Load(object sender, EventArgs e)
        {
            plan = new RepPlan();
            plan.TrabajadorMaxHorasTrabajadas();
            cargarTrabajadorMaxHorasTrabajadas();
            cargar();

        }

        public void cargarTrabajadorMaxHorasTrabajadas()
        {
            if(plan.TrabajadorMaxHorasTrabajadas())
            {
                dG_trabajdor.DataSource = plan.d_tb;
            }
        }
        public void cargar()
        {
            if (plan.leerPlanillajes())
            {
                dgv_plan.DataSource = plan.d_tb;
            }
            else
                MessageBox.Show("error al cargar los planillajes");
                
        }
    }
}
