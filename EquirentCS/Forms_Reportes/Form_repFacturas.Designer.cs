﻿namespace EquirentCS
{
    partial class Form_repFacturas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txt_filtro = new System.Windows.Forms.TextBox();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.gB_fac = new System.Windows.Forms.GroupBox();
            this.dgv_fact = new System.Windows.Forms.DataGridView();
            this.factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alquiler = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_salir = new System.Windows.Forms.Button();
            this.cb_filtro = new System.Windows.Forms.ComboBox();
            this.lbl_filtrar = new System.Windows.Forms.Label();
            this.rB_emp = new System.Windows.Forms.RadioButton();
            this.rB_nat = new System.Windows.Forms.RadioButton();
            this.gB_fac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fact)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_filtro
            // 
            this.txt_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txt_filtro.Enabled = false;
            this.txt_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtro.Location = new System.Drawing.Point(312, 26);
            this.txt_filtro.Name = "txt_filtro";
            this.txt_filtro.Size = new System.Drawing.Size(309, 24);
            this.txt_filtro.TabIndex = 41;
            this.txt_filtro.TextChanged += new System.EventHandler(this.txt_filtro_TextChanged);
            this.txt_filtro.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_filtro_KeyUp);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Location = new System.Drawing.Point(456, 328);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(75, 23);
            this.btn_limpiar.TabIndex = 37;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // gB_fac
            // 
            this.gB_fac.Controls.Add(this.dgv_fact);
            this.gB_fac.Location = new System.Drawing.Point(12, 78);
            this.gB_fac.Name = "gB_fac";
            this.gB_fac.Size = new System.Drawing.Size(614, 229);
            this.gB_fac.TabIndex = 30;
            this.gB_fac.TabStop = false;
            // 
            // dgv_fact
            // 
            this.dgv_fact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_fact.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.factura,
            this.fecha,
            this.detalle,
            this.alquiler,
            this.valor_total});
            this.dgv_fact.Location = new System.Drawing.Point(6, 19);
            this.dgv_fact.Name = "dgv_fact";
            this.dgv_fact.Size = new System.Drawing.Size(603, 204);
            this.dgv_fact.TabIndex = 3;
            // 
            // factura
            // 
            this.factura.DataPropertyName = "codigoFactura";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.factura.DefaultCellStyle = dataGridViewCellStyle1;
            this.factura.HeaderText = "Código";
            this.factura.Name = "factura";
            this.factura.ReadOnly = true;
            this.factura.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.factura.Width = 60;
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.fecha.DefaultCellStyle = dataGridViewCellStyle2;
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.fecha.Width = 80;
            // 
            // detalle
            // 
            this.detalle.DataPropertyName = "descripcion";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.detalle.DefaultCellStyle = dataGridViewCellStyle3;
            this.detalle.HeaderText = "Detalle";
            this.detalle.Name = "detalle";
            this.detalle.ReadOnly = true;
            this.detalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.detalle.Width = 150;
            // 
            // alquiler
            // 
            this.alquiler.DataPropertyName = "codigoAlquiler";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.alquiler.DefaultCellStyle = dataGridViewCellStyle4;
            this.alquiler.HeaderText = "Alquiler";
            this.alquiler.Name = "alquiler";
            this.alquiler.ReadOnly = true;
            this.alquiler.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.alquiler.Width = 60;
            // 
            // valor_total
            // 
            this.valor_total.DataPropertyName = "valor";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.valor_total.DefaultCellStyle = dataGridViewCellStyle5;
            this.valor_total.HeaderText = "Valor";
            this.valor_total.Name = "valor_total";
            this.valor_total.ReadOnly = true;
            this.valor_total.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valor_total.Width = 70;
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(551, 328);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 29;
            this.btn_salir.Text = "SALIR";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // cb_filtro
            // 
            this.cb_filtro.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.cb_filtro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_filtro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_filtro.FormattingEnabled = true;
            this.cb_filtro.Items.AddRange(new object[] {
            "Nombre",
            "Decripción",
            "Alquiler",
            "Valor"});
            this.cb_filtro.Location = new System.Drawing.Point(96, 26);
            this.cb_filtro.Name = "cb_filtro";
            this.cb_filtro.Size = new System.Drawing.Size(182, 24);
            this.cb_filtro.TabIndex = 43;
            this.cb_filtro.SelectedIndexChanged += new System.EventHandler(this.cb_filtro_SelectedIndexChanged);
            // 
            // lbl_filtrar
            // 
            this.lbl_filtrar.AutoSize = true;
            this.lbl_filtrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_filtrar.Location = new System.Drawing.Point(15, 29);
            this.lbl_filtrar.Name = "lbl_filtrar";
            this.lbl_filtrar.Size = new System.Drawing.Size(75, 18);
            this.lbl_filtrar.TabIndex = 44;
            this.lbl_filtrar.Text = "Filtrar por:";
            // 
            // rB_emp
            // 
            this.rB_emp.AutoSize = true;
            this.rB_emp.Enabled = false;
            this.rB_emp.Location = new System.Drawing.Point(96, 55);
            this.rB_emp.Name = "rB_emp";
            this.rB_emp.Size = new System.Drawing.Size(66, 17);
            this.rB_emp.TabIndex = 45;
            this.rB_emp.TabStop = true;
            this.rB_emp.Text = "Empresa";
            this.rB_emp.UseVisualStyleBackColor = true;
            this.rB_emp.CheckedChanged += new System.EventHandler(this.rB_emp_CheckedChanged);
            // 
            // rB_nat
            // 
            this.rB_nat.AutoSize = true;
            this.rB_nat.Enabled = false;
            this.rB_nat.Location = new System.Drawing.Point(177, 55);
            this.rB_nat.Name = "rB_nat";
            this.rB_nat.Size = new System.Drawing.Size(101, 17);
            this.rB_nat.TabIndex = 46;
            this.rB_nat.TabStop = true;
            this.rB_nat.Text = "Persona Natural";
            this.rB_nat.UseVisualStyleBackColor = true;
            this.rB_nat.CheckedChanged += new System.EventHandler(this.rB_nat_CheckedChanged);
            // 
            // Form_repFacturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(638, 363);
            this.Controls.Add(this.rB_nat);
            this.Controls.Add(this.rB_emp);
            this.Controls.Add(this.lbl_filtrar);
            this.Controls.Add(this.cb_filtro);
            this.Controls.Add(this.txt_filtro);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.gB_fac);
            this.Controls.Add(this.btn_salir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_repFacturas";
            this.Text = "REPORTE FACTURAS";
            this.Load += new System.EventHandler(this.Form_repFacturas_Load);
            this.gB_fac.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fact)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_filtro;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.GroupBox gB_fac;
        private System.Windows.Forms.DataGridView dgv_fact;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.ComboBox cb_filtro;
        private System.Windows.Forms.Label lbl_filtrar;
        private System.Windows.Forms.RadioButton rB_emp;
        private System.Windows.Forms.RadioButton rB_nat;
        private System.Windows.Forms.DataGridViewTextBoxColumn factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn detalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn alquiler;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor_total;



    }
}