﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_repAlquileres : Form
    {
        RepAlqCN alqCn;
        RepAlqCE alqCe;

        public Form_repAlquileres()
        {
            InitializeComponent();
        }


        public void cargarAlqCN()
        {
            if(alqCn.leerRepAlpCN())
            {
                dG_RepALQ.DataSource = alqCn.d_tb;
            }
        }



        private void Form_Ralquiler_Load(object sender, EventArgs e)
        {
            alqCn = new RepAlqCN();
            alqCe = new RepAlqCE();
            alqCn.leerRepAlpCN();
            cargarAlqCN();
            
        }

        


        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cb_filtro_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Clear();
            this.rB_emp.Enabled = true;
            this.rB_nat.Enabled = true;
            this.rB_emp.Checked = false;
            this.rB_nat.Checked = false;
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.cb_filtro.SelectedIndex = -1;
            this.txt_filtro.Clear();
            this.txt_filtro.Enabled = false;
            this.rB_emp.Checked = false;
            this.rB_nat.Checked = false;
            this.rB_emp.Enabled = false;
            this.rB_nat.Enabled = false;
        }

        private void rB_emp_CheckedChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Enabled = true;
            this.txt_filtro.Clear();

        }

        private void rB_nat_CheckedChanged(object sender, EventArgs e)
        {
            this.txt_filtro.Enabled = true;
            this.txt_filtro.Clear();
        }

        private void txt_filtro_KeyUp(object sender, KeyEventArgs e)
        {
          

            if (rB_nat.Checked== true)
            {
                int ind = this.cb_filtro.SelectedIndex;

                if (ind == 0)
                {
                    
                    if (alqCn.filtrar_RepAlbCN_nombre(txt_filtro.Text))
                    {
                        dG_RepALQ.DataSource = alqCn.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar Alquiler cliente persona");
                }
                else if (ind == 1)
                {
                    if (alqCn.filtrar_RepAlbCN_codigo(txt_filtro.Text))
                    {
                        dG_RepALQ.DataSource = alqCn.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar Codigo cliente persona");
                }
            }


            if (rB_emp.Checked == true)
            {
                int ind = this.cb_filtro.SelectedIndex;
                if (ind == 0)
                {
                    
                    if (alqCe.filtrar_RepAlbCE_nombre(this.txt_filtro.Text))
                    {
                        dG_RepALQ.DataSource = alqCe.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar alquiler empresa");
                }
                else if (ind==1)
                {

                    if (alqCe.filtrar_RepAlbCE_codigo(this.txt_filtro.Text))
                    {
                        dG_RepALQ.DataSource = alqCe.d_tb;
                    }
                    else
                        MessageBox.Show("Error al filtrar Codigo empresa");
                }
            }



        }
    }
}
