﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class RepPlan: DataBase
    {
        public string ced = "";
        public string codA = "";
        public string placa = "";
        public string horas = "";
        public string fecha;
        public parametros[] prm;

        public RepPlan() 
        { 
        }

        public bool TrabajadorMaxHorasTrabajadas()
        {
            return ReadTable("Trabjador_Max3");
        }

        public RepPlan(string ced, string codA, string placa, string horas, string fecha)
        {
            this.ced = ced;
            this.codA = codA;
            this.placa = placa;
            this.horas = horas;
            this.fecha = fecha;
        }

        public bool leerPlanillajes()
        {
            return ReadTable("GET_PLANILLAJE");
        }


        public bool filtrar_Ced(string ced)
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_ced", ced);
            return WriteTable("FILTRO_REPPLA_OP", prm);
        }

        public bool filtrar_Fecha(string fecha)
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_fecha", fecha);
            return WriteTable("FILTRO_REPPLA_FEC", prm);
        }

        public bool filtrar_Maquina(string maq)
        {
            prm = new parametros[1];
            prm[0] = new parametros("in_maq", maq);
            return WriteTable("FILTRO_REPPLA_MAQ", prm);
        }

        public bool filtrar_Alq(string alquiler)
        {
            prm = new parametros[1];
            if (alquiler == "")
                alquiler = "0";

            prm[0] = new parametros("in_alq", alquiler);
            return WriteTable("FILTRO_REPPLA_ALQ", prm);
        }
       

    }
}
