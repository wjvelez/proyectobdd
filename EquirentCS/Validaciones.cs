﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace EquirentCS
{
    class Validaciones
    {
        String formato;

        public bool sonNumeros (string s) 
        {
            formato = "^[0-9]$";//Indica que el formato debe comenzar con un numero y terminar con un numero
            Regex re= new Regex(formato);

            if (!re.IsMatch(s))
            {
                return false;//Si no cumple con el formato me retorna falso
            }
            else
                return true;    
        }

        public bool sondecimales(string s)
        {
            formato = "^[0-9]{1,10}.?[0-9]{0,2}$";//Indica que el formato debe comenzar con un numero y terminar con un numero
            
            Regex re = new Regex(formato);

            if (!re.IsMatch(s))
            {
                return false;//Si no cumple con el formato me retorna falso
            }
            else
                return true;
        }

        public bool sonletras(string s)
        {
            formato = "^[a-zA-Z]$";//Indica que el formato debe comenzar con un numero y terminar con un numero
            Regex re = new Regex(formato);

            if (!re.IsMatch(s))
            {
                return false;//Si no cumple con el formato me retorna falso
            }
            else
                return true;
        }


        /*valida  10 caracteres*/
        public bool num_10(string s)
        {
            if (s.Length == 10)
            {
                return true;
            }
            else
                return false;
        }

        /*valida 13 caracteres*/
        public bool num_13 (string s)
        {
            if (s.Length == 13)
            {
                return true;
            }
            else
                return false;
        }

        /*Letray numero*/

        public void letra_num(System.Windows.Forms.KeyPressEventArgs e)
        {
            try
            {
                if (char.IsLetter(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if(char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;          
                }
                else
                    e.Handled = true;
            }
            catch (Exception ex) { }
        }


        /*validacion email*/
        public bool esEmail (string s)
        {
            formato = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            Regex re=new Regex(formato);
            if (!re.IsMatch(s))
                return false;
            else
                return true;
        }

        public bool esVacio(String s)
        {
            if (s == "")
            {

                return true;
            }
            else
                return false;
        }

        public bool esta_vacio(String s)
        {
            if (s == "")
            {

                return true;
            }
            else
                return false;
        }
     
    }
}
