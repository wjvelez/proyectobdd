﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace EquirentCS
{
    public partial class Form_Login : Form
    {
        public Form_Login()
        {
            InitializeComponent();
        }

        private void button_ingresar_Click(object sender, EventArgs e)
        {
            
            
            String admin = "admin";
            String pass = "admin";


            if (txt_clave.Text != "" && txt_usuario.Text != "")
            {
                if (txt_clave.Text == admin && txt_usuario.Text == pass)
                {
                    Form inicio = new Form_Inicio(admin);
                    inicio.Visible = true;
                    this.Visible = false;

                    /*Una vez validado usuario y clave se conecta a la BASE DE DATOS*/

                    //try
                    //{
                    //    MySqlConnection cn = DataBase.conectar();

                    //    MessageBox.Show("CONECTADO A LA BASE DE DATOS");

                        //MySqlCommand cmd = new MySqlCommand();
                        //cmd.CommandText = "select * from pepe";
                        //cmd.Connection = cn;

                        //MySqlDataReader reader;
                        //reader = cmd.ExecuteReader();
                        //reader.Read();
                        //MessageBox.Show("Bienvenido" + reader.GetValue(0));
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show("ERROR" + ex.Message);
                    //}

                }
                else
                {
                    MessageBox.Show("Usuario o clave incorrecta");
                    txt_clave.Clear();
                }
            }
            else
            {
                MessageBox.Show("Campo vacío");
                if (txt_usuario.Text == "")
                {
                    txt_usuario.Focus();
                }
                else
                {
                    txt_clave.Focus();
                }
            }

        }

        private void button_limpiar_Click(object sender, EventArgs e)
        {
            this.txt_clave.Clear();
            //this.txt_clave.Focus();
        }

        private void Form_Login_Load(object sender, EventArgs e)
        {
        }

        private void btn_salir_Click(object sender, EventArgs e) 
        {
            this.Close();

        }

        private void txt_clave_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_usuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) 
            {
                txt_clave.Focus();
            }
        }
    }
}
