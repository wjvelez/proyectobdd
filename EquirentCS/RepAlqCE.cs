﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class RepAlqCE : DataBase
    {
        
        string codigo;
        string fecha_inicio;
        string fecha_fin;
        string placa;
        string nombre;
        parametros []prm;
        public RepAlqCE()
        {

        }

        public RepAlqCE(string cod, string fecha_in, string fecha_f, string plac, string nomb)
        {
            codigo = cod;
            fecha_inicio = fecha_in;
            fecha_fin = fecha_f;
            placa = plac;
            nombre = nomb;
        }

        public bool leerRepAlpCN()
        {
            return ReadTable("GET_ALQUILER");
        }

        public bool filtrar_RepAlbCE_nombre(String nombre)
        {
            prm = new parametros[1];
            prm[0] = new parametros("nombre", nombre);
            return WriteTable("FILTRO_REPALQ_CE", prm);
        }


        public bool filtrar_RepAlbCE_codigo(String codigo)
        {
            if(codigo=="")
            {
                codigo = "0";
            }
            prm = new parametros[1];
            prm[0] = new parametros("in_codigo", codigo);
            return WriteTable("FILTRO_REPALQ_COD_CE", prm);
        }



    }
}
