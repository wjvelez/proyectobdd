﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EquirentCS.Forms_BaseDeDatos;

namespace EquirentCS
{
    public partial class Form_Inicio : Form
    {

        public Form_Inicio(String admin)
        {
            InitializeComponent();
            //TEMPORAL
            //lbl_currentAdmin.Text = admin;
            lbl_usuario.Text = admin;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_Alquiler_Click(object sender, EventArgs e)
        {
            Form_Alquiler A = new Form_Alquiler();
            A.Show();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_Inicio_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }



        private void btn_facturar_Click(object sender, EventArgs e)
        {
            Form_FacturarAlquiler fact = new Form_FacturarAlquiler();
            fact.Show();
        }

        private void btn_reportes_Click(object sender, EventArgs e)
        {
            Form_Reportes R = new Form_Reportes();
            R.Show();
        }

        private void btn_maquinaria_Click(object sender, EventArgs e)
        {
            Form_Maquinaria maq = new Form_Maquinaria();
            maq.Show();
        }

        private void btn_clientes_Click(object sender, EventArgs e)
        {
            Form_Cliente c = new Form_Cliente();
            c.Show();
        }

        private void btn_planillajes_Click(object sender, EventArgs e)
        {
            Form_Planillajes p = new Form_Planillajes();
            p.Show();
        }

        private void btn_administrar_Click(object sender, EventArgs e)
        {
            Form_InicioABD adm = new Form_InicioABD();
            adm.Show();
        }

        private void btn_pagosProveedores_Click(object sender, EventArgs e)
        {
            Form_PagosProveedores p = new Form_PagosProveedores();
            p.Show();
        }



        private void btn_ABD_Click(object sender, EventArgs e)
        {
            Form_InicioABD frm = new Form_InicioABD();
            frm.Show();
        }

        private void Form_Inicio_Load(object sender, EventArgs e)
        {

        }

    }
}
