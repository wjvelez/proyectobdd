﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class Factura : DataBase
    {
        public int codAlquiler = 0;
        public string descripcion = "";
        public float valor = 0;
        public float iva = 0;
        public float descuento = 0;
        public string fecha = "";
        public string usuario = "";
        public parametros[] prm;

        public Factura()
        {

        }

        public Factura(int codAlquiler, string descripcion, float valor, float iva, 
            float descuento, string fecha, string usuario)
        {
            this.codAlquiler = codAlquiler;
            this.descripcion = descripcion;
            this.valor = valor;
            this.iva = iva;
            this.descuento = descuento;
            this.fecha = fecha;
            this.usuario = usuario;
        }

        public bool agregarFactura()
        {
            prm = new parametros[7];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_codAlq", codAlquiler.ToString());
            prm[1] = new parametros("in_descrip", descripcion);
            prm[2] = new parametros("in_valor", valor.ToString("F", CultureInfo.CreateSpecificCulture("en-US")));
            prm[3] = new parametros("in_iva", iva.ToString("F", CultureInfo.CreateSpecificCulture("en-US")));
            prm[4] = new parametros("in_descuento", descuento.ToString("F", CultureInfo.CreateSpecificCulture("en-US")));
            prm[5] = new parametros("in_fecha", fecha);
            prm[6] = new parametros("in_usuario", usuario);

            return WriteTable("SET_FACTURA", prm);
        }

    }
}
