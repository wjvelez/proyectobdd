﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_PagosProveedores : Form
    {

        PagoProveed pago;
        Validaciones valida = new Validaciones();
        DialogResult confirmacion;
        public Form_PagosProveedores()
        {
            InitializeComponent();
        }

        public void cargar()
        {
            if (pago.leerPagoProveed())
            {
                dG_pagos.DataSource = pago.d_tb;
            }
            else
            {
                MessageBox.Show("Error en lectura de pagos");
            }
        } 


        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            this.txt_det.Clear();
            this.txt_fact.Clear();
            this.txt_maq.Clear();
            this.txt_tipo.Clear();
            this.txt_valor.Clear();
            this.txt_ruc.Clear();
        }


        private void Form_PagoProoved_Load(object sender, EventArgs e)
        {
            pago = new PagoProveed();
            cargar();
        }


        private void btn_guardar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear planillaje con lo valores a guardar
                // string ci, string maq, string horas, string f, string cod
                //PagoProveed(string ruc, string maq, string codFact, string f, string det, string tipo, string valor)
                pago = new PagoProveed(txt_ruc.Text, txt_maq.Text, txt_fact.Text, dtp.Text, txt_det.Text, txt_tipo.Text, txt_valor.Text);

                //Lo va guardar en la base de datos
                pago.agregarPagoProveed();

                //refresca la tabla
                cargar();

            }
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //pl = new Planillaje(txt_ci.Text, txt_maq.Text, txt_horas.Text, dtp.Text, txt_alq.Text);
                pago = new PagoProveed(txt_ruc.Text, txt_maq.Text, txt_fact.Text, dtp.Text, txt_det.Text, txt_tipo.Text, txt_valor.Text);

                //Lo va guardar en la base de datos
                pago.modificarPagoProveed();
                

                //refresca la tabla
                cargar();
            }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pago = new PagoProveed(txt_ruc.Text, txt_maq.Text, txt_fact.Text, dtp.Text, txt_det.Text, txt_tipo.Text, txt_valor.Text);
                //Lo va guardar en la base de datos
                pago.eliminarPagoProveed();

                //refresca la tabla
                cargar();
            }
        }

        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {
            txt_fact.Text = dG_pagos.CurrentRow.Cells[0].Value.ToString();
            txt_ruc.Text = dG_pagos.CurrentRow.Cells[1].Value.ToString();
            txt_valor.Text = dG_pagos.CurrentRow.Cells[4].Value.ToString();
            txt_det.Text = dG_pagos.CurrentRow.Cells[3].Value.ToString();
            txt_maq.Text = dG_pagos.CurrentRow.Cells[2].Value.ToString();
            txt_tipo.Text = dG_pagos.CurrentRow.Cells[5].Value.ToString();
            dtp.Text = dG_pagos.CurrentRow.Cells[6].Value.ToString();
            
            
        }

        private void txt_fact_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_fact.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_det_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_det_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_det.MaxLength = 255;
            valida.letra_num(e);
        }

        private void txt_ruc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_det.MaxLength = 13;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_valor_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_valor.MaxLength = 10;

            /*valida que sean numerico y despues de que termine de escribir validara si en un decimal en el evento LEAVE*/
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if (e.KeyChar == '.')
                    e.Handled = false;
            }
        }

        private void txt_valor_Leave(object sender, EventArgs e)
        {
            if (txt_valor.Text != "")
            {
                if (!valida.sondecimales(txt_valor.Text))
                {
                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    //txt_costoHora.Clear();
                    txt_valor.Focus();
                }
            }
        }







    }
}
