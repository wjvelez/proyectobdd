﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquirentCS
{
    class Planillaje : DataBase
    {

        public string ced = "";
        public string maquina = "";
        public string horas = "";
        public string fecha = "";
        public string codAlq = "";
        public parametros[] prm;

        public Planillaje()
        {
        }

        public Planillaje(string ci, string maq, string horas, string f, string cod)
        {
            this.ced = ci;
            this.maquina = maq;
            this.horas = horas;
            this.fecha = f;
            this.codAlq = cod;
        }

        public bool leerPlanillaje()
        {
            return ReadTable("GET_PLANILLAJE");
        }


        public bool agregarPlanillaje()
        {
            prm = new parametros[5];
            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("cedula", ced);
            prm[1] = new parametros("placa", maquina);
            prm[2] = new parametros("codigoAlquiler", codAlq);
            prm[3] = new parametros("horas", horas);
            prm[4] = new parametros("fecha", fecha);

            return WriteTable("SET_PLANILLAJE", prm);
        }

        public bool modificarPlanillaje()
        {
            prm = new parametros[5];

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            prm[0] = new parametros("in_ced", ced);
            prm[1] = new parametros("in_placa", maquina);
            prm[2] = new parametros("in_codAlq", codAlq);
            prm[3] = new parametros("in_horas", horas);
            prm[4] = new parametros("in_fec", fecha);

            return WriteTable("MOD_PLANILLAJE", prm);
        }

        public bool eliminarPlanillaje()
        {
            prm = new parametros[3];
            
            prm[0] = new parametros("in_placa", maquina);
            prm[1] = new parametros("in_codAlq", codAlq);
            prm[2] = new parametros("in_fecha", fecha);

            /*IMPORTANTE lo que esta entre comillas debe ser IGUAL a los parametros de entradas de los PROCEDURE*/
            return WriteTable("DEL_PLANILLAJE", prm);
        }

    }
}
