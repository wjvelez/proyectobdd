﻿namespace EquirentCS
{
    partial class Form_FacturarAlquiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lbl_total = new System.Windows.Forms.Label();
            this.txt_filtro_alquiler = new System.Windows.Forms.TextBox();
            this.lbl_fact = new System.Windows.Forms.Label();
            this.lbl_codfact = new System.Windows.Forms.Label();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_facturar = new System.Windows.Forms.Button();
            this.lbl_fecha = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.DateTimePicker();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.txt_iva = new System.Windows.Forms.TextBox();
            this.txt_desc = new System.Windows.Forms.TextBox();
            this.lbl_iva = new System.Windows.Forms.Label();
            this.lbl_desc = new System.Windows.Forms.Label();
            this.lbl_subtotal = new System.Windows.Forms.Label();
            this.txt_subtotal = new System.Windows.Forms.TextBox();
            this.rbtn_ruc = new System.Windows.Forms.RadioButton();
            this.rbtn_ci = new System.Windows.Forms.RadioButton();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_detalle = new System.Windows.Forms.TextBox();
            this.dgv_detalle = new System.Windows.Forms.DataGridView();
            this.Maquina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Horas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdBtn_codAlquiler = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_busc_clie = new System.Windows.Forms.Label();
            this.dgv_cliente = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_aceptar = new System.Windows.Forms.Button();
            this.dgv_maquina = new System.Windows.Forms.DataGridView();
            this.DevolverMaquina = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_detalle)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cliente)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_maquina)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total.Location = new System.Drawing.Point(504, 198);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(99, 17);
            this.lbl_total.TabIndex = 26;
            this.lbl_total.Text = "Valor Total ($)";
            // 
            // txt_filtro_alquiler
            // 
            this.txt_filtro_alquiler.Enabled = false;
            this.txt_filtro_alquiler.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_filtro_alquiler.Location = new System.Drawing.Point(157, 89);
            this.txt_filtro_alquiler.Name = "txt_filtro_alquiler";
            this.txt_filtro_alquiler.Size = new System.Drawing.Size(127, 23);
            this.txt_filtro_alquiler.TabIndex = 32;
            this.txt_filtro_alquiler.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_filtro_alquiler_KeyPress);
            this.txt_filtro_alquiler.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_filtro_alquiler_KeyUp);
            // 
            // lbl_fact
            // 
            this.lbl_fact.AutoSize = true;
            this.lbl_fact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fact.Location = new System.Drawing.Point(24, 9);
            this.lbl_fact.Name = "lbl_fact";
            this.lbl_fact.Size = new System.Drawing.Size(56, 17);
            this.lbl_fact.TabIndex = 33;
            this.lbl_fact.Text = "Factura";
            // 
            // lbl_codfact
            // 
            this.lbl_codfact.AutoSize = true;
            this.lbl_codfact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_codfact.Location = new System.Drawing.Point(115, 9);
            this.lbl_codfact.Name = "lbl_codfact";
            this.lbl_codfact.Size = new System.Drawing.Size(77, 17);
            this.lbl_codfact.TabIndex = 34;
            this.lbl_codfact.Text = "codigoFact";
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancelar.Location = new System.Drawing.Point(34, 563);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(72, 26);
            this.btn_cancelar.TabIndex = 35;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_facturar
            // 
            this.btn_facturar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_facturar.Location = new System.Drawing.Point(677, 563);
            this.btn_facturar.Name = "btn_facturar";
            this.btn_facturar.Size = new System.Drawing.Size(72, 26);
            this.btn_facturar.TabIndex = 36;
            this.btn_facturar.Text = "Facturar";
            this.btn_facturar.UseVisualStyleBackColor = true;
            this.btn_facturar.Click += new System.EventHandler(this.btn_facturar_Click);
            // 
            // lbl_fecha
            // 
            this.lbl_fecha.AutoSize = true;
            this.lbl_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fecha.Location = new System.Drawing.Point(254, 10);
            this.lbl_fecha.Name = "lbl_fecha";
            this.lbl_fecha.Size = new System.Drawing.Size(47, 17);
            this.lbl_fecha.TabIndex = 38;
            this.lbl_fecha.Text = "Fecha";
            // 
            // date
            // 
            this.date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date.Location = new System.Drawing.Point(323, 5);
            this.date.MinDate = new System.DateTime(2015, 8, 29, 0, 0, 0, 0);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(113, 23);
            this.date.TabIndex = 39;
            // 
            // txt_total
            // 
            this.txt_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_total.Location = new System.Drawing.Point(609, 195);
            this.txt_total.Name = "txt_total";
            this.txt_total.ReadOnly = true;
            this.txt_total.Size = new System.Drawing.Size(127, 23);
            this.txt_total.TabIndex = 40;
            this.txt_total.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_total_KeyPress);
            this.txt_total.Leave += new System.EventHandler(this.txt_total_Leave);
            // 
            // txt_iva
            // 
            this.txt_iva.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_iva.Location = new System.Drawing.Point(362, 195);
            this.txt_iva.Name = "txt_iva";
            this.txt_iva.ReadOnly = true;
            this.txt_iva.Size = new System.Drawing.Size(127, 23);
            this.txt_iva.TabIndex = 41;
            this.txt_iva.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_iva_KeyPress_1);
            this.txt_iva.Leave += new System.EventHandler(this.txt_iva_Leave);
            // 
            // txt_desc
            // 
            this.txt_desc.Enabled = false;
            this.txt_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_desc.Location = new System.Drawing.Point(362, 166);
            this.txt_desc.Name = "txt_desc";
            this.txt_desc.Size = new System.Drawing.Size(127, 23);
            this.txt_desc.TabIndex = 42;
            this.txt_desc.Text = "0";
            this.txt_desc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_desc_KeyPress_1);
            this.txt_desc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_desc_KeyUp);
            this.txt_desc.Leave += new System.EventHandler(this.txt_desc_Leave);
            // 
            // lbl_iva
            // 
            this.lbl_iva.AutoSize = true;
            this.lbl_iva.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_iva.Location = new System.Drawing.Point(280, 198);
            this.lbl_iva.Name = "lbl_iva";
            this.lbl_iva.Size = new System.Drawing.Size(41, 17);
            this.lbl_iva.TabIndex = 43;
            this.lbl_iva.Text = "I.V.A.";
            // 
            // lbl_desc
            // 
            this.lbl_desc.AutoSize = true;
            this.lbl_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_desc.Location = new System.Drawing.Point(280, 169);
            this.lbl_desc.Name = "lbl_desc";
            this.lbl_desc.Size = new System.Drawing.Size(76, 17);
            this.lbl_desc.TabIndex = 44;
            this.lbl_desc.Text = "Descuento";
            // 
            // lbl_subtotal
            // 
            this.lbl_subtotal.AutoSize = true;
            this.lbl_subtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_subtotal.Location = new System.Drawing.Point(543, 169);
            this.lbl_subtotal.Name = "lbl_subtotal";
            this.lbl_subtotal.Size = new System.Drawing.Size(60, 17);
            this.lbl_subtotal.TabIndex = 46;
            this.lbl_subtotal.Text = "Subtotal";
            // 
            // txt_subtotal
            // 
            this.txt_subtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_subtotal.Location = new System.Drawing.Point(609, 166);
            this.txt_subtotal.Name = "txt_subtotal";
            this.txt_subtotal.ReadOnly = true;
            this.txt_subtotal.Size = new System.Drawing.Size(127, 23);
            this.txt_subtotal.TabIndex = 45;
            this.txt_subtotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_subtotal_KeyPress);
            this.txt_subtotal.Leave += new System.EventHandler(this.txt_subtotal_Leave);
            // 
            // rbtn_ruc
            // 
            this.rbtn_ruc.AutoSize = true;
            this.rbtn_ruc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ruc.Location = new System.Drawing.Point(21, 90);
            this.rbtn_ruc.Name = "rbtn_ruc";
            this.rbtn_ruc.Size = new System.Drawing.Size(61, 20);
            this.rbtn_ruc.TabIndex = 47;
            this.rbtn_ruc.TabStop = true;
            this.rbtn_ruc.Text = "R.U.C";
            this.rbtn_ruc.UseVisualStyleBackColor = true;
            this.rbtn_ruc.CheckedChanged += new System.EventHandler(this.rbtn_ruc_CheckedChanged);
            // 
            // rbtn_ci
            // 
            this.rbtn_ci.AutoSize = true;
            this.rbtn_ci.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ci.Location = new System.Drawing.Point(21, 56);
            this.rbtn_ci.Name = "rbtn_ci";
            this.rbtn_ci.Size = new System.Drawing.Size(44, 20);
            this.rbtn_ci.TabIndex = 48;
            this.rbtn_ci.TabStop = true;
            this.rbtn_ci.Text = "C.I.";
            this.rbtn_ci.UseVisualStyleBackColor = true;
            this.rbtn_ci.CheckedChanged += new System.EventHandler(this.rbtn_ci_CheckedChanged);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_limpiar.Location = new System.Drawing.Point(170, 563);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(72, 26);
            this.btn_limpiar.TabIndex = 49;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_detalle);
            this.groupBox1.Controls.Add(this.txt_total);
            this.groupBox1.Controls.Add(this.txt_subtotal);
            this.groupBox1.Controls.Add(this.lbl_desc);
            this.groupBox1.Controls.Add(this.lbl_total);
            this.groupBox1.Controls.Add(this.dgv_detalle);
            this.groupBox1.Controls.Add(this.lbl_iva);
            this.groupBox1.Controls.Add(this.lbl_subtotal);
            this.groupBox1.Controls.Add(this.txt_desc);
            this.groupBox1.Controls.Add(this.txt_iva);
            this.groupBox1.Location = new System.Drawing.Point(13, 306);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 240);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle de la factura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Detalle";
            // 
            // txt_detalle
            // 
            this.txt_detalle.Enabled = false;
            this.txt_detalle.Location = new System.Drawing.Point(67, 166);
            this.txt_detalle.Multiline = true;
            this.txt_detalle.Name = "txt_detalle";
            this.txt_detalle.Size = new System.Drawing.Size(207, 52);
            this.txt_detalle.TabIndex = 47;
            this.txt_detalle.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dgv_detalle
            // 
            this.dgv_detalle.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_detalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_detalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Maquina,
            this.Horas,
            this.Precio,
            this.Total});
            this.dgv_detalle.Enabled = false;
            this.dgv_detalle.Location = new System.Drawing.Point(23, 19);
            this.dgv_detalle.Name = "dgv_detalle";
            this.dgv_detalle.ReadOnly = true;
            this.dgv_detalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_detalle.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_detalle.Size = new System.Drawing.Size(713, 123);
            this.dgv_detalle.TabIndex = 37;
            // 
            // Maquina
            // 
            this.Maquina.HeaderText = "Maquina";
            this.Maquina.MinimumWidth = 100;
            this.Maquina.Name = "Maquina";
            this.Maquina.ReadOnly = true;
            // 
            // Horas
            // 
            this.Horas.HeaderText = "Horas";
            this.Horas.Name = "Horas";
            this.Horas.ReadOnly = true;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox2.Controls.Add(this.rdBtn_codAlquiler);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lbl_busc_clie);
            this.groupBox2.Controls.Add(this.dgv_cliente);
            this.groupBox2.Controls.Add(this.rbtn_ruc);
            this.groupBox2.Controls.Add(this.rbtn_ci);
            this.groupBox2.Controls.Add(this.txt_filtro_alquiler);
            this.groupBox2.Location = new System.Drawing.Point(13, 45);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 255);
            this.groupBox2.TabIndex = 51;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seleccionar Alquiler";
            // 
            // rdBtn_codAlquiler
            // 
            this.rdBtn_codAlquiler.AutoSize = true;
            this.rdBtn_codAlquiler.Location = new System.Drawing.Point(161, 58);
            this.rdBtn_codAlquiler.Name = "rdBtn_codAlquiler";
            this.rdBtn_codAlquiler.Size = new System.Drawing.Size(95, 17);
            this.rdBtn_codAlquiler.TabIndex = 52;
            this.rdBtn_codAlquiler.TabStop = true;
            this.rdBtn_codAlquiler.Text = "Código Alquiler";
            this.rdBtn_codAlquiler.UseVisualStyleBackColor = true;
            this.rdBtn_codAlquiler.CheckedChanged += new System.EventHandler(this.rdBtn_codAlquiler_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Buscar por alquiler:";
            // 
            // lbl_busc_clie
            // 
            this.lbl_busc_clie.AutoSize = true;
            this.lbl_busc_clie.Location = new System.Drawing.Point(11, 25);
            this.lbl_busc_clie.Name = "lbl_busc_clie";
            this.lbl_busc_clie.Size = new System.Drawing.Size(95, 13);
            this.lbl_busc_clie.TabIndex = 50;
            this.lbl_busc_clie.Text = "Buscar por cliente:";
            // 
            // dgv_cliente
            // 
            this.dgv_cliente.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_cliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cliente.Location = new System.Drawing.Point(6, 120);
            this.dgv_cliente.Name = "dgv_cliente";
            this.dgv_cliente.Size = new System.Drawing.Size(352, 120);
            this.dgv_cliente.TabIndex = 49;
            this.dgv_cliente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_cliente_CellClick);
            this.dgv_cliente.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_cliente_CellEnter);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btn_aceptar);
            this.groupBox3.Controls.Add(this.dgv_maquina);
            this.groupBox3.Location = new System.Drawing.Point(383, 45);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(389, 255);
            this.groupBox3.TabIndex = 52;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Máquinas del alquiler";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(301, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Seleccione las maquinaria que se devuelven con esta factura.";
            // 
            // btn_aceptar
            // 
            this.btn_aceptar.Enabled = false;
            this.btn_aceptar.Location = new System.Drawing.Point(6, 58);
            this.btn_aceptar.Name = "btn_aceptar";
            this.btn_aceptar.Size = new System.Drawing.Size(75, 23);
            this.btn_aceptar.TabIndex = 1;
            this.btn_aceptar.Text = "Aceptar";
            this.btn_aceptar.UseVisualStyleBackColor = true;
            this.btn_aceptar.Click += new System.EventHandler(this.btn_aceptar_Click);
            // 
            // dgv_maquina
            // 
            this.dgv_maquina.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_maquina.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_maquina.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DevolverMaquina});
            this.dgv_maquina.Location = new System.Drawing.Point(7, 87);
            this.dgv_maquina.Name = "dgv_maquina";
            this.dgv_maquina.Size = new System.Drawing.Size(383, 153);
            this.dgv_maquina.TabIndex = 0;
            // 
            // DevolverMaquina
            // 
            this.DevolverMaquina.FalseValue = "FALSE";
            this.DevolverMaquina.HeaderText = "DevolverMaquina";
            this.DevolverMaquina.IndeterminateValue = "FALSE";
            this.DevolverMaquina.Name = "DevolverMaquina";
            this.DevolverMaquina.TrueValue = "TRUE";
            // 
            // Form_FacturarAlquiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 601);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.date);
            this.Controls.Add(this.lbl_fecha);
            this.Controls.Add(this.lbl_codfact);
            this.Controls.Add(this.lbl_fact);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_facturar);
            this.Name = "Form_FacturarAlquiler";
            this.Text = "Facturar Alquiler";
            this.Load += new System.EventHandler(this.Form_FacturarAlquiler_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_detalle)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cliente)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_maquina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.TextBox txt_filtro_alquiler;
        private System.Windows.Forms.Label lbl_fact;
        private System.Windows.Forms.Label lbl_codfact;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_facturar;
        private System.Windows.Forms.Label lbl_fecha;
        private System.Windows.Forms.DateTimePicker date;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.TextBox txt_iva;
        private System.Windows.Forms.TextBox txt_desc;
        private System.Windows.Forms.Label lbl_iva;
        private System.Windows.Forms.Label lbl_desc;
        private System.Windows.Forms.Label lbl_subtotal;
        private System.Windows.Forms.TextBox txt_subtotal;
        private System.Windows.Forms.RadioButton rbtn_ruc;
        private System.Windows.Forms.RadioButton rbtn_ci;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_cliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_busc_clie;
        private System.Windows.Forms.RadioButton rdBtn_codAlquiler;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgv_maquina;
        private System.Windows.Forms.Button btn_aceptar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv_detalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Maquina;
        private System.Windows.Forms.DataGridViewTextBoxColumn Horas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DevolverMaquina;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_detalle;


    }
}