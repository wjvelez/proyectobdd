﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_Alquiler : Form
    {
        ClienteEmp emp;
        ClienteNat nat;
        Maquina machine;
        Alquiler alq;
        alquilerMaquinas AlqM;
        string valor;//variable para codigo de alquiler
        DialogResult confirmacion;
        Validaciones valida = new Validaciones();
        bool BANDER; // variable para controlar el leave de text_ident

        public Form_Alquiler()
        {       
            InitializeComponent();
        }



        /*Metodo que carga codigo de alquiler*/
        public void cargarCod()
        {
            if (alq.getCodigo_ALquiler())
            {
                valor = alq.UnicoValor;
               // MessageBox.Show(valor);
            }
            else
                MessageBox.Show("ERROR CODIGO");
        }


        /*Metodo para cargar los valores en el FORM_CLIENT*/
        public void cargarClienteNat()
        {
            if (nat.buscarClientNat())
            {
                dgv_cliente.DataSource = nat.d_tb;
            }
            else
                MessageBox.Show("ERROR PERSONA");
        }

        /*Metodo para cargar los valores en el FORM_CLIENT*/
        public void cargarClienteEmp()
        {
            if (emp.buscarClienteEmp())
            {
                dgv_cliente.DataSource = emp.d_tb;
                //dgv_Empresa.DataSource = emp.d_tb;
            }
            else
                MessageBox.Show("ERROR EMPRESA");
        }


        /*   ¡¡¡¡  NO SE ESTA UTILIZANDO PERO PUEDE SER UTIL  !!!!*/
        //       Para muestra TIPO y PLACA que estan DISPONIBLES pero no Filtra por TIPO
        public void cargarMaquinasDisponibles()
        {
            if (machine.leerMaquina_P_T())
            {
                 //cB_Maquina.DataSource=machine.d_tb;
                // cB_Maquina.DisplayMember = "tipo";
                 dG_maquinas.DataSource = machine.d_tb;
            }
            else
            {
                MessageBox.Show("Error MAQUINA");
            }
        }

        /*Para Muestra PLACA y TIPO que estan DISPONIBLES*/
        /*        Y Filtra por TIPO                 */
        public void cargarMaquinaDisponiblesTipo()
        {
            if (machine.buscarMaquina_Tipo())
            {
                dG_maquinas.DataSource = machine.d_tb;
            }
            else
                MessageBox.Show("Error en Cargar Maquina por Tipo");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txt_date.Text = dateTP_alquiler.Value.ToString("yyyy-MM-dd");
        }

        private void Form_Alquiler_Load(object sender, EventArgs e)
        {

            machine = new Maquina();
            emp = new ClienteEmp();
            nat = new ClienteNat();
            alq = new Alquiler();
            //cargarMaquinasDisponibles();
            cargarMaquinaDisponiblesTipo();
            cargarCod();

            txt_date.Text = dateTP_alquiler.Value.ToString("yyyy-MM-dd");

            //cargar();
           // cB_Maquina.DataSource=;
            //cB_Maquina.DisplayMember = "";
            //cargarClienteNat();
            //cargarClienteEmp();
        }

        private void txt_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_nombre.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if(char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

                if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
                {
                    txt_direcc.Focus();
                }

            }
        }

        private void txt_Adress_TextChanged(object sender, EventArgs e)
        {
            
        }



        private void txt_Adress_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_direcc.MaxLength = 255;
            valida.letra_num(e);
            if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
            {
                txt_telefono.Focus();
            }
        }





        private void txt_Telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_telefono.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
                {
                    txt_email.Focus();
                }
            }
        }


        private void btn_Alquilar_Click(object sender, EventArgs e)
        {
            
            
            String [ ]llenado=new  string [6];
            
            llenado[0] = txt_ident.Text;
            llenado[1] = txt_nombre.Text;
            llenado[2] = txt_direcc.Text;
            llenado[3] = txt_telefono.Text;
            llenado[4] = txt_email.Text;
            llenado[5] = txt_costoHora.Text;
            int num_filas = dG_OperMaq.RowCount;

            //MessageBox.Show(""+num_filas);

            if (!valida.esVacio(llenado[0]) &
                !valida.esVacio(llenado[1]) &
                !valida.esVacio(llenado[2]) &
                !valida.esVacio(llenado[3]) &
                !valida.esVacio(llenado[4]) &
                !valida.esVacio(llenado[5]) &
                (num_filas > 1)
                ) 
            { 
                confirmacion = MessageBox.Show("Seguro que desea ALQULAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (confirmacion == DialogResult.Yes)
                {
                    /*Proceso de llenado a la base de Datos*/

                    //                ************    Llenado ---> Cliente Natural   ************
                    if (rbtn_ci.Checked == true)
                    {
                        //defino cliente
                        nat = new ClienteNat(txt_ident.Text, txt_nombre.Text, txt_direcc.Text, txt_telefono.Text, txt_email.Text);
                        //agrego a la base 
                        nat.agregarClienteNat();

                        //   ***** Llenado ---> Alquiler para clienteNatural *****
                        alq = new Alquiler(null, txt_ident.Text, txt_date.Text, txt_costoHora.Text);
                        alq.agregarAlquiler();

                    }

                    //                ************     Llenado ---> Cliente Empresa  *************
                    if (rbtn_ruc.Checked == true)
                    {
                        //defino empresa
                        emp = new ClienteEmp(txt_ident.Text, txt_nombre.Text, txt_direcc.Text, txt_telefono.Text, txt_email.Text);
                        emp.agregarClienteEmp();

                        //   *****   Llenado --> Alquiler para Empresa  ******
                        alq = new Alquiler(txt_ident.Text, null, txt_date.Text, txt_costoHora.Text);
                        alq.agregarAlquiler();
                    }




                    //TOMO EL CODIGO GENERADO POR ALQUILER ANTES DE AÑADIR A LA BASE DE DATOS DE "ALQUILER MAQUINA"
                    alq = new Alquiler();
                    alq.getCodigo_ALquiler();
                    cargarCod(); //carga el valor del ultimo codigo de alquiler y le asigna al string VALOR

                    //            ******* Llenado --->AlquilerMaquina  ******

                    // llenado temporal para una sola maquina porque lo toma directamente del TextBox
                    //AlqM = new alquilerMaquinas(valor, txt_placa.Text);// Es un string que se genera a partir cargarCod que contiene el codigo de alquiler
                    //AlqM.agregarAlquilerMaquinaDefault();


                    //Recorre el DataGripView es decir dG_OperMaq
                    //para que añada todas las maquinas que desea alquiler y han sido añadido al dG_OperMaq

                    foreach (DataGridViewRow row in dG_OperMaq.Rows)
                    {
                        try
                        {
                            string placaDG = row.Cells["placaAdd"].Value.ToString();

                            AlqM = new alquilerMaquinas(valor,placaDG);
                            AlqM.agregarAlquilerMaquinaDefault();
                        }
                        catch (Exception ex) { };

                    }

                    MessageBox.Show("MAQUINARIA ALQUILADA");
                    this.Close();
                }
            }
            else
                MessageBox.Show("FALTA LLENAR ALGUN CAMPO");
            /* -- Procesos de llenado a la base de datos */

        }

        private void rB_CI_CheckedChanged(object sender, EventArgs e)
        {
            txt_ident.Clear();
            txt_ident.Enabled=true;
            txt_filtro.Enabled = true;
        }

        private void rB_ruc_CheckedChanged(object sender, EventArgs e)
        {
            txt_ident.Clear();
            txt_ident.Enabled = true;
            txt_filtro.Enabled = true;
        }




        private void txt_costoHora_Leave(object sender, EventArgs e)
        {
            if (txt_costoHora.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.sondecimales(txt_costoHora.Text))
                {
                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    txt_costoHora.Clear();
                }
            }

        }


        private void txt_costoHora_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_costoHora.MaxLength = 10;

            /*valida que sean numerico y despues de que termine de escribir validara si en un decimal en el evento LEAVE*/

            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if ( e.KeyChar == '.')
                    e.Handled = false;
            }
        }


        private void txt_ident_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            string num = "" + c;

            /*valida numeros de caracteres permitidos a ingresar*/
            if (rbtn_ci.Checked == true)
            {
                txt_ident.MaxLength = 10;
            }

            if (rbtn_ruc.Checked == true)
            {
                txt_ident.MaxLength = 13;
            }

            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;


                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                    txt_nombre.Focus();

            }
        }



        private void txt_ident_Leave(object sender, EventArgs e)
        {

            if (txt_ident.Text != "" & btn_cancelar.ContainsFocus == false)
                {
                    if (rbtn_ci.Checked == true)
                    {
                        if (!valida.num_10(txt_ident.Text))
                        {

                            MessageBox.Show("C.I DEBE TENER 10 DIGITOS");
                            //txt_ident.Focus();
                            txt_ident.Clear();
                        }
                    }

                    if (rbtn_ruc.Checked == true)
                    {
                        if (!valida.num_13(txt_ident.Text))
                        {
                            MessageBox.Show("RUC DEBE TENER 13 DIGITOS");
                            txt_ident.Clear();


                        }
                    }
                }
            
        }

 

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            txt_ident.Clear();
            txt_nombre.Clear();
            txt_direcc.Clear();
            txt_telefono.Clear();
            txt_email.Clear();
        }




        private void cB_Maquina_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*temporalmente*/
            //if (cB_Maquina.Text == "Maquina1")
            //{
            //    txt_placa.Text = "GPT-123";
            //}
            //else if (cB_Maquina.Text == "Maquina2")
            //{
            //    txt_placa.Text = "GTT-323";
            //}
            //else if (cB_Maquina.Text == "Maquina3")
            //{
            //    txt_placa.Text = "GYY-195";
            //}
            //else if (cB_Maquina.Text == "Maquina4")
            //{
            //    txt_placa.Text = "GHG-767";
            //}

           //MessageBox.Show( cB_Maquina.SelectedText.ToString());


        }

        private void btn_añadir_Click(object sender, EventArgs e)
        {
            
            /*Añade una fila con los datos selecionados*/
            bool existe=false;

            //recorre el DataGripView es decir dG_OperMaq

            foreach(DataGridViewRow row in dG_OperMaq.Rows )
            {
                try
                {
                    //Solo va verificando las columna señalada
                    if (txt_placa.Text == row.Cells["placaAdd"].Value.ToString())
                    {
                        existe = true;
                        break;
                    }
                }
                catch(Exception ex){};
 
            }

            if (existe == false & txt_placa.Text != "" & txt_tipo.Text != "")
            {
                dG_OperMaq.Rows.Add(txt_tipo.Text, txt_placa.Text, "ELIMINAR");
            }


            //dG_OperMaq.Rows.Add(cB_Maquina.Text,);
            
        }

 
        /*BOTON ELIMINAR DENTO DE DATAGRIPVIEW*/
        private void click_botondG(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dG_OperMaq.CurrentCell.ColumnIndex == 2)
                {
                    //Remueve del dataGridView actual la fila donde esta a celda activa
                    dG_OperMaq.Rows.Remove(dG_OperMaq.CurrentRow);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("NO EXISTE DATOS A ELIMINAR");
            }


        }

        private void txt_email_Leave(object sender, EventArgs e)
        {
            if (txt_email.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.esEmail(txt_email.Text))
                {
                    MessageBox.Show("CORREO INVALIDO\n ej:correo@correo.exts");
                    txt_email.Clear();

                }
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dG_OperMaq_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_ident_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_nombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_email_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_email.MaxLength = 50;
        }

  

        private void tPg_DC_Click(object sender, EventArgs e)
        {

        }

        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {
            txt_placa.Text = dG_maquinas.CurrentRow.Cells[0].Value.ToString();
            txt_tipo.Text = dG_maquinas.CurrentRow.Cells[1].Value.ToString();
        }

        private void txt_ident_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbtn_ci.Checked == true)
            {
                nat = new ClienteNat(txt_ident.Text);
                //Lo va guardar en la base de datos
                nat.buscarClientNat();
                //refresca la tabla
                cargarClienteNat();
            }

            if (rbtn_ruc.Checked == true)
            {
                emp = new ClienteEmp(txt_ident.Text);
                emp.buscarClienteEmp();
                cargarClienteEmp();
            }
              
        }

        private void click_autollenadoClient(object sender, DataGridViewCellEventArgs e)
        {

            if (dgv_cliente.RowCount > 1)
            {
                txt_ident.Text = dgv_cliente.CurrentRow.Cells[0].Value.ToString();
                txt_nombre.Text = dgv_cliente.CurrentRow.Cells[1].Value.ToString();
                txt_direcc.Text = dgv_cliente.CurrentRow.Cells[2].Value.ToString();
                txt_telefono.Text = dgv_cliente.CurrentRow.Cells[3].Value.ToString();
                txt_email.Text = dgv_cliente.CurrentRow.Cells[4].Value.ToString();
            }
        }

        private void dgv_cliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_tipoFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            //MessageBox.Show("funcionando: "+txt_tipoFiltro.Text );

            machine = new Maquina(txt_tipoFiltro.Text);
            machine.buscarMaquina_Tipo();
            cargarMaquinaDisponiblesTipo();
            //cargarMaquinasDisponibles();
        }

        private void tPg_DA_Click(object sender, EventArgs e)
        {

        }

        private void txt_filtro_KeyUp(object sender, KeyEventArgs e)
        {
            txt_ident.Text = txt_filtro.Text;
            if (rbtn_ci.Checked == true)
            {
                nat = new ClienteNat(txt_filtro.Text);
                //Lo va guardar en la base de datos
                nat.buscarClientNat();
                //refresca la tabla
                cargarClienteNat();
            }

            if (rbtn_ruc.Checked == true)
            {
                emp = new ClienteEmp(txt_filtro.Text);
                emp.buscarClienteEmp();
                cargarClienteEmp();
            }
            txt_ident.Text = txt_filtro.Text;
        }

        private void txt_filtro_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            string num = "" + c;

            /*valida numeros de caracteres permitidos a ingresar*/
            if (rbtn_ci.Checked == true)
            {
                txt_filtro.MaxLength = 10;
            }

            if (rbtn_ruc.Checked == true)
            {
                txt_filtro.MaxLength = 13;
            }

            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;


                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                    txt_nombre.Focus();

            }
        }

        private void tabControl1_BindingContextChanged(object sender, EventArgs e)
        {
        }

        private void txt_filtro_Leave(object sender, EventArgs e)
        {
            txt_ident.Focus();
        }

        private void txt_date_TextChanged(object sender, EventArgs e)
        {

        }



 
        




        

    }
}
