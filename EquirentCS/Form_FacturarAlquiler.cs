﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS
{
    public partial class Form_FacturarAlquiler : Form
    {
        SeleccionarAlquiler seleccionAlquiler;
        MaquinariasAlquiladas maquinasAlquiladas;
        Maquina maq = new Maquina();
        alquilerMaquinas AlqMaq = new alquilerMaquinas();
        Factura factura = new Factura();

        Hashtable rowsMaquinas = new Hashtable();

        int alquilerAFacturar;

        float valorTotal = 0;

        Validaciones valida = new Validaciones();
        public Form_FacturarAlquiler()
        {
            InitializeComponent();
        }


        private void cargarSeleccionAlquilerCN()
        {
            if (seleccionAlquiler.buscarVistaClienteNat())
            {
                dgv_cliente.DataSource = seleccionAlquiler.d_tb;
            }
            else
            {
                MessageBox.Show("Error en la carga de seleccion de alquiler CN");
            }
        }

        private void cargarSeleccionAlquilerEMP()
        {
            if (seleccionAlquiler.buscarVistaClienteEmp())
            {
                dgv_cliente.DataSource = seleccionAlquiler.d_tb;
            }
            else
            {
                MessageBox.Show("Error en la carga de seleccion de alquiler EMP");
            }
        }

        private void cargarSeleccionAlquilerCodigo(string codigo)
        {
            if (seleccionAlquiler.buscarVistaCodigo(codigo))
            {
                dgv_cliente.DataSource = seleccionAlquiler.d_tb;
            }
            else
            {
                MessageBox.Show("Error en la carga de seleccion de alquiler por codigo");
            }
        }


        private void cargarMaquinasAlquiler()
        {
            if (maquinasAlquiladas.buscarMaquinariasAlquiladas())
            {
                dgv_maquina.DataSource = maquinasAlquiladas.d_tb;
            }
        }




        private void rbtn_ruc_CheckedChanged(object sender, EventArgs e)
        {
            txt_filtro_alquiler.Clear();
            txt_filtro_alquiler.Enabled = true;
            txt_filtro_alquiler.MaxLength = 13;
            btn_limpiar_Click(sender, e);
        }

        private void rbtn_ci_CheckedChanged(object sender, EventArgs e)
        {
            txt_filtro_alquiler.Clear();
            txt_filtro_alquiler.Enabled = true;
            txt_filtro_alquiler.MaxLength = 10;

            btn_limpiar_Click(sender, e);
        }



        private void btn_facturar_Click(object sender, EventArgs e)
        {
            String[] llenado = new string[8];

            llenado[0] = txt_desc.Text;
            llenado[1] = txt_subtotal.Text;
            llenado[2] = txt_total.Text;
            llenado[3] = txt_iva.Text;
            llenado[4] = txt_detalle.Text;

            if (!valida.esta_vacio(llenado[0]) &
               !valida.esta_vacio(llenado[1]) &
               !valida.esta_vacio(llenado[2]) &
               !valida.esta_vacio(llenado[3]) &
                !valida.esta_vacio(llenado[4]))
            {

                //Faltan asignar valores a usuario y descripcion
                try{
                    int codAlquiler = alquilerAFacturar;
                    string descripcion = txt_detalle.Text;
                    float valor = 0;
                    float.TryParse(txt_total.Text, out valor);
                    float iva = 0;
                    float.TryParse(txt_iva.Text, out iva);
                    float descuento = 0;
                    float.TryParse(txt_desc.Text, out descuento);
                    string fecha = date.Value.ToString("yyyy-MM-dd");
                    string usuario = "admin";

                    factura = new Factura(codAlquiler, descripcion, valor, iva, descuento,
                        fecha, usuario);

                    if (factura.agregarFactura())
                    {
                        MessageBox.Show("Facturación exitosa");
                    }
                    else 
                    { 
                        MessageBox.Show("Ha ocurrido un error y no se pudo facturar '02'"); 
                    }


                    foreach (DictionaryEntry entry in rowsMaquinas)
                    {
                        string placa = (string)entry.Key;
                        string devolver = (string)entry.Value;

                        if (devolver == "TRUE")
                        {
                            AlqMaq = new alquilerMaquinas(alquilerAFacturar.ToString(), placa);
                            AlqMaq.devolverAlquilerMaquina();
                        }
                    }


                }catch{
                    MessageBox.Show("Ha ocurrido un error y no se pudo facturar '01'");
                }
                finally
                {
                    this.Close();
                }
            }
            else
                MessageBox.Show("FALTA LLENAR ALGUN CAMPO");
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            btn_aceptar.Enabled = false;
            
            txt_filtro_alquiler.Clear();
            txt_desc.Clear();
            txt_desc.Enabled = false;
            txt_subtotal.Clear();
            txt_total.Clear();
            txt_iva.Clear();
            dgv_cliente.DataSource = null;
            dgv_maquina.DataSource = null;
            dgv_maquina.Enabled = true;
            dgv_detalle.Rows.Clear();

            rowsMaquinas = new Hashtable();
            //foreach (DataGridViewRow row in dgv_detalle.Rows)
            //{
            //    dgv_detalle.Rows.Remove(row);
            //}

        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void rdbtn_nombre_CheckedChanged(object sender, EventArgs e)
        {
            txt_filtro_alquiler.Clear();
            txt_filtro_alquiler.Enabled = true;
            txt_filtro_alquiler.MaxLength = 200;
        }

        private void txt_filtro_alquiler_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            string num = "" + c;


            if (rbtn_ci.Checked == true || rbtn_ruc.Checked == true || rdBtn_codAlquiler.Checked == true)
            {
                /*Valida que los datos ingresados solo sean  numeros*/
                if (!valida.sonNumeros(num))
                {
                    e.Handled = true;
                    /*valida para que se pueda borrar mientras se ingresa la informacion*/
                    if (char.IsControl(e.KeyChar))
                        e.Handled = false;

                }
            }

            //if(rdbtn_nombre.Checked==true)
            //{
            //    if (!valida.sonletras("" + e.KeyChar))
            //    {
            //        e.Handled = true;
            //        if (char.IsSeparator(e.KeyChar))
            //        {
            //            e.Handled = false;
            //        }
            //        if (char.IsControl(e.KeyChar))
            //        {
            //            e.Handled = false;
            //        }

            //    }
            //}
        }

        private void txt_filtro_alquiler_Leave(object sender, EventArgs e)
        {

        }

        private void rdBtn_codAlquiler_CheckedChanged(object sender, EventArgs e)
        {
            txt_filtro_alquiler.Clear();
            txt_filtro_alquiler.Enabled = true;
            txt_filtro_alquiler.MaxLength = 200;
        }

        private void txt_subtotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if (e.KeyChar == ',' || e.KeyChar == '.')
                    e.Handled = false;
            }
        }

        private void txt_total_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if (e.KeyChar == ',' || e.KeyChar == '.')
                    e.Handled = false;
            }
        }

        private void txt_subtotal_Leave(object sender, EventArgs e)
        {
            if (txt_subtotal.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.sondecimales(txt_subtotal.Text))
                {
                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    txt_subtotal.Clear();
                }
            }
        }

        private void txt_total_Leave(object sender, EventArgs e)
        {
            if (txt_total.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.sondecimales(txt_total.Text))
                {
                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    txt_total.Clear();
                }
            }
        }

        private void txt_desc_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if (e.KeyChar == ',' || e.KeyChar == '.')
                    e.Handled = false;
            }
        }

        private void txt_iva_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
                if (e.KeyChar == ',' || e.KeyChar == '.')
                    e.Handled = false;
            }
        }

        private void txt_desc_Leave(object sender, EventArgs e)
        {
            if (txt_desc.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.sondecimales(txt_desc.Text))
                {

                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    txt_desc.Clear();
                }
            }
        }

        private void txt_iva_Leave(object sender, EventArgs e)
        {
            if (txt_iva.Text != "" & btn_cancelar.ContainsFocus == false)
            {
                if (!valida.sondecimales(txt_iva.Text))
                {
                    MessageBox.Show("VALOR INGRESADO NO DECIMAL");
                    txt_iva.Clear();
                }
            }
        }

        private void Form_FacturarAlquiler_Load(object sender, EventArgs e)
        {
            date.MinDate = DateTime.Today;

            seleccionAlquiler = new SeleccionarAlquiler();
            maquinasAlquiladas = new MaquinariasAlquiladas();
        }

        private void txt_filtro_alquiler_KeyUp(object sender, KeyEventArgs e)
        {

            if (dgv_maquina.DataSource != null)
            {
                dgv_maquina.DataSource = null;
            }
            

            if (rbtn_ci.Checked == true)
            {
                seleccionAlquiler = new SeleccionarAlquiler(txt_filtro_alquiler.Text);
                //Lo va guardar en la base de datos
                seleccionAlquiler.buscarVistaClienteNat();
                //refresca la tabla
                cargarSeleccionAlquilerCN();
            }

            if (rbtn_ruc.Checked == true)
            {
                seleccionAlquiler = new SeleccionarAlquiler(txt_filtro_alquiler.Text);
                seleccionAlquiler.buscarVistaClienteEmp();
                cargarSeleccionAlquilerEMP();
            }

            if (rdBtn_codAlquiler.Checked == true)
            {
                seleccionAlquiler = new SeleccionarAlquiler();
                seleccionAlquiler.buscarVistaCodigo(txt_filtro_alquiler.Text);
                cargarSeleccionAlquilerCodigo(txt_filtro_alquiler.Text);
            }
        }

        private void dgv_cliente_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dgv_cliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_desc.Clear();
            txt_desc.Enabled = false;
            txt_subtotal.Clear();
            txt_total.Clear();
            txt_iva.Clear();
            dgv_detalle.Rows.Clear();

            //Obtener el codigo del alquiler del gridview
            string str = dgv_cliente.CurrentRow.Cells[2].Value.ToString();

            if (str != "")
            {
                btn_aceptar.Enabled = true;
                //txt_filtro_alquiler.Text = dgv_cliente.CurrentRow.Cells[0].Value.ToString();

                //guardar el alquiler para utilizarlo en el metodo de facturacion
                alquilerAFacturar = Int32.Parse(dgv_cliente.CurrentRow.Cells[2].Value.ToString());

                maquinasAlquiladas = new MaquinariasAlquiladas(alquilerAFacturar);

                maquinasAlquiladas.buscarMaquinariasAlquiladas();
                cargarMaquinasAlquiler();

                try
                {
                    foreach (DataGridViewRow row in dgv_maquina.Rows)
                    {
                        row.Cells[0].Value = "FALSE";
                    }
                }
                catch
                {
                    MessageBox.Show("Error en dgv_cliente_CellClick '03'");
                }

            }
            else
            {
                dgv_maquina.DataSource = null;
            }

        }

        private void btn_aceptar_Click(object sender, EventArgs e)
        {
            dgv_detalle.Rows.Clear();
            txt_detalle.Enabled = true;
            dgv_maquina.Enabled = false;
            
            
            float subtotal = 0;
            float iva = 0;

            // Obteniendo el codigo de todas las maquinas
            try
            {
                foreach (DataGridViewRow row in dgv_maquina.Rows)
                {
                    if (row.Cells[1].Value != null)
                    {
                        rowsMaquinas[row.Cells[1].Value.ToString()] = row.Cells[0].Value.ToString();
                    }
                    
                }


                // Generando las horas totales a facturar por cada maquina

                if (rowsMaquinas.Keys.Count > 0){

                    DateTime dT_inicioAlquiler = DateTime.Parse(dgv_cliente.CurrentRow.Cells[3].Value.ToString());

                    string f_inicio = dT_inicioAlquiler.ToString("yyyy-MM-dd");
                    string f_facturacion = date.Value.ToString("yyyy-MM-dd");

                    //Asigno el costo por hora, obteniendolo del gdv_cliente, 5ta columna 'CostoHora'
                    float costoPorHora = 0;
                    float.TryParse(dgv_cliente.CurrentRow.Cells[4].Value.ToString(), out costoPorHora);

                    foreach (DictionaryEntry entry in rowsMaquinas)
                    {
                        string placa = (string)entry.Key;
                        int horasTrabajadas = maq.getHorasTrabajadas(placa, f_inicio, f_facturacion);

                        float totalPagarMaquina = horasTrabajadas * costoPorHora;
                        subtotal += totalPagarMaquina;

                        dgv_detalle.Rows.Add(placa, horasTrabajadas.ToString(),
                                        costoPorHora.ToString(), totalPagarMaquina.ToString());

                    }

                    iva = subtotal * 0.12f;

                    txt_subtotal.Text = subtotal.ToString();
                    txt_iva.Text = iva.ToString();
                    txt_total.Text = (subtotal + iva).ToString();
                    txt_desc.Enabled = true;
                    txt_desc.Text = "0";

                    valorTotal = (subtotal + iva);
                }

            }
            catch
            {
                MessageBox.Show("Ocurrió un error '00'.");
            }
        }

        private void txt_desc_KeyUp(object sender, KeyEventArgs e)
        {
            float vDesc = 0;
            float newTotal  = 0;

            float.TryParse(txt_desc.Text, out vDesc);

            newTotal = Math.Max(0.0f, (valorTotal - vDesc));

            txt_total.Text = newTotal.ToString();
        }

        private void dgv_cliente_Leave(object sender, DataGridViewCellEventArgs e)
        {
            btn_limpiar_Click(sender, e);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
