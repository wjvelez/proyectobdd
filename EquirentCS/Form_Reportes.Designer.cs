﻿namespace EquirentCS
{
    partial class Form_Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rB_alquileres = new System.Windows.Forms.RadioButton();
            this.rB_facturas = new System.Windows.Forms.RadioButton();
            this.btn_ConsultarR = new System.Windows.Forms.Button();
            this.btn_Salir = new System.Windows.Forms.Button();
            this.gB_tipo = new System.Windows.Forms.GroupBox();
            this.rB_planillajes = new System.Windows.Forms.RadioButton();
            this.gB_tipo.SuspendLayout();
            this.SuspendLayout();
            // 
            // rB_alquileres
            // 
            this.rB_alquileres.AutoSize = true;
            this.rB_alquileres.Location = new System.Drawing.Point(12, 49);
            this.rB_alquileres.Name = "rB_alquileres";
            this.rB_alquileres.Size = new System.Drawing.Size(118, 22);
            this.rB_alquileres.TabIndex = 3;
            this.rB_alquileres.TabStop = true;
            this.rB_alquileres.Text = "ALQUILERES";
            this.rB_alquileres.UseVisualStyleBackColor = true;
            // 
            // rB_facturas
            // 
            this.rB_facturas.AutoSize = true;
            this.rB_facturas.Location = new System.Drawing.Point(12, 21);
            this.rB_facturas.Name = "rB_facturas";
            this.rB_facturas.Size = new System.Drawing.Size(105, 22);
            this.rB_facturas.TabIndex = 1;
            this.rB_facturas.TabStop = true;
            this.rB_facturas.Text = "FACTURAS";
            this.rB_facturas.UseVisualStyleBackColor = true;
            this.rB_facturas.CheckedChanged += new System.EventHandler(this.rB_facturas_CheckedChanged);
            // 
            // btn_ConsultarR
            // 
            this.btn_ConsultarR.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ConsultarR.Location = new System.Drawing.Point(193, 25);
            this.btn_ConsultarR.Name = "btn_ConsultarR";
            this.btn_ConsultarR.Size = new System.Drawing.Size(119, 36);
            this.btn_ConsultarR.TabIndex = 4;
            this.btn_ConsultarR.Text = "CONSULTAR";
            this.btn_ConsultarR.UseVisualStyleBackColor = true;
            this.btn_ConsultarR.Click += new System.EventHandler(this.btn_ConsultarR_Click);
            // 
            // btn_Salir
            // 
            this.btn_Salir.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Salir.Location = new System.Drawing.Point(193, 81);
            this.btn_Salir.Name = "btn_Salir";
            this.btn_Salir.Size = new System.Drawing.Size(119, 36);
            this.btn_Salir.TabIndex = 5;
            this.btn_Salir.Text = "SALIR";
            this.btn_Salir.UseVisualStyleBackColor = true;
            this.btn_Salir.Click += new System.EventHandler(this.btn_Salir_Click);
            // 
            // gB_tipo
            // 
            this.gB_tipo.Controls.Add(this.rB_planillajes);
            this.gB_tipo.Controls.Add(this.rB_alquileres);
            this.gB_tipo.Controls.Add(this.rB_facturas);
            this.gB_tipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gB_tipo.Location = new System.Drawing.Point(7, 11);
            this.gB_tipo.Name = "gB_tipo";
            this.gB_tipo.Size = new System.Drawing.Size(170, 113);
            this.gB_tipo.TabIndex = 6;
            this.gB_tipo.TabStop = false;
            this.gB_tipo.Text = "Reportes";
            this.gB_tipo.Enter += new System.EventHandler(this.gB_tipo_Enter);
            // 
            // rB_planillajes
            // 
            this.rB_planillajes.AutoSize = true;
            this.rB_planillajes.Location = new System.Drawing.Point(12, 77);
            this.rB_planillajes.Name = "rB_planillajes";
            this.rB_planillajes.Size = new System.Drawing.Size(120, 22);
            this.rB_planillajes.TabIndex = 5;
            this.rB_planillajes.TabStop = true;
            this.rB_planillajes.Text = "PLANILLAJES";
            this.rB_planillajes.UseVisualStyleBackColor = true;
            this.rB_planillajes.CheckedChanged += new System.EventHandler(this.rB_planillajes_CheckedChanged);
            // 
            // Form_Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(319, 127);
            this.Controls.Add(this.gB_tipo);
            this.Controls.Add(this.btn_Salir);
            this.Controls.Add(this.btn_ConsultarR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Reportes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reportes";
            this.gB_tipo.ResumeLayout(false);
            this.gB_tipo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rB_facturas;
        private System.Windows.Forms.RadioButton rB_alquileres;
        private System.Windows.Forms.Button btn_ConsultarR;
        private System.Windows.Forms.Button btn_Salir;
        private System.Windows.Forms.GroupBox gB_tipo;
        private System.Windows.Forms.RadioButton rB_planillajes;
    }
}