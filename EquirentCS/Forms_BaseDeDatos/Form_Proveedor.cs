﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_Proveedor : Form
    {
        DialogResult confirmacion;
        Validaciones valida = new Validaciones();
        proveedor pro;
        public Form_Proveedor()
        {
            InitializeComponent();

        }

        public void cargar()
        {
            if (pro.leerProveedor())
            {
                dgv_proveedor.DataSource = pro.d_tb;
            }
            else
                MessageBox.Show("ERROR");
        }


        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gB_ingresar_Enter(object sender, EventArgs e)
        {

        }

        //private void btn_limpiar_Click(object sender, EventArgs e)
        //{
        //    txt_nombre.Clear();
        //    txt_id.Clear();
        //    txt_telefono.Clear();
        //    txt_direccion.Clear();
        //}

        //private void btn_agregar_Click(object sender, EventArgs e)
        //{
        //    String mod = "Modificar";
        //    String elim = "Eliminar";

        //    String nombre = txt_nombre.Text.ToString();
        //    String id = txt_id.Text.ToString();
        //    String telf = txt_telefono.Text.ToString();
        //    String direccion = txt_direccion.Text.ToString();

        //    dgv_proveedor.Rows.Add(id, nombre, direccion, telf, mod, elim);

        //    btn_limpiar_Click(sender, e);
        //}

        //private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!valida.sonletras("" + e.KeyChar))
        //    {
        //        e.Handled = true;
        //        if (char.IsSeparator(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }
        //        if (char.IsControl(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }

        //        if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //        {
        //            txt_direccion.Focus();
        //        }
        //    }
        //}

        private void txt_direccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            valida.letra_num(e);
            if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
            {
                //txt_id.Focus();
            }
        }

        //private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    txt_id.MaxLength = 13;
        //    char c = e.KeyChar;
        //    string num = "" + c;
        //    if (!valida.sonNumeros(num))
        //    {
        //        e.Handled = true;
        //        /*valida para que se pueda borrar mientras se ingresa la informacion*/
        //        if (char.IsControl(e.KeyChar))
        //            e.Handled = false;

        //        if (e.KeyChar == Convert.ToChar(Keys.Enter))
        //            txt_telefono.Focus();

        //    }
        //}

        //private void txt_id_Leave(object sender, EventArgs e)
        //{
        //    if (txt_id.Text != "" )
        //    {

        //            if (!valida.num_13(txt_id.Text))
        //            {
        //                MessageBox.Show("RUC DEBE TENER 13 DIGITOS");
        //                txt_id.Clear();
        //            }
        //    }
        //}

        private void txt_telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void Form_Proveedor_Load(object sender, EventArgs e)
        {
            pro = new proveedor();
            cargar();
        }

        private void dgv_proveedor_Click(object sender, EventArgs e)
        {

        }

        private void dgv_proveedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ClickEnBoton(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


            //try
            //{
            //    if (dgv_proveedor.CurrentCell.ColumnIndex == 4)
            //    {
            //        var row = dgv_proveedor.CurrentRow;
            //        Form_EditarProveedor frm = new Form_EditarProveedor();

            //        var nombre = frm.Controls["txt_nombre"];
            //        var id = frm.Controls["txt_id"];
            //        var telf = frm.Controls["txt_telefono"];
            //        var direccion = frm.Controls["txt_direccion"];

            //        id.Text = row.Cells[0].Value.ToString();
            //        nombre.Text = row.Cells[1].Value.ToString();
            //        direccion.Text = row.Cells[2].Value.ToString();
            //        telf.Text = row.Cells[3].Value.ToString();

            //        if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //        {
            //            row.Cells[0].Value = id.Text;
            //            row.Cells[1].Value = nombre.Text;
            //            row.Cells[2].Value = direccion.Text;
            //            row.Cells[3].Value = telf.Text;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("******No existen datos para modificar****");
            //}





        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {
            txt_ruc.Text = dgv_proveedor.CurrentRow.Cells[0].Value.ToString();
            txt_nombre.Text = dgv_proveedor.CurrentRow.Cells[1].Value.ToString();
            txt_direccion.Text = dgv_proveedor.CurrentRow.Cells[2].Value.ToString();
            txt_telefono.Text = dgv_proveedor.CurrentRow.Cells[3].Value.ToString();

        }

        private void btn_guardar_Click_1(object sender, EventArgs e)
        {

            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pro = new proveedor(txt_ruc.Text, txt_nombre.Text, txt_direccion.Text, txt_telefono.Text);

                //Lo va guardar en la base de datos
                pro.agregarProveedor();
                //refresca la tabla
                cargar();
            }
        }

        private void btn_modificar_Click_1(object sender, EventArgs e)
        {

            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pro = new proveedor(txt_ruc.Text, txt_nombre.Text, txt_direccion.Text, txt_telefono.Text);

                //Lo va guardar en la base de datos
                pro.modificarProveedor();
                //refresca la tabla
                cargar();
            }

        }

        private void btn_eliminar_Click_1(object sender, EventArgs e)
        {

            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                pro = new proveedor(txt_ruc.Text, txt_nombre.Text, txt_direccion.Text, txt_telefono.Text);
                //Lo va guardar en la base de datos

                pro.eliminarProveedor();

                //refresca la tabla
                cargar();
            }
        }

        //funcion que debe actualizar a la base de datos.... y limpiar los text box
        private void click_btn_actualizar(object sender, EventArgs e)
        {
            txt_ruc.Clear();
            txt_nombre.Clear();
            txt_direccion.Clear();
            txt_telefono.Clear();
        }

        private void txt_direccion_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            txt_direccion.MaxLength = 255;
            valida.letra_num(e);
        }

        private void txt_telefono_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            txt_telefono.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_nombre.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

            }
        }

        private void txt_ruc_KeyPress(object sender, KeyPressEventArgs e)
        {

            char c = e.KeyChar;
            string num = "" + c;

                txt_ruc.MaxLength = 13;


            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;


            }
        }

        private void txt_ruc_Leave(object sender, EventArgs e)
        {

            if (txt_ruc.Text != "")
            {

                if (!valida.num_13(txt_ruc.Text))
                {
                    MessageBox.Show("RUC DEBE TENER 13 DIGITOS");
                    txt_ruc.Clear();
                }

            }
        }







    }
}
