﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_Maquinaria : Form
    {
        Validaciones valida = new Validaciones();
        Maquina machine;
        DialogResult confirmacion;

        public Form_Maquinaria()
        {
            InitializeComponent();

        }

        public void cargar()
        {
            if(machine.leerMaquina())
            {
                dgv_maquinaria.DataSource = machine.d_tb;
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private void tPg_DC_Click(object sender, EventArgs e)
        {

        }

        private void Form_maquinaria_Load(object sender, EventArgs e)
        {
            machine = new Maquina();
            cargar();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("DATO ELIMINADO");
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Datos restablecidos");
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private void btn_agregar_Click_1(object sender, EventArgs e)
        //{
        //    String mod = "Modificar";
        //    String elim = "Eliminar";

        //    String placa = txt_placa.Text.ToString();
        //    String modelo = txt_modelo.Text.ToString();
        //    String tipo = txt_tipo.Text.ToString();
        //    String horometro = txt_horometro.Text.ToString();
        //    String marca = txt_marca.Text.ToString();

        //    dgv_maquinaria.Rows.Add(placa, modelo, tipo, horometro, marca, mod, elim);

        //    btn_limpiar_Click(sender, e);
        //}

        //private void btn_limpiar_Click(object sender, EventArgs e)
        //{
        //    txt_placa.Clear();
        //    txt_modelo.Clear();
        //    txt_tipo.Clear();
        //    txt_horometro.Clear();
        //    txt_marca.Clear();
        //}

        //private void gB_ingresar_Enter(object sender, EventArgs e)
        //{

        //}

        //private void txt_modelo_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    valida.letra_num(e);
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    {
        //        txt_tipo.Focus();
        //    }
        //}

        //private void txt_tipo_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    valida.letra_num(e);
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    {
        //        txt_marca.Focus();
        //    }
        //}

        //private void txt_marca_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    valida.letra_num(e);
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    {
        //        txt_placa.Focus();
        //    }
        //}

        //private void txt_placa_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    valida.letra_num(e);
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    {
        //        txt_horometro.Focus();
        //    }
        //}

        //private void txt_horometro_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!valida.sonNumeros("" + e.KeyChar))
        //    {
        //        e.Handled = true;
        //        if (char.IsControl(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }
        //    }
        //}

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear Operador con lo valores a guardar
                //int i;
                //i = Int32.Parse(txt_horometro.Text);



                machine = new Maquina(txt_placa.Text, txt_marca.Text, txt_tipo.Text, txt_horometro.Text, txt_modelo.Text);
                
                //Lo va guardar en la base de datos
                machine.agregarMaquina();

                //refresca la tabla
                cargar();

            }
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            if (confirmacion == DialogResult.Yes)
            {
                //Crear Operador con lo valores a guardar
                //int i;
                //i = Int32.Parse(txt_horometro.Text);

                machine = new Maquina(txt_placa.Text, txt_marca.Text, txt_tipo.Text, txt_horometro.Text, txt_modelo.Text);

                //Lo va guardar en la base de datos
                machine.modificarMaquina();

                //refresca la tabla
                cargar();
            }
        }

        private void btn_eliminar_Click_1(object sender, EventArgs e)
        {
            if (confirmacion == DialogResult.Yes)
            {
                //Crear Operador con lo valores a guardar
                //int i;
                //i = Int32.Parse(txt_horometro.Text);

                machine = new Maquina(txt_placa.Text, txt_marca.Text, txt_tipo.Text, txt_horometro.Text, txt_modelo.Text);

                //Lo va guardar en la base de datos
                machine.eliminarMaquina();

                //refresca la tabla
                cargar();
            }
        }

        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {

            txt_placa.Text = dgv_maquinaria.CurrentRow.Cells[0].Value.ToString();
            txt_marca.Text = dgv_maquinaria.CurrentRow.Cells[1].Value.ToString();
            txt_tipo.Text = dgv_maquinaria.CurrentRow.Cells[2].Value.ToString();
            txt_horometro.Text = dgv_maquinaria.CurrentRow.Cells[3].Value.ToString();
            txt_modelo.Text = dgv_maquinaria.CurrentRow.Cells[4].Value.ToString();

        }



        private void click_btn_actualizar(object sender, EventArgs e)
        {
            txt_placa.Clear();
            txt_marca.Clear();
            txt_tipo.Clear();
            txt_horometro.Clear();
            txt_modelo.Clear();
        }

        private void txt_placa_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_placa.MaxLength = 7;
            valida.letra_num(e);
            if (e.KeyChar == '-')//Keys Espicifica los modificadores y codigos de tecla
            {
                e.Handled = false;
            }
        }

        private void txt_marca_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_placa.MaxLength = 30;
            valida.letra_num(e);
            if (e.KeyChar == '-')//Keys Espicifica los modificadores y codigos de tecla
            {
                e.Handled = false;
            }
        }

        private void txt_tipo_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_tipo.MaxLength = 30;
            valida.letra_num(e);
            if (e.KeyChar == '-')//Keys Espicifica los modificadores y codigos de tecla
            {
                e.Handled = false;
            }
        }

        private void txt_modelo_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_modelo.MaxLength = 30;
            valida.letra_num(e);
            if (e.KeyChar == '-')//Keys Espicifica los modificadores y codigos de tecla
            {
                e.Handled = false;
            }
        }

        private void txt_horometro_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_horometro.MaxLength = 10;




            char c = e.KeyChar;
            string num = "" + c;


            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;
            }
        }
    }
}
