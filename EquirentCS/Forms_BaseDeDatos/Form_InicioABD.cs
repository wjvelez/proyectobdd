﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_InicioABD : Form
    {
        public Form_InicioABD()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_Maquinaria frm = new Form_Maquinaria();
            frm.Show();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            Form_Cliente frm = new Form_Cliente();
            frm.Show();
        }

        private void btnProveedor_Click(object sender, EventArgs e)
        {
            Form_Proveedor frm = new Form_Proveedor();
            frm.Show();
        }

        private void btnOperador_Click(object sender, EventArgs e)
        {
            Form_Operador frm = new Form_Operador();
            frm.Show();
        }

        private void btnMaquinaria_Click(object sender, EventArgs e)
        {
            Form_Maquinaria frm = new Form_Maquinaria();
            frm.Show();
        }

        private void Form_InicioABD_Load(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
