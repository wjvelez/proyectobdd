﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_Cliente : Form
    {

        Validaciones valida = new Validaciones();
        ClienteEmp emp;
        ClienteNat nat;
        DialogResult confirmacion;


        public Form_Cliente()
        {
            InitializeComponent();

        }

        /*Metodo para cargar los valores en el FORM_CLIENT*/
        public void cargarClienteNat()
        {
            if (nat.leerClienteNat())
            {
                dgv_PersNat.DataSource = nat.d_tb;
            }
            else
                MessageBox.Show("ERROR");
        }

        /*Metodo para cargar los valores en el FORM_CLIENT*/
        public void cargarClienteEmp()
        {
            if (emp.leerClienteEmp())
            {
                dgv_Empresa.DataSource = emp.d_tb;
            }
            else
                MessageBox.Show("ERROR");
        }



        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void rbtn_personas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btn_aceptar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Modificacion Exitosa");
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cliente restablecido");
            this.Close();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Eliminacion exitosa");
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void Form_Cliente_Load(object sender, EventArgs e)
        {
            emp = new ClienteEmp();
            nat = new ClienteNat();
            cargarClienteNat();
            cargarClienteEmp();
        }

        private void dataGridView2_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            //txt_nombres.Clear();
            //txt_apellidos.Clear();
            //txt_id.Clear();
            //txt_telefono.Clear();
            //txt_email.Clear();
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            //String mod = "Modificar";
            //String elim = "Eliminar";

            //String nombre = txt_nombres.Text.ToString();
            //String apellido = txt_apellidos.Text.ToString();
            //String id = txt_id.Text.ToString();
            //String telf = txt_telefono.Text.ToString();
            //String email = txt_email.Text.ToString();

            //dgv_cliente.Rows.Add(nombre, apellido, id, telf, email, mod, elim);

            //btn_limpiar_Click(sender, e);
        }

        private void ClickEnBoton(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    if (dgv_cliente.CurrentCell.ColumnIndex == 5)
            //    {
            //        var row = dgv_cliente.CurrentRow;
            //        Form_EditarCliente frm = new Form_EditarCliente();

            //        var nombre = frm.Controls["txt_nombres"];
            //        var apellido = frm.Controls["txt_apellidos"];
            //        var id = frm.Controls["txt_id"];
            //        var telf = frm.Controls["txt_telefono"];
            //        var email = frm.Controls["txt_email"];

            //        nombre.Text = row.Cells[0].Value.ToString();
            //        apellido.Text = row.Cells[1].Value.ToString();
            //        id.Text = row.Cells[2].Value.ToString();
            //        telf.Text = row.Cells[3].Value.ToString();
            //        email.Text = row.Cells[4].Value.ToString();

            //        if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //        {
            //            row.Cells[0].Value = nombre.Text;
            //            row.Cells[1].Value = apellido.Text;
            //            row.Cells[2].Value = id.Text;
            //            row.Cells[3].Value = telf.Text;
            //            row.Cells[4].Value = email.Text;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("No existen datos que modificar");
            //}
            try
            {
                if (dgv_PersNat.CurrentCell.ColumnIndex == 5)
                {
                    var row = dgv_PersNat.CurrentRow;
                    dgv_PersNat.Rows.Remove(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No existen datos que eliminar");
            }


        }

        private void txt_nombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!valida.sonletras("" + e.KeyChar))
            //{
            //    e.Handled = true;
            //    if (char.IsSeparator(e.KeyChar))
            //    {
            //        e.Handled = false;
            //    }
            //    if (char.IsControl(e.KeyChar))
            //    {
            //        e.Handled = false;
            //    }

            //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
            //    {
            //        txt_apellidos.Focus();
            //    }
            //  }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgv_Empresa.CurrentCell.ColumnIndex == 4)
                {
                    var row = dgv_Empresa.CurrentRow;
                    dgv_Empresa.Rows.Remove(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No existen datos que eliminar");
            }
        }

        private void click_dgv_emp(object sender, DataGridViewCellEventArgs e)
        {
            txt_ruc.Text = dgv_Empresa.CurrentRow.Cells[0].Value.ToString();
            txt_nombreEmp.Text = dgv_Empresa.CurrentRow.Cells[1].Value.ToString();
            txt_dirEmp.Text = dgv_Empresa.CurrentRow.Cells[2].Value.ToString();
            txt_telfEmp.Text = dgv_Empresa.CurrentRow.Cells[3].Value.ToString();
            txt_emailEmp.Text = dgv_Empresa.CurrentRow.Cells[4].Value.ToString();
        }

        private void click_dgv_nat(object sender, DataGridViewCellEventArgs e)
        {
            txt_ci.Text = dgv_PersNat.CurrentRow.Cells[0].Value.ToString();
            txt_nombre.Text = dgv_PersNat.CurrentRow.Cells[1].Value.ToString();
            txt_dir.Text = dgv_PersNat.CurrentRow.Cells[2].Value.ToString();
            txt_telf.Text = dgv_PersNat.CurrentRow.Cells[3].Value.ToString();
            txt_mail.Text = dgv_PersNat.CurrentRow.Cells[4].Value.ToString();
        }


        private void click_btn_actualizarPN(object sender, EventArgs e)
        {
            txt_ci.Clear();
            txt_nombre.Clear();
            txt_dir.Clear();
            txt_telf.Clear();
            txt_mail.Clear();
        }

        private void click_btn_actualizarE(object sender, EventArgs e)
        {
            txt_ruc.Clear();
            txt_nombreEmp.Clear();
            txt_dirEmp.Clear();
            txt_telfEmp.Clear();
            txt_emailEmp.Clear();
        }

        private void btn_guardarPN_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear Cliente Natural con lo valores a guardar
                nat = new ClienteNat(txt_ci.Text, txt_nombre.Text, txt_dir.Text, txt_telf.Text, txt_mail.Text);

                //Lo va guardar en la base de datos
                nat.agregarClienteNat();

                //refresca la tabla
                cargarClienteNat();

            }
        }

        private void btn_guardarE_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear Cliente Empresa con lo valores a guardar
                 emp = new ClienteEmp(txt_ruc.Text, txt_nombreEmp.Text, txt_dirEmp.Text, txt_telfEmp.Text, txt_emailEmp.Text);

                //Lo va guardar en la base de datos
                emp.agregarClienteEmp();

                //refresca la tabla
                cargarClienteEmp();

            }
        }

        private void btn_modificarPN_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                nat = new ClienteNat(txt_ci.Text, txt_nombre.Text, txt_dir.Text, txt_telf.Text, txt_mail.Text);

                //Lo va guardar en la base de datos
                nat.modificarClienteNat();
                //refresca la tabla
                cargarClienteNat();
            }
        }

        private void btn_modE_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                emp = new ClienteEmp(txt_ruc.Text, txt_nombreEmp.Text, txt_dirEmp.Text, txt_telfEmp.Text, txt_emailEmp.Text);

                //Lo va guardar en la base de datos
                emp.modificarClienteEmp();
                //refresca la tabla
                cargarClienteEmp();
            }
        }

        private void btn_eliminarPN_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                nat = new ClienteNat(txt_ci.Text, txt_nombre.Text, txt_dir.Text, txt_telf.Text, txt_mail.Text);
                //Lo va guardar en la base de datos
                nat.eliminarClienteNat();

                //refresca la tabla
                cargarClienteNat();
            }
        }

        private void btn_elimE_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                emp = new ClienteEmp(txt_ruc.Text, txt_emailEmp.Text, txt_dirEmp.Text, txt_telfEmp.Text, txt_emailEmp.Text);
                //Lo va guardar en la base de datos
                emp.eliminarClienteEmp();

                //refresca la tabla
                cargarClienteEmp();
            }
        }

        private void txt_dir_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_dir.MaxLength = 255;
            valida.letra_num(e);
        }

        private void txt_dirEmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_dirEmp.MaxLength = 255;
            valida.letra_num(e);
            }

        private void txt_telf_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_telf.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_telfEmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_telfEmp.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_mail_MouseLeave(object sender, EventArgs e)
        {

            if (txt_mail.Text != "" )
            {
                if (!valida.esEmail(txt_mail.Text))
                {
                    MessageBox.Show("CORREO INVALIDO\n ej:correo@correo.exts");
                    txt_mail.Clear();

                }
            }
        }

        private void txt_emailEmp_Leave(object sender, EventArgs e)
        {

            if (txt_mail.Text != "")
            {
                if (!valida.esEmail(txt_mail.Text))
                {
                    MessageBox.Show("CORREO INVALIDO\n ej:correo@correo.exts");
                    txt_mail.Clear();

                }
            }

        }

        private void txt_emailEmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_mail.MaxLength = 50;
        }

        private void txt_mail_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_mail.MaxLength = 50;
        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_nombre.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

            }
        }

        private void txt_nombreEmp_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_nombre.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

            }
        }

        private void txt_ci_KeyPress(object sender, KeyPressEventArgs e)
        {

            char c = e.KeyChar;
            string num = "" + c;


            txt_ci.MaxLength = 10;


            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;

            }
        }

        private void txt_ruc_KeyPress(object sender, KeyPressEventArgs e)
        {

            char c = e.KeyChar;
            string num = "" + c;

                txt_ruc.MaxLength = 13;


            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;


            }
        }

        private void txt_ruc_Leave(object sender, EventArgs e)
        {

            if (txt_ruc.Text != "")
            {



                if (!valida.num_13(txt_ruc.Text))
                {
                    MessageBox.Show("RUC DEBE TENER 13 DIGITOS");
                    txt_ruc.Clear();
                }

            }
        }

        private void txt_ci_Leave(object sender, EventArgs e)
        {
            if (txt_ci.Text != "")
            {

                if (!valida.num_10(txt_ci.Text))
                {
                    MessageBox.Show("CI DEBE TENER 10 DIGITOS");
                    txt_ci.Clear();
                }

            }
        }

        private void txt_emailEmp_MouseLeave(object sender, EventArgs e)
        {

            if (txt_emailEmp.Text != "")
            {
                if (!valida.esEmail(txt_emailEmp.Text))
                {
                    MessageBox.Show("CORREO INVALIDO\n ej:correo@correo.exts");

                }
            }
        }
        





        //private void txt_apellidos_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //if (!valida.sonletras("" + e.KeyChar))
        //    //{
        //    //    e.Handled = true;
        //    //    if (char.IsSeparator(e.KeyChar))
        //    //    {
        //    //        e.Handled = false;
        //    //    }
        //    //    if (char.IsControl(e.KeyChar))
        //    //    {
        //    //        e.Handled = false;
        //    //    }

        //    //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    //    {
        //    //        txt_email.Focus();
        //    //    }

        //    }
        //}

        //private void txt_email_Leave(object sender, EventArgs e)
        //{
        //    //if (txt_email.Text != "")
        //    //{
        //    //    if (!valida.esEmail(txt_email.Text))
        //    //    {
        //    //        MessageBox.Show("CORREO INVALIDO\n ej:correo@correo.exts");
        //    //        txt_email.Clear();

        //    //    }
        //    //}
        //}

        //private void txt_email_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    //{
        //    //    txt_id.Focus();
        //    //}

        //}

        //private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //char c = e.KeyChar;
        //    //string num = "" + c;

        //    //txt_id.MaxLength = 10;

        //    ///*Valida que los datos ingresados solo sean  numeros*/
        //    //if (!valida.sonNumeros(num))
        //    //{
        //    //    e.Handled = true;
        //    //    /*valida para que se pueda borrar mientras se ingresa la informacion*/
        //    //    if (char.IsControl(e.KeyChar))
        //    //        e.Handled = false;

        //    //    if (e.KeyChar == Convert.ToChar(Keys.Enter))
        //    //        txt_telefono.Focus();

        //    //}
        //}

        //private void txt_id_Leave(object sender, EventArgs e)
        //{
        //    //if (txt_id.Text != "")
        //    //{

        //    //    if (!valida.num_10(txt_id.Text))
        //    //    {

        //    //        MessageBox.Show("C.I DEBE TENER 10 DIGITOS");
        //    //        txt_id.Clear();
        //    //    }
        //    //}
        //}

        //private void txt_telefono_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    //if (!valida.sonNumeros("" + e.KeyChar))
        //    //{
        //    //    e.Handled = true;
        //    //    if (char.IsControl(e.KeyChar))
        //    //    {
        //    //        e.Handled = false;
        //    //    }
        //    //}
        //}
        
    }
}
