﻿namespace EquirentCS.Forms_BaseDeDatos
{
    partial class Form_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_PersNat = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gB_filtro = new System.Windows.Forms.GroupBox();
            this.txt_filtro = new System.Windows.Forms.TextBox();
            this.rdBtn_id = new System.Windows.Forms.RadioButton();
            this.rdBtn_nombre = new System.Windows.Forms.RadioButton();
            this.btn_salir = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCI = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_fecha = new System.Windows.Forms.Label();
            this.txt_mail = new System.Windows.Forms.TextBox();
            this.btn_eliminarPN = new System.Windows.Forms.Button();
            this.btn_modificarPN = new System.Windows.Forms.Button();
            this.txt_ci = new System.Windows.Forms.TextBox();
            this.lbl_maq = new System.Windows.Forms.Label();
            this.btn_guardarPN = new System.Windows.Forms.Button();
            this.lbl_hF = new System.Windows.Forms.Label();
            this.txt_telf = new System.Windows.Forms.TextBox();
            this.lbl_hI = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.lbl_ci = new System.Windows.Forms.Label();
            this.txt_dir = new System.Windows.Forms.TextBox();
            this.btn_actualizarPerNat = new System.Windows.Forms.Button();
            this.tabRUC = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_emailEmp = new System.Windows.Forms.TextBox();
            this.btn_elimE = new System.Windows.Forms.Button();
            this.btn_modE = new System.Windows.Forms.Button();
            this.txt_ruc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_guardarE = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_telfEmp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_nombreEmp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_dirEmp = new System.Windows.Forms.TextBox();
            this.btn_actualizarEmpresa = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.dgv_Empresa = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direccion1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PersNat)).BeginInit();
            this.gB_filtro.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabCI.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabRUC.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Empresa)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_PersNat
            // 
            this.dgv_PersNat.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_PersNat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_PersNat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.nombres,
            this.Direccion,
            this.telefono,
            this.email});
            this.dgv_PersNat.Location = new System.Drawing.Point(3, 132);
            this.dgv_PersNat.Name = "dgv_PersNat";
            this.dgv_PersNat.Size = new System.Drawing.Size(746, 288);
            this.dgv_PersNat.TabIndex = 8;
            this.dgv_PersNat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_dgv_nat);
            this.dgv_PersNat.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick_1);
            // 
            // id
            // 
            this.id.DataPropertyName = "cedula";
            this.id.HeaderText = "CI";
            this.id.MaxInputLength = 10;
            this.id.Name = "id";
            this.id.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // nombres
            // 
            this.nombres.DataPropertyName = "nombre";
            this.nombres.HeaderText = "Nombres";
            this.nombres.MaxInputLength = 50;
            this.nombres.Name = "nombres";
            this.nombres.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Direccion
            // 
            this.Direccion.DataPropertyName = "direccion";
            this.Direccion.HeaderText = "Direccion";
            this.Direccion.Name = "Direccion";
            // 
            // telefono
            // 
            this.telefono.DataPropertyName = "telefono";
            this.telefono.HeaderText = "Telefono";
            this.telefono.MaxInputLength = 10;
            this.telefono.Name = "telefono";
            this.telefono.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // email
            // 
            this.email.DataPropertyName = "email";
            this.email.HeaderText = "Email";
            this.email.MaxInputLength = 50;
            this.email.Name = "email";
            this.email.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.email.Width = 200;
            // 
            // gB_filtro
            // 
            this.gB_filtro.Controls.Add(this.txt_filtro);
            this.gB_filtro.Controls.Add(this.rdBtn_id);
            this.gB_filtro.Controls.Add(this.rdBtn_nombre);
            this.gB_filtro.Location = new System.Drawing.Point(6, 426);
            this.gB_filtro.Name = "gB_filtro";
            this.gB_filtro.Size = new System.Drawing.Size(392, 45);
            this.gB_filtro.TabIndex = 9;
            this.gB_filtro.TabStop = false;
            this.gB_filtro.Text = "Filtro";
            // 
            // txt_filtro
            // 
            this.txt_filtro.Location = new System.Drawing.Point(158, 16);
            this.txt_filtro.Name = "txt_filtro";
            this.txt_filtro.Size = new System.Drawing.Size(200, 20);
            this.txt_filtro.TabIndex = 2;
            // 
            // rdBtn_id
            // 
            this.rdBtn_id.AutoSize = true;
            this.rdBtn_id.Location = new System.Drawing.Point(6, 17);
            this.rdBtn_id.Name = "rdBtn_id";
            this.rdBtn_id.Size = new System.Drawing.Size(58, 17);
            this.rdBtn_id.TabIndex = 1;
            this.rdBtn_id.TabStop = true;
            this.rdBtn_id.Text = "Cedula";
            this.rdBtn_id.UseVisualStyleBackColor = true;
            // 
            // rdBtn_nombre
            // 
            this.rdBtn_nombre.AutoSize = true;
            this.rdBtn_nombre.Location = new System.Drawing.Point(68, 17);
            this.rdBtn_nombre.Name = "rdBtn_nombre";
            this.rdBtn_nombre.Size = new System.Drawing.Size(62, 17);
            this.rdBtn_nombre.TabIndex = 0;
            this.rdBtn_nombre.TabStop = true;
            this.rdBtn_nombre.Text = "Nombre";
            this.rdBtn_nombre.UseVisualStyleBackColor = true;
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(697, 526);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 3;
            this.btn_salir.Text = "Salir";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCI);
            this.tabControl1.Controls.Add(this.tabRUC);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(760, 508);
            this.tabControl1.TabIndex = 10;
            // 
            // tabCI
            // 
            this.tabCI.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabCI.Controls.Add(this.groupBox2);
            this.tabCI.Controls.Add(this.btn_actualizarPerNat);
            this.tabCI.Controls.Add(this.dgv_PersNat);
            this.tabCI.Controls.Add(this.gB_filtro);
            this.tabCI.Location = new System.Drawing.Point(4, 22);
            this.tabCI.Name = "tabCI";
            this.tabCI.Padding = new System.Windows.Forms.Padding(3);
            this.tabCI.Size = new System.Drawing.Size(752, 482);
            this.tabCI.TabIndex = 0;
            this.tabCI.Text = "Personas Naturales";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_fecha);
            this.groupBox2.Controls.Add(this.txt_mail);
            this.groupBox2.Controls.Add(this.btn_eliminarPN);
            this.groupBox2.Controls.Add(this.btn_modificarPN);
            this.groupBox2.Controls.Add(this.txt_ci);
            this.groupBox2.Controls.Add(this.lbl_maq);
            this.groupBox2.Controls.Add(this.btn_guardarPN);
            this.groupBox2.Controls.Add(this.lbl_hF);
            this.groupBox2.Controls.Add(this.txt_telf);
            this.groupBox2.Controls.Add(this.lbl_hI);
            this.groupBox2.Controls.Add(this.txt_nombre);
            this.groupBox2.Controls.Add(this.lbl_ci);
            this.groupBox2.Controls.Add(this.txt_dir);
            this.groupBox2.Location = new System.Drawing.Point(12, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(725, 120);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            // 
            // lbl_fecha
            // 
            this.lbl_fecha.AutoSize = true;
            this.lbl_fecha.Location = new System.Drawing.Point(248, 79);
            this.lbl_fecha.Name = "lbl_fecha";
            this.lbl_fecha.Size = new System.Drawing.Size(35, 13);
            this.lbl_fecha.TabIndex = 46;
            this.lbl_fecha.Text = "E-mail";
            // 
            // txt_mail
            // 
            this.txt_mail.Location = new System.Drawing.Point(321, 76);
            this.txt_mail.Name = "txt_mail";
            this.txt_mail.Size = new System.Drawing.Size(100, 20);
            this.txt_mail.TabIndex = 45;
            this.txt_mail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mail_KeyPress);
            this.txt_mail.MouseLeave += new System.EventHandler(this.txt_mail_MouseLeave);
            // 
            // btn_eliminarPN
            // 
            this.btn_eliminarPN.Location = new System.Drawing.Point(601, 79);
            this.btn_eliminarPN.Name = "btn_eliminarPN";
            this.btn_eliminarPN.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminarPN.TabIndex = 44;
            this.btn_eliminarPN.Text = "ELIMINAR";
            this.btn_eliminarPN.UseVisualStyleBackColor = true;
            this.btn_eliminarPN.Click += new System.EventHandler(this.btn_eliminarPN_Click);
            // 
            // btn_modificarPN
            // 
            this.btn_modificarPN.Location = new System.Drawing.Point(601, 50);
            this.btn_modificarPN.Name = "btn_modificarPN";
            this.btn_modificarPN.Size = new System.Drawing.Size(75, 23);
            this.btn_modificarPN.TabIndex = 43;
            this.btn_modificarPN.Text = "MODIFICAR";
            this.btn_modificarPN.UseVisualStyleBackColor = true;
            this.btn_modificarPN.Click += new System.EventHandler(this.btn_modificarPN_Click);
            // 
            // txt_ci
            // 
            this.txt_ci.Location = new System.Drawing.Point(111, 27);
            this.txt_ci.Name = "txt_ci";
            this.txt_ci.Size = new System.Drawing.Size(100, 20);
            this.txt_ci.TabIndex = 31;
            this.txt_ci.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ci_KeyPress);
            this.txt_ci.Leave += new System.EventHandler(this.txt_ci_Leave);
            // 
            // lbl_maq
            // 
            this.lbl_maq.AutoSize = true;
            this.lbl_maq.Location = new System.Drawing.Point(38, 75);
            this.lbl_maq.Name = "lbl_maq";
            this.lbl_maq.Size = new System.Drawing.Size(44, 13);
            this.lbl_maq.TabIndex = 41;
            this.lbl_maq.Text = "Nombre";
            // 
            // btn_guardarPN
            // 
            this.btn_guardarPN.Location = new System.Drawing.Point(601, 19);
            this.btn_guardarPN.Name = "btn_guardarPN";
            this.btn_guardarPN.Size = new System.Drawing.Size(75, 23);
            this.btn_guardarPN.TabIndex = 30;
            this.btn_guardarPN.Text = "GUARDAR";
            this.btn_guardarPN.UseVisualStyleBackColor = true;
            this.btn_guardarPN.Click += new System.EventHandler(this.btn_guardarPN_Click);
            // 
            // lbl_hF
            // 
            this.lbl_hF.AutoSize = true;
            this.lbl_hF.Location = new System.Drawing.Point(248, 53);
            this.lbl_hF.Name = "lbl_hF";
            this.lbl_hF.Size = new System.Drawing.Size(49, 13);
            this.lbl_hF.TabIndex = 40;
            this.lbl_hF.Text = "Teléfono";
            // 
            // txt_telf
            // 
            this.txt_telf.Location = new System.Drawing.Point(321, 50);
            this.txt_telf.Name = "txt_telf";
            this.txt_telf.Size = new System.Drawing.Size(100, 20);
            this.txt_telf.TabIndex = 33;
            this.txt_telf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_telf_KeyPress);
            // 
            // lbl_hI
            // 
            this.lbl_hI.AutoSize = true;
            this.lbl_hI.Location = new System.Drawing.Point(248, 27);
            this.lbl_hI.Name = "lbl_hI";
            this.lbl_hI.Size = new System.Drawing.Size(52, 13);
            this.lbl_hI.TabIndex = 39;
            this.lbl_hI.Text = "Direccion";
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(111, 72);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(100, 20);
            this.txt_nombre.TabIndex = 35;
            this.txt_nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_nombre_KeyPress);
            // 
            // lbl_ci
            // 
            this.lbl_ci.AutoSize = true;
            this.lbl_ci.Location = new System.Drawing.Point(38, 30);
            this.lbl_ci.Name = "lbl_ci";
            this.lbl_ci.Size = new System.Drawing.Size(20, 13);
            this.lbl_ci.TabIndex = 37;
            this.lbl_ci.Text = "C.I";
            // 
            // txt_dir
            // 
            this.txt_dir.Location = new System.Drawing.Point(321, 24);
            this.txt_dir.Name = "txt_dir";
            this.txt_dir.Size = new System.Drawing.Size(100, 20);
            this.txt_dir.TabIndex = 36;
            this.txt_dir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_dir_KeyPress);
            // 
            // btn_actualizarPerNat
            // 
            this.btn_actualizarPerNat.Location = new System.Drawing.Point(662, 443);
            this.btn_actualizarPerNat.Name = "btn_actualizarPerNat";
            this.btn_actualizarPerNat.Size = new System.Drawing.Size(75, 23);
            this.btn_actualizarPerNat.TabIndex = 11;
            this.btn_actualizarPerNat.Text = "LIMPIAR";
            this.btn_actualizarPerNat.UseVisualStyleBackColor = true;
            this.btn_actualizarPerNat.Click += new System.EventHandler(this.click_btn_actualizarPN);
            // 
            // tabRUC
            // 
            this.tabRUC.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabRUC.Controls.Add(this.groupBox3);
            this.tabRUC.Controls.Add(this.btn_actualizarEmpresa);
            this.tabRUC.Controls.Add(this.groupBox1);
            this.tabRUC.Controls.Add(this.dgv_Empresa);
            this.tabRUC.Location = new System.Drawing.Point(4, 22);
            this.tabRUC.Name = "tabRUC";
            this.tabRUC.Padding = new System.Windows.Forms.Padding(3);
            this.tabRUC.Size = new System.Drawing.Size(752, 482);
            this.tabRUC.TabIndex = 1;
            this.tabRUC.Text = "Empresas";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txt_emailEmp);
            this.groupBox3.Controls.Add(this.btn_elimE);
            this.groupBox3.Controls.Add(this.btn_modE);
            this.groupBox3.Controls.Add(this.txt_ruc);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btn_guardarE);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txt_telfEmp);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txt_nombreEmp);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txt_dirEmp);
            this.groupBox3.Location = new System.Drawing.Point(12, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(725, 120);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(248, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "E-mail";
            // 
            // txt_emailEmp
            // 
            this.txt_emailEmp.Location = new System.Drawing.Point(321, 76);
            this.txt_emailEmp.Name = "txt_emailEmp";
            this.txt_emailEmp.Size = new System.Drawing.Size(100, 20);
            this.txt_emailEmp.TabIndex = 45;
            this.txt_emailEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_mail_KeyPress);
            this.txt_emailEmp.Leave += new System.EventHandler(this.txt_emailEmp_Leave);
            this.txt_emailEmp.MouseLeave += new System.EventHandler(this.txt_emailEmp_MouseLeave);
            // 
            // btn_elimE
            // 
            this.btn_elimE.Location = new System.Drawing.Point(601, 79);
            this.btn_elimE.Name = "btn_elimE";
            this.btn_elimE.Size = new System.Drawing.Size(75, 23);
            this.btn_elimE.TabIndex = 44;
            this.btn_elimE.Text = "ELIMINAR";
            this.btn_elimE.UseVisualStyleBackColor = true;
            this.btn_elimE.Click += new System.EventHandler(this.btn_elimE_Click);
            // 
            // btn_modE
            // 
            this.btn_modE.Location = new System.Drawing.Point(601, 50);
            this.btn_modE.Name = "btn_modE";
            this.btn_modE.Size = new System.Drawing.Size(75, 23);
            this.btn_modE.TabIndex = 43;
            this.btn_modE.Text = "MODIFICAR";
            this.btn_modE.UseVisualStyleBackColor = true;
            this.btn_modE.Click += new System.EventHandler(this.btn_modE_Click);
            // 
            // txt_ruc
            // 
            this.txt_ruc.Location = new System.Drawing.Point(111, 27);
            this.txt_ruc.Name = "txt_ruc";
            this.txt_ruc.Size = new System.Drawing.Size(100, 20);
            this.txt_ruc.TabIndex = 31;
            this.txt_ruc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ruc_KeyPress);
            this.txt_ruc.Leave += new System.EventHandler(this.txt_ruc_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Nombre";
            // 
            // btn_guardarE
            // 
            this.btn_guardarE.Location = new System.Drawing.Point(601, 19);
            this.btn_guardarE.Name = "btn_guardarE";
            this.btn_guardarE.Size = new System.Drawing.Size(75, 23);
            this.btn_guardarE.TabIndex = 30;
            this.btn_guardarE.Text = "GUARDAR";
            this.btn_guardarE.UseVisualStyleBackColor = true;
            this.btn_guardarE.Click += new System.EventHandler(this.btn_guardarE_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(248, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Teléfono";
            // 
            // txt_telfEmp
            // 
            this.txt_telfEmp.Location = new System.Drawing.Point(321, 50);
            this.txt_telfEmp.Name = "txt_telfEmp";
            this.txt_telfEmp.Size = new System.Drawing.Size(100, 20);
            this.txt_telfEmp.TabIndex = 33;
            this.txt_telfEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_telfEmp_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(248, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Direccion";
            // 
            // txt_nombreEmp
            // 
            this.txt_nombreEmp.Location = new System.Drawing.Point(111, 72);
            this.txt_nombreEmp.Name = "txt_nombreEmp";
            this.txt_nombreEmp.Size = new System.Drawing.Size(100, 20);
            this.txt_nombreEmp.TabIndex = 35;
            this.txt_nombreEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_nombreEmp_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "R.U.C.";
            // 
            // txt_dirEmp
            // 
            this.txt_dirEmp.Location = new System.Drawing.Point(321, 24);
            this.txt_dirEmp.Name = "txt_dirEmp";
            this.txt_dirEmp.Size = new System.Drawing.Size(100, 20);
            this.txt_dirEmp.TabIndex = 36;
            this.txt_dirEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_dirEmp_KeyPress);
            // 
            // btn_actualizarEmpresa
            // 
            this.btn_actualizarEmpresa.Location = new System.Drawing.Point(671, 443);
            this.btn_actualizarEmpresa.Name = "btn_actualizarEmpresa";
            this.btn_actualizarEmpresa.Size = new System.Drawing.Size(75, 23);
            this.btn_actualizarEmpresa.TabIndex = 12;
            this.btn_actualizarEmpresa.Text = "LIMPIAR";
            this.btn_actualizarEmpresa.UseVisualStyleBackColor = true;
            this.btn_actualizarEmpresa.Click += new System.EventHandler(this.click_btn_actualizarE);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Location = new System.Drawing.Point(6, 426);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 45);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(158, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 2;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 17);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(48, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "RUC";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(68, 17);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(62, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Nombre";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // dgv_Empresa
            // 
            this.dgv_Empresa.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_Empresa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Empresa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn1,
            this.Direccion1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgv_Empresa.Location = new System.Drawing.Point(3, 132);
            this.dgv_Empresa.Name = "dgv_Empresa";
            this.dgv_Empresa.Size = new System.Drawing.Size(746, 288);
            this.dgv_Empresa.TabIndex = 9;
            this.dgv_Empresa.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.click_dgv_emp);
            this.dgv_Empresa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ruc";
            this.dataGridViewTextBoxColumn3.HeaderText = "RUC";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 13;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "nombre";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // Direccion1
            // 
            this.Direccion1.DataPropertyName = "direccion";
            this.Direccion1.HeaderText = "Direccion";
            this.Direccion1.Name = "Direccion1";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "telefono";
            this.dataGridViewTextBoxColumn4.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "email";
            this.dataGridViewTextBoxColumn5.HeaderText = "Email";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.Width = 200;
            // 
            // Form_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_salir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLIENTE";
            this.Load += new System.EventHandler(this.Form_Cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PersNat)).EndInit();
            this.gB_filtro.ResumeLayout(false);
            this.gB_filtro.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabCI.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabRUC.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Empresa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_PersNat;
        private System.Windows.Forms.GroupBox gB_filtro;
        private System.Windows.Forms.TextBox txt_filtro;
        private System.Windows.Forms.RadioButton rdBtn_id;
        private System.Windows.Forms.RadioButton rdBtn_nombre;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCI;
        private System.Windows.Forms.TabPage tabRUC;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.DataGridView dgv_Empresa;
        private System.Windows.Forms.Button btn_actualizarPerNat;
        private System.Windows.Forms.Button btn_actualizarEmpresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direccion1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombres;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_fecha;
        private System.Windows.Forms.TextBox txt_mail;
        private System.Windows.Forms.Button btn_eliminarPN;
        private System.Windows.Forms.Button btn_modificarPN;
        private System.Windows.Forms.TextBox txt_ci;
        private System.Windows.Forms.Label lbl_maq;
        private System.Windows.Forms.Button btn_guardarPN;
        private System.Windows.Forms.Label lbl_hF;
        private System.Windows.Forms.TextBox txt_telf;
        private System.Windows.Forms.Label lbl_hI;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.Label lbl_ci;
        private System.Windows.Forms.TextBox txt_dir;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_emailEmp;
        private System.Windows.Forms.Button btn_elimE;
        private System.Windows.Forms.Button btn_modE;
        private System.Windows.Forms.TextBox txt_ruc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_guardarE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_telfEmp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_nombreEmp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_dirEmp;

    }
}