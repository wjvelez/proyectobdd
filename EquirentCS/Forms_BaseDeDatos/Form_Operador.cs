﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_Operador : Form
    {
        Operador op;
        Validaciones valida = new Validaciones();
        DialogResult confirmacion;

        public Form_Operador()
        {
            InitializeComponent();
        }

        /*Metodo para cargar los valores en el FORM_OPERADOR*/
        public void cargar()
        {
            if (op.leerOperador())
            {
                dgv_operador.DataSource = op.d_tb;
            }
            else
            {
                MessageBox.Show("Error");
            }
        } 
        

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        //private void btn_limpiar_Click(object sender, EventArgs e)
        //{
        //    txt_nombres.Clear();
        //    txt_apellidos.Clear();
        //    txt_id.Clear();
        //    txt_telefono.Clear();
        //    txt_direccion.Clear();
        //}

        //private void btn_agregar_Click(object sender, EventArgs e)
        //{
        //    String mod = "Modificar";
        //    String elim = "Eliminar";

        //    String nombre = txt_nombres.Text.ToString();
        //    String apellido = txt_apellidos.Text.ToString();
        //    String id = txt_id.Text.ToString();
        //    String telf = txt_telefono.Text.ToString();
        //    String direccion = txt_direccion.Text.ToString();

        //    dgv_operador.Rows.Add(id, nombre, apellido, direccion, telf, mod, elim);

        //    btn_limpiar_Click(sender, e);
        //}

       

        
        private void Form_Operador_Load(object sender, EventArgs e)
        {

            op = new Operador();
            cargar();
            
        }


        private void btn_guardar_Click(object sender, EventArgs e)
        {
            confirmacion = MessageBox.Show("Seguro que desea GUARDAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                //Crear Operador con lo valores a guardar
                op = new Operador(txt_cedula.Text, txt_nombre.Text, txt_apellido.Text, txt_direccion.Text, txt_telefono.Text);

                //Lo va guardar en la base de datos
                op.agregarOperador();

                //refresca la tabla
                cargar();

            }
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
           confirmacion = MessageBox.Show("Seguro que desea MODIFICAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

           if (confirmacion == DialogResult.Yes)
           {
               op = new Operador(txt_cedula.Text, txt_nombre.Text, txt_apellido.Text, txt_direccion.Text, txt_telefono.Text);

               //Lo va guardar en la base de datos
               op.modificarOperador();
               //refresca la tabla
               cargar();
           }
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {

            confirmacion = MessageBox.Show("Seguro que desea ELIMINAR", "CONFIRMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmacion == DialogResult.Yes)
            {
                op = new Operador(txt_cedula.Text, txt_nombre.Text, txt_apellido.Text, txt_direccion.Text, txt_telefono.Text);
                //Lo va guardar en la base de datos
                op.eliminarOperador();

                //refresca la tabla
                cargar();
            }
        }

        private void click_autollenado(object sender, DataGridViewCellEventArgs e)
        {
            txt_cedula.Text = dgv_operador.CurrentRow.Cells[0].Value.ToString();
            txt_nombre.Text = dgv_operador.CurrentRow.Cells[1].Value.ToString();
            txt_apellido.Text = dgv_operador.CurrentRow.Cells[2].Value.ToString();
            txt_direccion.Text = dgv_operador.CurrentRow.Cells[3].Value.ToString();
            txt_telefono.Text = dgv_operador.CurrentRow.Cells[4].Value.ToString();

        }

        private void click_btn_actualizar(object sender, EventArgs e)
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            txt_apellido.Clear();
            txt_direccion.Clear();
            txt_telefono.Clear();
        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_nombre.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

            }
        }

        private void txt_apellido_KeyPress(object sender, KeyPressEventArgs e)
        {

            txt_apellido.MaxLength = 50;
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

            }
        }

        private void txt_direccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_direccion.MaxLength = 255;
            valida.letra_num(e);
        }

        private void txt_telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            txt_telefono.MaxLength = 10;
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_cedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            string num = "" + c;


                txt_cedula.MaxLength = 10;
 

            /*Valida que los datos ingresados solo sean  numeros*/
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;

            }
        }

        private void txt_cedula_Leave(object sender, EventArgs e)
        {
            if (txt_cedula.Text != "" )
            {

                    if (!valida.num_10(txt_cedula.Text))
                    {

                        MessageBox.Show("C.I DEBE TENER 10 DIGITOS");
                        txt_cedula.Clear();
                    }
                
            }
        }






        //private void txt_nombres_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!valida.sonletras("" + e.KeyChar))
        //    {
        //        e.Handled = true;
        //        if (char.IsSeparator(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }
        //        if (char.IsControl(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }

        //        if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //        {
        //            txt_apellidos.Focus();
        //        }
        //    }
        //}

        //private void txt_apellidos_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!valida.sonletras("" + e.KeyChar))
        //    {
        //        e.Handled = true;
        //        if (char.IsSeparator(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }
        //        if (char.IsControl(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }

        //        if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //        {
        //            txt_direccion.Focus();
        //        }

        //    }
        //}

        //private void txt_direccion_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    valida.letra_num(e);
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
        //    {
        //        txt_id.Focus();
        //    }
        //}

        //private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    char c = e.KeyChar;
        //    string num = "" + c;

        //    txt_id.MaxLength = 10;

        //    /*Valida que los datos ingresados solo sean  numeros*/
        //    if (!valida.sonNumeros(num))
        //    {
        //        e.Handled = true;
        //        /*valida para que se pueda borrar mientras se ingresa la informacion*/
        //        if (char.IsControl(e.KeyChar))
        //            e.Handled = false;

        //        if (e.KeyChar == Convert.ToChar(Keys.Enter))
        //            txt_telefono.Focus();

        //    }
        //}

        //private void txt_id_Leave(object sender, EventArgs e)
        //{
        //    if (txt_id.Text != "")
        //    {

        //        if (!valida.num_10(txt_id.Text))
        //        {

        //            MessageBox.Show("C.I DEBE TENER 10 DIGITOS");
        //            txt_id.Clear();
        //        }
        //    }
        //}

        //private void txt_telefono_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!valida.sonNumeros("" + e.KeyChar))
        //    {
        //        e.Handled = true;
        //        if (char.IsControl(e.KeyChar))
        //        {
        //            e.Handled = false;
        //        }
        //    }
        //}

        //private void txt_telefono_TextChanged(object sender, EventArgs e)
        //{

        //}

    }
}
