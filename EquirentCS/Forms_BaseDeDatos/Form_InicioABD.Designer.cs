﻿namespace EquirentCS.Forms_BaseDeDatos
{
    partial class Form_InicioABD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCliente = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnMaquinaria = new System.Windows.Forms.Button();
            this.btnProveedor = new System.Windows.Forms.Button();
            this.btnOperador = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCliente
            // 
            this.btnCliente.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnCliente.Location = new System.Drawing.Point(50, 35);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(175, 75);
            this.btnCliente.TabIndex = 0;
            this.btnCliente.Text = "CLIENTE";
            this.btnCliente.UseVisualStyleBackColor = false;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.Color.Black;
            this.btnSalir.Location = new System.Drawing.Point(350, 244);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(100, 30);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnMaquinaria
            // 
            this.btnMaquinaria.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnMaquinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaquinaria.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnMaquinaria.Location = new System.Drawing.Point(275, 35);
            this.btnMaquinaria.Name = "btnMaquinaria";
            this.btnMaquinaria.Size = new System.Drawing.Size(175, 75);
            this.btnMaquinaria.TabIndex = 2;
            this.btnMaquinaria.Text = "MAQUINARIA";
            this.btnMaquinaria.UseVisualStyleBackColor = false;
            this.btnMaquinaria.Click += new System.EventHandler(this.btnMaquinaria_Click);
            // 
            // btnProveedor
            // 
            this.btnProveedor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProveedor.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnProveedor.Location = new System.Drawing.Point(50, 129);
            this.btnProveedor.Name = "btnProveedor";
            this.btnProveedor.Size = new System.Drawing.Size(175, 75);
            this.btnProveedor.TabIndex = 1;
            this.btnProveedor.Text = "PROVEEDOR";
            this.btnProveedor.UseVisualStyleBackColor = false;
            this.btnProveedor.Click += new System.EventHandler(this.btnProveedor_Click);
            // 
            // btnOperador
            // 
            this.btnOperador.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnOperador.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOperador.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnOperador.Location = new System.Drawing.Point(275, 129);
            this.btnOperador.Name = "btnOperador";
            this.btnOperador.Size = new System.Drawing.Size(175, 75);
            this.btnOperador.TabIndex = 3;
            this.btnOperador.Text = "OPERADOR";
            this.btnOperador.UseVisualStyleBackColor = false;
            this.btnOperador.Click += new System.EventHandler(this.btnOperador_Click);
            // 
            // Form_InicioABD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(509, 286);
            this.Controls.Add(this.btnProveedor);
            this.Controls.Add(this.btnCliente);
            this.Controls.Add(this.btnMaquinaria);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnOperador);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_InicioABD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Base de datos";
            this.Load += new System.EventHandler(this.Form_InicioABD_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnMaquinaria;
        private System.Windows.Forms.Button btnProveedor;
        private System.Windows.Forms.Button btnOperador;
    }
}