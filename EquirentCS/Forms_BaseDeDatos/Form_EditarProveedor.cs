﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EquirentCS.Forms_BaseDeDatos
{
    public partial class Form_EditarProveedor : Form
    {
        Validaciones valida = new Validaciones();
        
        public void Form_EditarOperador()
        {
            InitializeComponent();
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void Form_EditarCliente_Load(object sender, EventArgs e)
        {

        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonletras("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsSeparator(e.KeyChar))
                {
                    e.Handled = false;
                }
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

                if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
                {
                    txt_direccion.Focus();
                }
            }
        }

        private void txt_direccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            valida.letra_num(e);
            if (e.KeyChar == Convert.ToChar(Keys.Enter))//Keys Espicifica los modificadores y codigos de tecla
            {
                txt_id.Focus();
            }
        }

        private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            string num = "" + c;
            if (!valida.sonNumeros(num))
            {
                e.Handled = true;
                /*valida para que se pueda borrar mientras se ingresa la informacion*/
                if (char.IsControl(e.KeyChar))
                    e.Handled = false;

                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                    txt_telefono.Focus();

            }
        }

        private void txt_id_Leave(object sender, EventArgs e)
        {
            if (txt_id.Text != "" & btn_eliminar.ContainsFocus == false)
            {

                if (!valida.num_13(txt_id.Text))
                {
                    MessageBox.Show("RUC DEBE TENER 13 DIGITOS");
                    txt_id.Clear();
                }
            }
        }

        private void txt_telefono_Leave(object sender, EventArgs e)
        {

        }

        private void txt_telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!valida.sonNumeros("" + e.KeyChar))
            {
                e.Handled = true;
                if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
            }
        }

        private void txt_id_MouseLeave(object sender, EventArgs e)
        {

        }
    }
}
