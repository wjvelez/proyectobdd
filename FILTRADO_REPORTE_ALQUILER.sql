
/*****************************************************/
/*       FILTRADO PARA REPORTES DE ALQUILER          */
/*****************************************************/
use proyecto_bd;


-- BUSQUEDA POR NOMNRE
drop procedure if exists FILTRO_REPALQ_CP;
DELIMITER //
create procedure FILTRO_REPALQ_CP(in nombre varchar(50))
begin
	select * 
    from (select  A.codigoAlquiler,A.fecha_inicio,A.fecha_fin,AM.placa,CN.nombre
	from alquiler A 
	inner join alquilermaquinas AM on A.codigoAlquiler=AM.codigoAlquiler
	inner join clientnatural CN on CN.cedula = A.cedula
    ) Alq
    where Alq.nombre like(concat(nombre,'%'));
end //
DELIMITER ;



drop procedure if exists FILTRO_REPALQ_CE;
DELIMITER //
create procedure FILTRO_REPALQ_CE(in nombre varchar(50))
begin
	select * 
    from (select  A.codigoAlquiler,A.fecha_inicio,A.fecha_fin,AM.placa,CE.nombre
	from alquiler A 
	inner join alquilermaquinas AM on A.codigoAlquiler=AM.codigoAlquiler
	inner join clientempresa CE on CE.ruc = A.ruc
    ) Alqui
    where Alqui.nombre like(concat(nombre,'%'));
end //
DELIMITER ;

call FILTRO_REPALQ_CE('T');

-- BUSUEDA POR CODIGO

drop procedure if exists FILTRO_REPALQ_COD_CN;
DELIMITER //
Create procedure  FILTRO_REPALQ_COD_CN(in in_codigo int(11))
begin
select * 
    from (select  A.codigoAlquiler,A.fecha_inicio,A.fecha_fin,AM.placa,CN.nombre
	from alquiler A 
	inner join alquilermaquinas AM on A.codigoAlquiler=AM.codigoAlquiler
	inner join clientnatural CN on CN.cedula = A.cedula
    ) Alq
    where Alq.codigoAlquiler like (concat(in_codigo,'%'));
end //
DELIMITER ;

call FILTRO_REPALQ_COD_CN(3);

drop procedure if exists FILTRO_REPALQ_COD_CE;
DELIMITER //
Create procedure  FILTRO_REPALQ_COD_CE(in in_codigo int(11))
begin
select * 
    from (select  A.codigoAlquiler,A.fecha_inicio,A.fecha_fin,AM.placa,CE.nombre
	from alquiler A 
	inner join alquilermaquinas AM on A.codigoAlquiler=AM.codigoAlquiler
	inner join clientempresa CE on CE.ruc = A.ruc
    ) Alq
    where Alq.codigoAlquiler like (concat(in_codigo,'%'));
end //
DELIMITER ;

call FILTRO_REPALQ_COD_CE(1);

call GET_FACTURA;


-- BUSQUEDA POR MAQUINA



