create database proyecto_BD;

use proyecto_BD;

-- drop table if exists operador ;
create table if not exists operador(
cedula varchar(10) not null,
nombre varchar(50) not null,
apellido varchar(50) not null,
direccion varchar(255) not null,
telefono varchar(10) not null,
PRIMARY KEY(cedula)
);

insert into operador values ('0927651400','Julio Patricio','Granda Vera','martha de roldos','087327382');
insert into operador values ('0927651401','Arturo','Gómez Bonilla','alborada 4ta etapa, mz R32','042323256');
insert into operador values ('0927651402','Carlos Luis','Paez Pazmiño','Miraflores','04275689');
insert into operador values ('1307651403','Carlos Manuel','Cevallos Muñoz','Samanes 3','0429839');
insert into operador values ('1307651404','Ronald Fabricio','Moreno Moreno','Alborada 6ta etapa','0429584');
insert into operador values ('1305671323','José David','Franco Caicedo','martha de roldos','0429786');
insert into operador values ('1304321404','Luis Andres','Moreno Moreno','Alborada 6ta etapa','0429584');
insert into operador values ('1301291323','Andres Arturo','Vera Caicedo','samanes 6','0429706');
insert into operador values ('0941294323','Andy','Aparicio Gonzalez','samanes 6','0428786');


-- drop table if exists maquina;
create table if not exists maquina(
placa varchar(10) not null,
marca varchar(30) not null,
tipo varchar(30) not null,
modelo varchar(30) not null,
PRIMARY KEY(placa)
);


insert into maquina values('GTH-123','KOMATSU','Excavadora','PC200LC-8');
insert into maquina values('GTQ-105','KOMATSU','Excavadora','PC300LC-8');
insert into maquina values('GBI-675','KOMATSU','Excavadora','PC400LC-8');
insert into maquina values('GOE-150','KOMATSU','Excavadora','PC228USL-3EO');
insert into maquina values('YRB-843','BOMAG','Rodillo','BW211D-40');
insert into maquina values('YRW-130','BOMAG','Rodillo','BW211D-50');
insert into maquina values('REY-673','KOMATSU','RetroExcavadora', 'WB146-5');
insert into maquina values('ROY-400','KOMATSU','RetroExcavadora', 'WB146-5');
insert into maquina values('RAY-873','KOMATSU','RetroExcavadora', 'WB146-6');
insert into maquina values('GAW-843','BOMAG','Bulldozer','D65EX-16');
insert into maquina values('GPW-043','BOMAG','Bulldozer','D65EX-16');
insert into maquina values('RQZ-987', 'HINO', 'Volqueta', 'FM2PLSD');
insert into maquina values('RUJ-345', 'HINO', 'Volqueta', 'FM2PLSD');
insert into maquina values('RML-209', 'HINO', 'Volqueta', 'FM2PLSD');


-- drop table if exists proveedor;
create table if not exists proveedor(
ruc varchar(13) not null,
nombre varchar(50) not null,
direccion varchar(255) not null,
telefono varchar(10) not null,
PRIMARY KEY(ruc)
);

insert into proveedor values('1234567801','Vulcanizadora EL RECREO','Alborada','232444444');
insert into proveedor values('0943434545','DITECA S.A.','Sauces','2324766764');
insert into proveedor values('1275454567','MULTIREPUESTOS','Samanes','23234344');
insert into proveedor values('0912451923','TALLER AURORA','Francisco O','23454544');
insert into proveedor values('0956879862','Vulcanizadora LA RIVERA','Martha','233223444');

-- drop table if exists clientEmpresa;
create table if not exists clientEmpresa(
ruc varchar(13) ,
nombre varchar(50) not null,
direccion varchar(255) not null,
telefono varchar(10) not null,
email varchar(255),
PRIMARY KEY(ruc)
);

insert into clientEmpresa values('1234567890987','TREC S.A','6Marzo y J','042837475','trecsa@hotmail.com');
insert into clientEmpresa values('0923823723878','MUNICIPO GYE','V.M.Estrada','42434343','munigye@hotmail.com');
insert into clientEmpresa values('0997784773883','PROMOTORA LFG','F.Orellana','67554334','promo@hotmail.com');
insert into clientEmpresa values('0964552477398','HOLCIN','Chembers','6576754','povr@hotmail.com');
insert into clientEmpresa values('0932183272982','COLE S.A','Juan Montalvo','2433565674','nkln@hotmail.com');


-- drop table if exists clientnatural;
create table if not exists clientNatural(
cedula varchar(10) ,
nombre varchar(50) not null,
direccion varchar(255) not null,
telefono varchar(10) not null,
email varchar(255),
PRIMARY KEY(cedula)
);

insert into clientNatural values ('0973664582','Carlos Santana Mejía','protete','042453676','tutu@hotmail.com');
insert into clientNatural values ('0998779699','Juan Andres Mosquera Yepez','J.T.C','04275864','yut@hotmail.com');
insert into clientNatural values ('0976565879','Fernando Suarez Vera','38 y ayacucho','04266776','hut@hotmail.com');
insert into clientNatural values ('0256877998','Julio Ortega Medranda','J mascote y Colon','04288767','pop@hotmail.com');
insert into clientNatural values ('0132435454','Mario Andres Vasquez Loor','quito y colon','042334544','base@hotmail.com');


-- drop table if exists planillaje;
create table if not exists planillaje(
cedula varchar(10) not null,
placa varchar(10) not null,
codigoAlquiler int not null,
horas int(4) not null,
fecha date not null,
primary key (codigoAlquiler,placa,fecha),
foreign key(placa) references maquina(placa),
foreign key(cedula) references operador(cedula)
);

insert into planillaje values ('0927651402','GTH-123',1,5,'2015-08-12');
insert into planillaje values ('0927651402','GAW-843',1,5,'2015-08-12');
insert into planillaje values ('0927651402','GTH-123',1,6,'2015-08-13');
insert into planillaje values ('0941294323','GOE-150',3,4,'2015-08-15');
insert into planillaje values ('1307651403','RUJ-345',4,8,'2015-08-15');


-- drop table if exists registroproveedores;
create table if not exists registroProveedores(
codigo_factura varchar(10) not null,
ruc varchar(13) not null,
placa varchar(10) not null,
detalle varchar(255) not null,
valor float(10) not null,
tipo varchar(50) not null,
fecha date not null,
primary key(codigo_factura, ruc),
foreign key(ruc) references proveedor(ruc),
foreign key(placa) references maquina(placa)
);

insert into registroproveedores values ('132600','1234567801','RAY-873', 'cambio de filtro', 42.50, 'mantenimiento', '2015-07-13');
insert into registroproveedores values ('202456','0943434545','GBI-675', 'cambio de llantas', 140.75, 'mantenimiento', '2015-07-13');
insert into registroproveedores values ('132899','0956879862','RQZ-987', 'mantenimiento general', 500, 'mantenimiento', '2015-08-13');


-- drop table if exists alquiler;
create table if not exists alquiler(
codigoAlquiler int not null auto_increment,
ruc varchar(13) ,
cedula varchar(10),
fecha_inicio date not null,
costoHora float not null,
fecha_fin date default null,
primary key(codigoAlquiler),
foreign key(ruc) references clientEmpresa(ruc),
foreign key(cedula) references clientNatural(cedula)
);

insert into alquiler (ruc,cedula,fecha_inicio,costoHora)
  values ('1234567890987',null,'2015-08-08',2);
insert into alquiler (ruc,cedula,fecha_inicio,costoHora)
  values (null, '0132435454' ,'2015-08-07',2);
insert into alquiler (ruc,cedula,fecha_inicio,costoHora)
  values (null, '0256877998' ,'2015-08-10',4);
  

 
 -- drop table if exists  AlquilerMaquinas;
create table if not exists AlquilerMaquinas(
codigoAlquiler int not null,
placa varchar(10) not null,
devuelto bool default false not null,
f_ult_pago date,
primary key(codigoAlquiler,placa),
foreign key(codigoAlquiler) references alquiler(codigoAlquiler),
foreign key(placa) references maquina(placa)
);

insert into AlquilerMaquinas (codigoAlquiler,placa, f_ult_pago)
  values (1, 'GTH-123', null);
insert into AlquilerMaquinas (codigoAlquiler,placa, f_ult_pago)
  values (1, 'GAW-843', null);
insert into AlquilerMaquinas (codigoAlquiler,placa, f_ult_pago)
  values (3, 'GOE-150', null);


  
  
-- drop table if exists factura;
create table if not exists factura(
codigoFactura int not null auto_increment,
codigoAlquiler int not null,
descripcion varchar(512) not null,
valor float(8,2) not null,
iva float(8,2) not null,
descuento float(8,2),
fecha date,
usuario varchar(20) not null,
primary key(codigoFactura),
foreign key(codigoAlquiler) references alquiler(codigoAlquiler)
);

insert into factura (codigoAlquiler, descripcion, valor, iva, descuento, fecha, usuario)
values (1, 'cualquier cosa', 170.45, 1.12, 0.10, '2015-08-30', 'usuario1');
insert into factura (codigoAlquiler, descripcion, valor, iva, descuento, fecha, usuario)
values(2, 'alquiler dos', 180.50, 1.20, 0.15, '2015-08-20', 'usuario1');
insert into factura (codigoAlquiler, descripcion, valor, iva, descuento, fecha, usuario)
values(1, 'cualquier cosa parte2', 170.45, 1.12, 0.10, '2015-08-30', 'usuario1');










