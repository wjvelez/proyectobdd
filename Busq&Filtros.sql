/* *******************BUSQUEDAS Y FILTROS***************** */


drop procedure if exists  SEARCH_CLIENTENATURAL;
DELIMITER  //
create procedure SEARCH_CLIENTENATURAL(in cedula varchar(10))
begin
	select * from clientnatural where clientnatural.cedula=cedula;
end //
DELIMITER ;

/*FILTROS*/


drop procedure if exists FILTRO_CLIENTENAT;
DELIMITER //
create procedure FILTRO_CLIENTENAT(in cedula varchar(10) )
begin
 -- set @cedula1 = concat(cedula,'%');
  select * from clientnatural where clientnatural.cedula like (concat(cedula,'%'));
end //
DELIMITER ;


drop procedure if exists FILTRO_CLIENTE_EMP;
DELIMITER //
create procedure FILTRO_CLIENTE_EMP(in ruc varchar(13) )
begin
 -- set @cedula1 = concat(cedula,'%');
  select * from clientempresa where clientempresa.ruc like (concat(ruc,'%'));
end //
DELIMITER ;


-- Procedure que filtra las maquinas por el tipo y ademas solo muestra las disponibles
drop procedure if exists FILTRO_MAQUINA_TIPO;
DELIMITER //
create procedure FILTRO_MAQUINA_TIPO(in tipoV varchar(30))
begin
	-- select placa , tipo from maquina  where maquina.tipo like(concat(tipoV,'%'));    
	select maquina.placa, maquina.tipo from maquina where 
    (maquina.placa NOT IN (select placa from alquilermaquinas where alquilermaquinas.devuelto=false)
    and maquina.tipo like(concat(tipoV,'%'))
    );
END //
DELIMITER ;
-- call FILTRO_MAQUINA_TIPO('B');

-- select * from clientnatural;
-- call FILTRO_CLIENTENAT( '09');

-- nuevos procedures*********************

/* ******************************************************************* */
/* Procedures para el filtrado de facturas */
/* ******************************************************************* */


/* PARA REPORTES ******************** */

-- procedure que filtra por nombre de empresas las facturas emitidas
drop procedure if exists FILTRO_REPFACT_CE;
DELIMITER //
create procedure FILTRO_REPFACT_CE(in nombre varchar(50))
begin
	select *
	from
		(select F.codigoFactura, F.fecha, F.codigoAlquiler, A.ruc, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.nombre like(concat(nombre,'%'));
end //
DELIMITER ;

call FILTRO_REPFACT_CE('TREC S.A');

-- procedure que filtra por nombre de persona natural las facturas emitidas
drop procedure if exists FILTRO_REPFACT_CP;
DELIMITER //
create procedure FILTRO_REPFACT_CP(in nombre varchar(50))
begin
	select *
	from
		(select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cN.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientNatural cN on A.cedula = cN.cedula) Fact
	where Fact.nombre like(concat(nombre,'%'));
end //
DELIMITER ;

call FILTRO_REPFACT_CP('mario');


-- procedure para filtrar por detalle de factura de cliente natural
drop procedure if exists FILTRO_REPFACT_DET_CP;
DELIMITER //
create procedure FILTRO_REPFACT_DET_CP(in in_detalle varchar(512))
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cN.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientNatural cN on A.cedula = cN.cedula) Fact
	where Fact.descripcion like(concat(in_detalle,'%'));
		
end //
DELIMITER ;

call FILTRO_REPFACT_DET_CP('a');


-- procedure para filtrar por detalle de factura de cliente empresaa
drop procedure if exists FILTRO_REPFACT_DET_CE;
DELIMITER //
create procedure FILTRO_REPFACT_DET_CE(in in_detalle varchar(512))
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.ruc, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.descripcion like(concat(in_detalle,'%'));		
end //
DELIMITER ;


-- procedure para filtrar por alquiler de factura de cliente natural
drop procedure if exists FILTRO_REPFACT_ALQ_CP;
DELIMITER //
create procedure FILTRO_REPFACT_ALQ_CP(in in_alq int)
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cN.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientNatural cN on A.cedula = cN.cedula) Fact
	where Fact.codigoAlquiler like(concat(in_alq,'%'));
		
end //
DELIMITER ;



-- procedure para filtrar por alquiler de factura de cliente empresa
drop procedure if exists FILTRO_REPFACT_ALQ_CE;
DELIMITER //
create procedure FILTRO_REPFACT_ALQ_CE(in in_alq int)
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.ruc, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.codigoAlquiler like(concat(in_alq,'%'));
		
end //
DELIMITER ;


-- procedure para filtrar por valor de factura de cliente natural
drop procedure if exists FILTRO_REPFACT_VAL_CP;
DELIMITER //
create procedure FILTRO_REPFACT_VAL_CP(in in_valor float)
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cN.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientNatural cN on A.cedula = cN.cedula) Fact
	where Fact.valor like(concat(in_valor,'%'));
		
end //
DELIMITER ;


-- procedure para filtrar por valor de factura de cliente empresaa
drop procedure if exists FILTRO_REPFACT_VAL_CE;
DELIMITER //
create procedure FILTRO_REPFACT_VAL_CE(in in_valor int)
begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.ruc, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join factura F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.valor like(concat(in_valor,'%'));
		
end //
DELIMITER ;


/* ******************************************************************* */
/* Procedures para el filtrado de facturas  por fechas -- estos son pruebas .....*/
/* ******************************************************************* */
/*
 drop procedure if exists FILTRO_FECHAS;
 DELIMITER //
 create procedure FILTRO_FECHAS(in inicio varchar(20), in fin varchar(20))
 begin
	select * from factura F where F.fecha<= fin and F.fecha>=inicio;
 end //
 DELIMITER ;
 
 
 drop procedure if exists FFFF;
 DELIMITER //
 create procedure FFFF(in in_nombre varchar(50), in inicio varchar(20), in fin varchar(20))
 begin
	select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join (select * from factura Fac where Fac.fecha<=fin and Fac.fecha>=inicio) F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.nombre like(concat(in_nombre,'%'));
 end //
 DELIMITER ;

select * from factura;

call FILTRO_FECHAS('2015-08-02', '2015-08-29');

call FFFF('TREC S.A', '2015-08-02', '2015-08-29');

select * from factura F where F.fecha<= '2015-08-29' and F.fecha>='2015-08-02';
select * 
	from (select F.codigoFactura, F.fecha, F.codigoAlquiler, A.cedula, cE.nombre, F.descripcion, F.valor
		from alquiler A
		inner join (select * from factura F where F.fecha<= '2015-08-29' and F.fecha>='2015-08-02') F on A.codigoAlquiler = F.codigoAlquiler
		inner join clientEmpresa cE on A.ruc = cE.ruc) Fact
	where Fact.nombre like(concat('TREC S.A','%'));
*/


/* ******************************************************************* */
/* Procedures para el filtrado de planillajes */
/* ******************************************************************* */


drop procedure if exists FILTRO_REPPLA_OP;
DELIMITER //
create procedure FILTRO_REPPLA_OP(in in_ced varchar(10))
begin
	select * from planillaje P where P.cedula like (concat(in_ced, '%'));
end //
DELIMITER ;


drop procedure if exists FILTRO_REPPLA_OP;
DELIMITER //
create procedure FILTRO_REPPLA_OP(in in_ced varchar(10))
begin
	select * from planillaje P where P.cedula like (concat(in_ced, '%'));
end //
DELIMITER ;

drop procedure if exists FILTRO_REPPLA_MAQ;
DELIMITER //
create procedure FILTRO_REPPLA_MAQ(in in_maq varchar(10))
begin
	select * from planillaje P where P.placa like (concat(in_maq, '%'));
end //
DELIMITER ;
drop procedure if exists FILTRO_REPPLA_ALQ;
DELIMITER //
create procedure FILTRO_REPPLA_ALQ(in in_alq int(11))
begin
	select * from planillaje P where P.codigoAlquiler like (concat(in_alq, '%'));
end //
DELIMITER ;

drop procedure if exists FILTRO_REPPLA_FEC;
DELIMITER //
create procedure FILTRO_REPPLA_FEC(in in_fecha date)
begin
	select * from planillaje P where P.fecha like (concat(in_fecha, '%'));
end //
DELIMITER ;

