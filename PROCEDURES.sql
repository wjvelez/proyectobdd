
-- ****************TODOS LOS GET******************

-- OBTIENE todos los valores correspondientes a la TABLA OPERADOR --
drop procedure if exists GET_OPERADOR;
DELIMITER //
create procedure GET_OPERADOR()
BEGIN 
	select * from operador;
END //
DELIMITER ;

-- OBTIENE todos los valores correspondientes a la TABLA MAQUINA --
drop procedure if exists GET_MAQUINA;
DELIMITER //
create procedure GET_MAQUINA()
BEGIN
	select * from maquina;
END //
DELIMITER ;
-- call GET_MAQUINA();

drop procedure if exists GET_MAQUINASDISPONIBLES;
DELIMITER //
create procedure GET_MAQUINASDISPONIBLES()
BEGIN 
select maquina.placa, maquina.tipo from maquina where maquina.placa 
NOT IN (select placa from alquilermaquinas where alquilermaquinas.devuelto=false);
END //
DELIMITER ;

call GET_MAQUINASDISPONIBLES();


-- OBTIENE todos los valores correspondientes a la TABLA PROVEEDOR --
drop procedure if exists GET_PROVEEDOR;
DELIMITER //
create procedure GET_PROVEEDOR()
BEGIN
	select * from proveedor;
END //
DELIMITER ;

use proyecto_bd;
-- Obtiene los regsitros de los pagos de proveedores
drop procedure if exists GET_PAGPROVEED;
DELIMITER //
create procedure GET_PAGPROVEED()
begin
	select * from registroproveedores;
end //
DELIMITER ;


-- OBTIENE todos los valores correspondientes a la TABLA CLIENTEMPRESA --
drop procedure if exists GET_clientEMPRESA;
DELIMITER //
create procedure GET_clientEMPRESA()
BEGIN
	select * from clientEmpresa;
END //
DELIMITER ;

-- OBTIENE todos los valores correspondientes a la TABLA CLIENTNATURAL --
drop procedure if exists  GET_clientNATURAL;
DELIMITER //
create procedure GET_clientNATURAL()
BEGIN 
	select * from clientNatural;
END //
DELIMITER ;

-- obtiene todos los registros de la tabla planillaje
drop procedure if exists GET_PLANILLAJE;
DELIMITER //
create procedure GET_PLANILLAJE()
BEGIN 
	select * from planillaje;
END //
DELIMITER ;
call GET_PLANILLAJE;


-- UTIL AL MOMENTO DE ALQUILAR YA QUE SELECCIONA EL ULTIMO CODIGO ASIGNADO
drop procedure if exists GET_CODIGO_ALQUILER_LAST;
DELIMITER //
create procedure GET_CODIGO_ALQUILER_LAST()
BEGIN
	-- select codigoAlquiler  from alquiler order by codigoAlquiler DESC LIMIT 1;
    select max(codigoAlquiler) as codigo from alquiler;
END //
DELIMITER ;

 -- select * from alquiler;
 -- call    GET_CODIGO_ALQUILER_LAST();


-- obtiene todos los registros de la tabla factura
drop procedure if exists GET_FACTURA;
DELIMITER //
create procedure GET_FACTURA()
begin
select * from factura;
end //
DELIMITER ;
call GET_FACTURA; 

-- obtiene todos los registros de la tabla alquilermaquinas
drop procedure if exists GET_ALQMAQ;
DELIMITER //
create procedure GET_ALQMAQ()
begin
select * from alquilermaquinas;
end //
DELIMITER ;
call GET_ALQMAQ; 

-- obtiene todos los registros de la tabla alquiler
drop procedure if exists GET_ALQUILER;
DELIMITER //
create procedure GET_ALQUILER()
begin
select * from alquiler;
end // 
DELIMITER ;
call GET_ALQUILER;



-- ****************TODOS LOS SET******************

-- INSERTAR registros en tabla MAQUINA
drop procedure if exists SET_MAQUINA;
DELIMITER $
create procedure SET_MAQUINA(
in placa varchar(10), 
in marca varchar(30), 
in tipo varchar(30),	
in modelo varchar(30))
begin
	INSERT INTO maquina VALUES(placa, marca, tipo, modelo);
END $ DELIMITER ;


-- INSERTAR registros en tabla CLIENTNATURAL
drop procedure if exists SET_CLIENTENATURAL;
DELIMITER $
create procedure SET_CLIENTENATURAL(
in cedula varchar(10), 
in nombre varchar(50),
in direccion varchar(255), 
in telefono varchar(10),
in email varchar(255))
begin
	INSERT INTO clientnatural VALUES(cedula, nombre, direccion, telefono, email);
END $ DELIMITER ;


-- INSERTAR registros en tabla CLIENTEMPRESA
drop procedure if exists SET_CLIENTEEMPRESA;
DELIMITER $
create procedure SET_CLIENTEEMPRESA(
in ruc varchar(13), 
in nombre varchar(50),
in direccion varchar(255), 
in telefono varchar(10),
in email varchar(255))
begin
	INSERT INTO clientempresa VALUES(ruc, nombre, direccion, telefono, email);
END $ DELIMITER ;


-- INSERTAR registros en tabla OPERADOR
drop procedure if exists SET_OPERADOR;
DELIMITER $
create procedure SET_OPERADOR(
in cedula varchar(10), 
in nombre varchar(50),
in apellido varchar(50), 
in direccion varchar(255),
in telefono varchar(10))
begin
	INSERT INTO operador VALUES(cedula, nombre, apellido, direccion, telefono);
END $ DELIMITER ;


-- INSERTAR registros en tabla PROVEEDOR
drop procedure if exists SET_PROVEEDOR;
DELIMITER $
create procedure SET_PROVEEDOR(
in ruc varchar(13), 
in nombre varchar(50), 
in direccion varchar(255),
in telefono varchar(10))
begin
	INSERT INTO proveedor VALUES(ruc, nombre, direccion, telefono);
END $ DELIMITER ;

-- inserta un registro en la tabla registroproveedores
drop procedure if exists SET_PAGPROVEED;
DELIMITER //
create procedure SET_PAGPROVEED(
	in fact varchar(10), in in_ruc varchar(13), in in_placa varchar(10),
    in det varchar(255), in val float(10), in in_tipo varchar(50), in fech date)
begin
insert into registroproveedores values(
	fact, in_ruc, in_placa, det, val, in_tipo, fech);
end //
DELIMITER ;


-- inserta registro en la tabla planillaje
drop procedure if exists SET_PLANILLAJE;
DELIMITER $
create procedure SET_PLANILLAJE(
in cedula varchar(10),
in placa varchar(10),
in codigoAlquiler int(10),
in horas int(4),
in fecha date)
begin
	INSERT INTO planillaje VALUES(cedula, placa, codigoAlquiler, horas, fecha);
END $
DELIMITER ;

-- inserta registro en la tabla factura
drop procedure if exists SET_FACTURA;
DELIMITER $
create procedure SET_FACTURA(
in in_codAlq int,
in in_descrip varchar(512),
in in_valor float,
in in_iva float,
in in_descuento float,
in in_fecha date,
in in_usuario varchar(20))
begin
	INSERT INTO factura (codigoAlquiler, descripcion, valor, iva, descuento, fecha, usuario) 
    VALUES(in_codAlq, in_descrip, in_valor, in_iva, in_descuento, in_fecha, in_usuario);
END $
DELIMITER ;

-- inserta registro en la tabla alquilermaquinas
drop procedure if exists SET_ALQMAQ;
DELIMITER $
create procedure SET_ALQMAQ(
in codAlq int,
in placa varchar(10),
in devuelto bool)
begin
	INSERT INTO alquilermaquinas VALUES(
    codAlq, placa, devuelto);
END $
DELIMITER ;

-- inserta registro en la tabla alquiler

drop procedure if  exists SET_ALQUILER;
DELIMITER $
create procedure SET_ALQUILER(
in in_ruc varchar(13),
in in_cedula varchar(10),
in in_fecha_inicio date,
in in_costoHora float
-- in in_fecha_fin date
)
BEGIN
	insert into alquiler (ruc,cedula,fecha_inicio,costoHora) 
    values(in_ruc,in_cedula,in_fecha_inicio,in_costoHora);
END $
DELIMITER ;


-- Añade a AlquilerMaquinas pero solo con valores por default que se produce al hacer recien 
-- e alquiler
drop procedure if exists SET_ALQUILER_MAQUINA_DEFAULT;
DELIMITER $
create procedure  SET_ALQUILER_MAQUINA_DEFAULT(
in in_codigoAlquiler int(11),
in in_placa varchar(10)
)
BEGIN
	insert into AlquilerMaquinas (codigoAlquiler,placa)
	values (in_codigoAlquiler, in_placa);
END $
DELIMITER ;

-- ****************TODOS LOS DEL******************

-- ELIMINAR filas en tabla MAQUINA

drop procedure if exists DEL_MAQUINA;
DELIMITER $
create procedure DEL_MAQUINA(in in_placa varchar(10))
begin
	DELETE FROM maquina WHERE maquina.placa = in_placa;
END $ DELIMITER ;


-- ELIMINAR filas en tabla CLIENTNATURAL

drop procedure if exists DEL_CLIENTENATURAL;
DELIMITER $
create procedure DEL_CLIENTENATURAL(in in_cedula varchar(10))
begin
	DELETE FROM clientnatural WHERE clientnatural.cedula = in_cedula;
END $ DELIMITER ;


-- ELIMINAR filas en tabla CLIENTEMPRESA
drop procedure if exists DEL_CLIENTEEMPRESA;
DELIMITER $
create procedure DEL_CLIENTEEMPRESA(in in_ruc varchar(13))
begin
	DELETE FROM clientempresa WHERE clientempresa.ruc = in_ruc;
END $ DELIMITER ;


-- ELIMINAR filas en tabla OPERADOR
drop procedure if exists DEL_OPERADOR;
DELIMITER $
create procedure DEL_OPERADOR(in in_cedula varchar(10))
begin
	DELETE FROM operador WHERE operador.cedula = in_cedula;
END $ DELIMITER ;


/* ELIMINAR filas en tabla PROVEEDOR */
drop procedure if exists DEL_PROVEEDOR;
DELIMITER $
create procedure DEL_PROVEEDOR(in in_ruc varchar(13))
begin
	DELETE FROM proveedor WHERE proveedor.ruc = in_ruc;
END $ DELIMITER ;


drop procedure if exists DEL_PAGPROVEED;
DELIMITER //
create procedure DEL_PAGPROVEED(in fact varchar(10), in in_ruc varchar(13))
begin
delete from registroproveedores
where registroproveedores.codigo_factura = fact and registroproveedores.ruc = in_ruc;
end //
DELIMITER ;



-- elimina registro de la tabla palnillaje
drop procedure if exists DEL_PLANILLAJE;
DELIMITER $
create procedure DEL_PLANILLAJE(in in_placa varchar(10), in in_codAlq varchar(10), in in_fecha date)
begin
	DELETE FROM planillaje WHERE P.codigoAlquiler = in_codAlq and P.placa = in_placa and P.fecha = in_fecha;
END $ DELIMITER ;


-- elimina registro de la tabla aluiler
drop procedure if exists DEL_ALQUILER;
DELIMITER $
create procedure DEL_ALQUILER(in in_codAlq int)
begin
	DELETE FROM alquiler WHERE alquiler.codigoAlquiler = in_codAlq;
END $ DELIMITER ;

-- elimina registro de la tabla aluilermaquinas
drop procedure if exists DEL_ALQMAQ;
DELIMITER $
create procedure DEL_ALQMAQ(in in_codAlq int, in in_placa varchar(10))
begin
	DELETE FROM alquilermaquinas
    WHERE alquilermaquinas.codigoAlquiler = in_codAlq and alquilermaquinas.placa = in_placa;
END $ DELIMITER ;

-- elimina registro de la tabla factura
drop procedure if exists DEL_FACTURA;
DELIMITER $
create procedure DEL_FACTURA(in in_codFac varchar(10))
begin
	DELETE FROM factura
    WHERE factura.codigoFactura = in_codFact;
END $ DELIMITER ;



-- ****************TODOS LOS MOD******************

/* MODIFICAR valores en tabla Maquina */
drop procedure if exists MOD_MAQUINA;
DELIMITER $
create procedure MOD_MAQUINA(in in_placa varchar(10), in in_marca varchar(30), 
								in in_tipo varchar(30),	in in_horometro int, 
                                in in_modelo varchar(30))
begin
	UPDATE maquina M SET M.marca = in_marca, M.tipo = in_tipo,
    M.horometro = in_horometro, M.modelo = in_modelo
	WHERE M.placa = in_placa;
END $ DELIMITER ;


/* MODIFICAR valores en tabla ClientNatural */
drop procedure if exists MOD_CLIENTENATURAL;
DELIMITER /
create procedure MOD_CLIENTENATURAL(in in_cedula varchar(10), in in_nombre varchar(50),
									in in_direccion varchar(255), in in_telefono varchar(10),
									in in_email varchar(255))
begin
	UPDATE clientnatural C SET C.nombre = in_nombre, C.direccion = in_direccion,
	C.telefono = in_telefono, C.email = in_email
	WHERE C.cedula = in_cedula;

END / DELIMITER ;


/* MODIFICAR valores en tabla ClienteEmpresa */
drop procedure if exists MOD_CLIENTEEMPRESA;
DELIMITER /
create procedure MOD_CLIENTEEMPRESA(in in_ruc varchar(13), in in_nombre varchar(50),
									in in_direccion varchar(255), in in_telefono varchar(10),
									in in_email varchar(255))
begin
	UPDATE clientempresa C SET C.nombre = in_nombre, C.direccion = in_direccion,
	C.telefono = in_telefono, C.email = in_email
	WHERE C.ruc = in_ruc;

END / DELIMITER ;


/* MODIFICAR valores en tabla operador */
drop procedure if exists MOD_OPERADOR;
DELIMITER /
create procedure MOD_OPERADOR(
in in_cedula varchar(10), 
in in_nombre varchar(50),
in in_apellido varchar(50), 
in in_direccion varchar(255),
in in_telefono varchar(10))
begin
	UPDATE operador O SET O.nombre = in_nombre, O.direccion = in_direccion,
	O.telefono = in_telefono, O.apellido = in_apellido
	WHERE O.cedula = in_cedula;

END / DELIMITER ;


/* MODIFICAR valores en tabla proveedor */
drop procedure if exists MOD_PROVEEDOR;
DELIMITER /
create procedure MOD_PROVEEDOR(in in_ruc varchar(13), in in_nombre varchar(50), 
							   in in_direccion varchar(255),in in_telefono varchar(10))
begin
	UPDATE proveedor P SET P.nombre = in_nombre, P.direccion = in_direccion,
	P.telefono = in_telefono WHERE P.ruc = in_ruc;

END / DELIMITER ;

-- modifica registro en la tabla registroproveedores
drop procedure if exists MOD_PAGPROVEED;
DELIMITER //
create procedure MOD_PAGPROVEED(
	in fact varchar(10), in in_ruc varchar(13), in in_placa varchar(10),
    in det varchar(255), in val float(10), in in_tipo varchar(50), in fech date)
begin
	update registroproveedores R
    set R.placa = in_placa, R.detalle = det, R.valor =val, R.tipo = in_tipo, R.fecha = fech
	where R.codigo_factura = fact and R.ruc = in_ruc;
end //
DELIMITER ;

/* MODIFICAR valores en tabla planillaje */
drop procedure if exists MOD_PLANILLAJE;
DELIMITER /
create procedure MOD_PLANILLAJE(in in_ced varchar(10), in in_placa varchar(10), 
							   in in_codAlq varchar(10),in in_horas int(4),
                               in in_fec date)
begin
	UPDATE planillaje P SET P.horas = in_horas, P.fecha = in_fec
    WHERE P.codigoAlquiler = in_codAlq and P.placa = in_placa;

END / DELIMITER ;


-- permite que una maquina que haya sido registrada como alquilada ahora sea DEVUELTA
drop procedure if exists MOD_DEVUELVE_ALQUILER_MAQUINA;
DELIMITER //
create procedure MOD_DEVUELVE_ALQUILER_MAQUINA(in in_codigoAlquiler int(11),in in_placa varchar(10) )
begin
	update alquilermaquinas A set A.devuelto=true 
    where A.codigoAlquiler=in_codigoAlquiler and A.placa=in_placa;
end //
delimiter ;

