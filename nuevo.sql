/* Crea la vista para la selección de alquiler durante la facturación de Alquileres */

drop view if exists viewSeleccionarAlquilerCN;

create view viewSeleccionarAlquilerCN 
as select C.cedula, C.nombre, A.codigoAlquiler, A.fecha_inicio, A.costoHora
from clientnatural C join alquiler A on C.cedula = A.cedula 
where A.fecha_fin is null ;

/* Procedure para obtener la informacion de esta vista */
drop procedure if exists getViewSeleccionarAlquilerCN;

DELIMITER $$

create procedure getViewSeleccionarAlquilerCN()
begin
	select * from viewSeleccionaralquilercn;
end $$
DELIMITER ;

/* Procedure para obtener la informacion de esta vista */
drop procedure if exists searchViewSeleccionarAlquilerCN;

DELIMITER $$

create procedure searchViewSeleccionarAlquilerCN(in cedula varchar(10))
begin
	select * from viewSeleccionaralquilercn S where S.cedula like (concat(cedula, '%')) ;
end $$
DELIMITER ;



-- Buscar por codigo (Se busca tanto para Cliente natural como para empresa, pero el id 
-- regresa siempre como cedula)
drop procedure if exists searchViewSeleccionarAlquilerCodigo;

DELIMITER $$

create procedure searchViewSeleccionarAlquilerCodigo (in codigo int(10))
begin
	select * from (select * from viewSeleccionaralquilercn union 
	select * from viewSeleccionaralquileremp) S
	where S.codigoAlquiler = codigo;
end $$
DELIMITER ;

-- Buscar por nombre (Se busca tanto para Cliente natural como para empresa, pero el id 
-- regresa siempre como cedula)

drop procedure if exists searchViewSeleccionarAlquilerNombre;

DELIMITER $$

create procedure searchViewSeleccionarAlquilerNombre (in in_nombre varchar(50))
begin
	select * from (select * from viewSeleccionaralquilercn union 
	select * from viewSeleccionaralquileremp) S
	where S.nombre like (concat(in_nombre, '%'));
end $$
DELIMITER ;



/* Crea la vista para la selección de alquiler mediante Empresa
 durante la facturación de Alquileres */

drop view if exists viewSeleccionarAlquilerEMP;

create view viewSeleccionarAlquilerEMP 
as select C.ruc, C.nombre, A.codigoAlquiler, A.fecha_inicio, A.costoHora
from clientempresa C join alquiler A on C.ruc = A.ruc
where A.fecha_fin is null ;

/* Procedure para obtener la informacion de esta vista */
drop procedure if exists getViewSeleccionarAlquilerEMP;

DELIMITER $$

create procedure getViewSeleccionarAlquilerEMP()
begin
	select * from viewSeleccionaralquileremp;
end $$
DELIMITER ;


/* Procedure para obtener la informacion de esta vista */
drop procedure if exists searchViewSeleccionarAlquilerEMP;

DELIMITER $$

create procedure searchViewSeleccionarAlquilerEMP(in ruc varchar(10))
begin
	select * from viewSeleccionaralquileremp S where S.ruc like (concat(ruc, '%')) ;
end $$
DELIMITER ;


/* Vista para la seleccion de maquinaria */

drop view if exists viewSeleccionarMaquina;

create view viewSeleccionarMaquina 
as select A.codigoAlquiler, A.placa, M.tipo
from viewseleccionaralquilercn V ,alquilermaquinas A, Maquina M
where V.codigoAlquiler = A.codigoAlquiler and A.placa = M.placa
and A.devuelto = false
union
select A.codigoAlquiler, A.placa, M.tipo
from viewseleccionaralquileremp E ,alquilermaquinas A, Maquina M
where E.codigoAlquiler = A.codigoAlquiler and A.placa = M.placa
and A.devuelto = false;


/* Procedure para obtener la informacion de esta vista */
drop procedure if exists getViewSeleccionarMaquina;

DELIMITER $$

create procedure getViewSeleccionarMaquina()
begin
	select * from viewSeleccionarMaquina;
end $$
DELIMITER ;


/* Procedure para obtener la informacion de esta vista 
Debe ser llamada despues de obtener el codigo de alquiler en la vista
viewSeleccionarAlquilerCN o viewSeleccionarAlquilerEMP */

drop procedure if exists searchViewSeleccionarMaquina;

DELIMITER $$

create procedure searchViewSeleccionarMaquina(in codigoAlquiler int)
begin
	select S.placa, S.tipo from viewSeleccionarMaquina S where S.codigoAlquiler = codigoAlquiler;
end $$
DELIMITER ;



/** PROCEDURE PARA CALCULAR EL DETALLE POR MÁQUINA DURANTE LA FACTURACION */

/* Pasos:
- Obtener todos los planillajes de una maquina entre la fecha de max(inicio-alquiler , ultimo-pago)
y la fecha de facturacion
- En cada uno calcular las horas trabajadas (HoraFin - HoraInicio)
- Sumar todas las horas (Se presenta)
- Calcular el total a pagar  */

drop procedure if exists calcularHorasAFacturar;
DELIMITER $$
create procedure calcularHorasAFacturar (in placa varchar(10), in f_inicio date, in f_fin date)
begin
	select sum(P.horas) as horas_trabajadas
	from maquina M inner join planillaje P 
	on M.placa = P.placa
	where M.placa like placa and P.fecha between f_inicio and f_fin;
end $$
DELIMITER ;

-- call calcularHorasAFacturar('GTH-123', '2015-08-10', '2015-08-20');

/* Usar SET_FACTURA para registrar la factura en EMITIR-FACTURA  */


/* UNIR CON LOS OTROS ARCHIVOS */

/* Devuelve 1 si para un alquiler todas sus maquinas han sido devueltas (en alquilermaquinas)
	0 en caso contrario*/
drop procedure if exists verificarDevoluciones;
delimiter $
create procedure verificarDevoluciones(in codAlquiler int, out result int)
begin
	set @var1 = (select count(A.codigoAlquiler) as totalMaquinas 
	from alquilermaquinas A where A.codigoAlquiler = codAlquiler);

	set @var2 = (select count(A.codigoAlquiler) as maquinasDevueltas 
	from alquilermaquinas A where A.codigoAlquiler = codAlquiler and devuelto = true);
    
    IF @var1 = @var2 THEN
		set @result = 1;
	ELSE
		set @result = 0;
	END IF;
        
	set result = @result ;
    
end $
delimiter ;

/* Trigger para actualizar la fecha de fin de un alquiler una vez que 
se devuelve la ultima maquina */

drop trigger if exists tr_finalizarAlquiler;
delimiter $
create trigger tr_finalizarAlquiler after update on alquilermaquinas
for each row begin
	
    declare result int default 0;
    declare fecha date default null;
    
    set fecha = (select max(F.fecha) from factura F where F.codigoAlquiler = new.codigoAlquiler);
    
    -- No se puede hacer esto : set @var = call procedure.
    call verificarDevoluciones(new.codigoAlquiler, result);
	
	IF result = 1 THEN
		update alquiler A set A.fecha_fin = fecha where A.codigoAlquiler = new.codigoAlquiler;
	END IF;
    
end $
delimiter ;


